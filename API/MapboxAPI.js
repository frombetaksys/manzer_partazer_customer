const END_POINT = "https://api.mapbox.com/geocoding/v5/mapbox.places/";
const OPT = ".json?types=locality,address&limit=10&country=mg&access_token=";
const REVERSE_OPT = ".json?types=locality,address&country=mg&access_token=";

export function geocode(text,token){
  return fetch(END_POINT+text+OPT+token,{method:'GET'}).then((response)=>response.json());
}

export function reverseGeocode(lng,lat,token){
  return fetch(END_POINT+String(lng)+","+String(lat)+REVERSE_OPT+token,{method:'GET'}).then((response)=>response.json());
}
