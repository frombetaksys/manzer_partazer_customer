const END_POINT = "http://itwedo-appdev.betaksys.mg/consommateur/";
// const END_POINT = "http://192.168.43.12:8080/consommateur/";

export function getListeEntreprise(data){

  console.log("END_POINT : "+END_POINT);

  console.log("getListeEntreprise eee : ",data);

  return fetch(END_POINT+"entrepriseAproximity", {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
              "token":data.token,
              "latitude":data.lat,
              "longitude":data.lng
            }),
          })
          .then((response)=>response.json())
          .catch((error)=>console.error(error));

}

export function getListeProduit(data){

  console.log("END_POINT : "+END_POINT);

  console.log("getListeProduit eee : ",data);

  return fetch(END_POINT+"produit", {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
              "token":data.token,
              "indexStart":data.indexStart,
              "id_utilisateur":parseInt(data.id_utilisateur),
              "motCle":data.motCle,
              "prix_max":data.prix_max,
              "prix_min":data.prix_min,
              "typeMotsCle":data.typeMotsCle,
              "date_limite":data.date_limite,
              "info_nutr":data.info_nutr,
              "typeProduit":data.typeAliment,
              "listeEntreprise":data.entreprises,
              "tri":data.sorter
            }),
          })
          .then((response)=>response.json())
          .catch((error)=>console.error(error));

}

export function getListeFavoris(data){

  console.log("getListeFavoris eee : ",data);

  return fetch(END_POINT+"favoris", {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
              "token":data.token,
              "id_utilisateur":parseInt(data.id_utilisateur)
            }),
          })
          .then((response)=>response.json())
          .catch((error)=>console.error(error));

}

export function getListeCommande(data){

  console.log("getListeCommande eee : ",data);

  return fetch(END_POINT+"commande", {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
              "token":data.token,
              "id_utilisateur":parseInt(data.id_utilisateur)
            }),
          })
          .then((response)=>response.json())
          .catch((error)=>console.error(error));

}

export function getDetailCommande(data){

  console.log("getDetailCommande eee : ",data);

  return fetch(END_POINT+"commande/detail", {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
              "token":data.token,
              "id_utilisateur":parseInt(data.id_utilisateur),
              "id_commande":parseInt(data.commande_id)
            }),
          })
          .then((response)=>response.json())
          .catch((error)=>console.error(error));

}

export function getDetailProduit(data){

  console.log("getDetailProduit eee : ",data);

  return fetch(END_POINT+"produit/detail", {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
              "token":data.token,
              "id_donation":data.id_donation,
              "id_utilisateur":data.id_utilisateur
            }),
          })
          .then((response)=>response.json())
          .catch((error)=>console.error(error)) ;

}

export function ajouterFavoris(data){

  console.log("ajouterFavoris eee : ",data);

  return fetch(END_POINT+"ajouterFavoris", {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
              "token":data.token,
              "id_utilisateur":data.id_utilisateur,
              "id_donation":data.id_donation
            }),
          })
          .then((response)=>response.json())
          .catch((error)=>console.error(error)) ;

}

export function ajouterVueProduit(data){

  console.log("ajouterVueProduit eee : ",data);

  return fetch(END_POINT+"ajouterVueProduit", {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
              "token":data.token,
              "id_utilisateur":data.id_utilisateur,
              "id_donation":data.id_donation
            }),
          })
          .then((response)=>response.json())
          .catch((error)=>console.error(error)) ;

}

export function envoyerCommande(data){

  console.log("envoyerCommande eee : ",data);

  return fetch(END_POINT+"ajouterCommande", {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
          })
          .then((response)=>response.json())
          .catch((error)=>console.error(error));

}
