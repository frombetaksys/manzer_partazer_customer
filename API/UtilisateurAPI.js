const END_POINT = "http://itwedo-appdev.betaksys.mg/consommateur/";
// const END_POINT = "http://192.168.43.12:8080/consommateur/";

export function createAccount(nom,prenom,email,motDePasse){

  return fetch(END_POINT+"inscription", {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            "nom": nom,
            "prenom": prenom,
            "mail": email,
            "password": motDePasse
          }),
        })
        .then((response)=>response.json())
        .catch((error)=>console.error(error)) ;

}

export function logIn(email,motDePasse){

  return fetch(END_POINT+"connexion", {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            "mail": email,
            "password": motDePasse
          }),
        })
        .then((response)=>response.json())
        .catch((error)=>console.error(error)) ;

}


export function editProfil(data){

  return fetch(END_POINT+"modification", {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
              "token":data.token,
              "utilisateur_id":data.userId,
              "id_consommateur":data.consommateurId,
              "nom": data.nom,
              "prenom": data.prenom,
              "mail": data.email,
              "mobile_banking": data.mobileBanking,
              "adresse_de_livraison":data.adresseLivraison,
              "numero_telephone":data.numTel
            }),
          })
          .then((response)=>response.json())
          .catch((error)=>console.error(error)) ;

}

export function getUserProfil(data){

  console.log("getUser profile eee : ",data);

  return fetch(END_POINT+"profil", {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
              "token":data.token,
              "id_utilisateur":data.userId,
            }),
          })
          .then((response)=>response.json())
          .catch((error)=>console.error(error)) ;

}

export function sendFeedBack(data){

  console.log("sendFeedBack eee : ",data);

  return fetch(END_POINT+"feedback", {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(data),
          })
          .then((response)=>response.json())
          .catch((error)=>console.error(error));

}

// export function getUserProfil(id,token){
//   return fetch('https://api.themoviedb.org/3/movie/' + id + '?api_key=' + API_TOKEN + '&language=fr')
//
//     .then((response) => response.json())
//
//     .catch((error) => console.error(error));
// }
