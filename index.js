import { AppRegistry } from 'react-native';
import App from './App';
import bgMessaging from './Tools/bgMessaging'

AppRegistry.registerComponent('ManzerPartazer', () => App);

AppRegistry.registerHeadlessTask('RNFirebaseBackgroundMessage', () => bgMessaging);
