
const initialState = { idUser:-1,token:"",userName: "",userFirstName:"",email:"",activity:"",locate:-1,notify:-1,panier:[],favoriteFood:["foodwise_info"] }

function updateUserInfo(state = initialState,action){
  let nextState;

  console.log("updateUserInfo redux..... : "+action.type);

  if(action.type=="SET_ACTIVITY")
    console.log("updateUserInfo value..... : "+action.value.activity);

  switch (action.type) {
    case 'SET_INFO':
      nextState = {
        ...state,
        idUser: action.value.idUser,
        token: action.value.token,
        userName: action.value.userName,
        userFirstName: action.value.userFirstName,
        email: action.value.email
      }
      return nextState || state;
    case 'SET_PREFERENCES':
      nextState = {
        ...state,
        locate:action.value.locate,
        notify:action.value.notify
      }

      return nextState || state;
    case 'ADD_FAVORITE_FOOD':
        nextState = {
          ...state,
          favoriteFood: [ ...state.panier,action.value ]
      }
      return nextState || state
    case 'SET_ACTIVITY':
      nextState = {
        ...state,
        activity:action.value.activity
      }
      return nextState || state;
    case 'ADD_IN_PANIER':
      const panierItemIndex = state.panier.findIndex(item => item.produit.id === action.value.produit.id)

      if(panierItemIndex!==-1){
        nextState = Object.assign({}, state);
        let panierItem = nextState.panier[panierItemIndex];
        panierItem.qte += action.value.qte;
        // nextState = {
        //   ...state,
        //   panier: state.panier
        // }
      }else{

        nextState = {
          ...state,
          panier: [ ...state.panier,action.value ]
        }
      }
      return nextState || state
    case 'PUT_IN_PANIER':
        const panierItemIndex0 = state.panier.findIndex(item => item.produit.id === action.value.produit.id)

        if(panierItemIndex0!==-1){
          nextState = Object.assign({}, state);
          let panierItem = nextState.panier[panierItemIndex0];
          panierItem.qte = action.value.qte;
        }else{

          nextState = {
            ...state,
            panier: [ ...state.panier,action.value ]
          }
        }
        return nextState || state
    case 'REMOVE_FROM_PANIER':
      const panierItemIndex1 = state.panier.findIndex(item => item.produit.id === action.value.id)

      if(panierItemIndex1!==-1){
        nextState = {
          ...state,
          panier: state.panier.filter( (item, index) => index !== panierItemIndex1)
        }
      }

      return nextState || state
    case 'EDIT_PANIER':
      const panierItemIndex2 = state.panier.findIndex(item => item.produit.id === action.value.id)

      if(panierItemIndex2!==-1){
        nextState = Object.assign({}, state);
        let panierItem = nextState.panier[panierItemIndex2];
        panierItem.qte = action.value.newQte;
      }

      return nextState || state
    case 'CLEAR_PANIER':

        nextState = {
          ...state,
          panier: []
        }

        return nextState || state
    default:
      return state;
  }
}

export default updateUserInfo;
