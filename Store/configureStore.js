
import { createStore } from 'redux';
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import updateUserInfo from './Reducers/userReducer'

const persistConfig = {
  key: 'root',
  storage,
}

const persistedReducer = persistReducer(persistConfig, updateUserInfo)

const store = createStore(persistedReducer)
const persistor = persistStore(store)

export default { store, persistor };
