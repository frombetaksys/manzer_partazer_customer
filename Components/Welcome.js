import React from 'react'
import { StyleSheet, View, Image, Text, BackHandler, TextInput, TouchableOpacity,KeyboardAvoidingView, ScrollView,Alert,AsyncStorage,TouchableHighlight } from 'react-native'
import { logIn,createAccount } from '../API/UtilisateurAPI';
import Loader from './Loader';
import stylesDefault from "../Styles/styleDefault";
import { Dimensions } from 'react-native';
import BaseComponent from './BaseComponent';
import { connect } from 'react-redux';
import Permissions from 'react-native-permissions';



const win = Dimensions.get('window');
const iW = 2309;
const iH = 874;
const ratio = win.width/iW;


class Welcome extends BaseComponent {
  _didFocusSubscription;
  _willBlurSubscription;


  constructor(props) {
    super(props);

    this._didFocusSubscription = props.navigation.addListener('didFocus', payload =>
      BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
    );

    this.state={
      loading:false,
      nextSrc:require('../Images/icon/next.png'),
      checkBoxProximite:require('../Images/icon/checkbox_empty.png'),
      checkBoxNotification:require('../Images/icon/checkbox_empty.png'),
      step:1,
      offreProximite:false,
      notification:false
    };

  }

  componentDidUpdate() {

  }

  _saveUserPreference(){
    const action = { type: "SET_PREFERENCES", value: {locate:(this.state.offreProximite)?1:0,notify:(this.state.notification)?1:0} };
    this.props.dispatch(action);
  }

  componentDidMount() {
    this._willBlurSubscription = this.props.navigation.addListener('willBlur', payload =>
      BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
    );
  }

  onBackButtonPressAndroid = () => {
    return true;
  };

  componentWillUnmount() {
    this._didFocusSubscription && this._didFocusSubscription.remove();
    this._willBlurSubscription && this._willBlurSubscription.remove();
  }

  _nextStep(){
    if(this.state.step>=2){
      this._saveUserPreference();
      this._startActivity("ListeProduit");
      return;
    }
    
    this.setState({
      step:this.state.step+1
    })
  }

  _toggleChoiceProximite(){
    if(this.state.offreProximite){
      this.setState({offreProximite:false,checkBoxProximite:require('../Images/icon/checkbox_empty.png')});
    }else{
      Permissions.request('location').then(response => {
        // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
        if(response=='authorized'){
          this.setState({offreProximite:true,checkBoxProximite:require('../Images/icon/checkbox.png')})
        }else if(response=='denied'){
          Alert.alert(
            'Problème d\'autorisation',
            'Vous n\'avez pas autoriser l\'accès à votre position donc impossible de déterminer les offres à proxiité.',
            [
              {text: 'OK', onPress: () => console.log('OK Pressed'),style: 'cancel'},
            ],
            { cancelable: true }
          );
        }
      })

    }
  }


  _toggleChoiceNotification(){
    if(this.state.notification){
      this.setState({notification:false,checkBoxNotification:require('../Images/icon/checkbox_empty.png')});
    }else{
      this.setState({notification:true,checkBoxNotification:require('../Images/icon/checkbox.png')})
    }
  }

  _renderCurrentStep(){
    if(this.state.step==1){
      return(
        <View>
          <View style={{ marginTop:60,paddingLeft:16,paddingRight:16 }}>
            <Text style={[{ fontSize:64,color:"#fff" },stylesDefault.barlowBlackItalic]}>Congrats</Text>
          </View>

          <View style={{ flexDirection:"row" }}>
            <Text style={[{ flex:1,color:"#fff",fontSize:22,paddingLeft:150 },stylesDefault.barlowRegular]} > Njary !</Text>
          </View>

          <View style={{ marginTop:64,justifyContent:"center",alignItems:"center" }}>
            <Image style={{ width:300,height:187,resizeMode:Image.resizeMode.contain }} source={require("../Images/people.png")}/>
          </View>
        </View>
      )
    }else if(this.state.step==2){
      return(
        <View style={{ marginTop:64 }}>

          <View style={{ flexDirection:"row" }}>

            <View style={{ justifyContent:"center",alignItems:"center",padding:16 }}>
              <Image style={{ width:100,height:100,resizeMode:Image.resizeMode.contain }} source={ require("../Images/icon/marker.png") } />
            </View>

            <View style={{ flex:1,paddingBottom:45 }}>

              <Text style={[{ color:"#fff",fontSize:64 },stylesDefault.barlowBlackItalic]}>Oui</Text>
              <Text style={[{ color:"#fff",fontSize:22 },stylesDefault.barlowRegular]}>Je veux voir les offres autour de moi.</Text>

            </View>

            <View style={{ justifyContent:"flex-end",padding:8 }}>
              <TouchableOpacity onPress={()=>{ this._toggleChoiceProximite() }}>
                <Image style={{ width:32,height:32,resizeMode:Image.resizeMode.contain,marginRight:8 }} source={this.state.checkBoxProximite} />
              </TouchableOpacity>
            </View>

          </View>

          <View style={{ flexDirection:"row" }}>

            <View style={{ justifyContent:"center",alignItems:"center",padding:16 }}>
              <Image style={{ width:100,height:100,resizeMode:Image.resizeMode.contain }} source={ require("../Images/icon/bulle.png") } />
            </View>

            <View style={{ flex:1,paddingBottom:45 }}>

              <Text style={[{ color:"#fff",fontSize:64 },stylesDefault.barlowBlackItalic]}>Oui</Text>
              <Text style={[{ color:"#fff",fontSize:22 },stylesDefault.barlowRegular]}>Je veux être notifier des offres irrésistibles.</Text>

            </View>

            <View style={{ justifyContent:"flex-end",padding:8 }}>
              <TouchableOpacity onPress={()=>{ this._toggleChoiceNotification() }}>
                <Image style={{ width:32,height:32,resizeMode:Image.resizeMode.contain,marginRight:8 }} source={this.state.checkBoxNotification} />
              </TouchableOpacity>
            </View>

          </View>


        </View>
      )
    }
  }

  render() {
      return (
        <KeyboardAvoidingView style={{flex:1}} >
          <Loader loading={this.state.loading} />
          <View style={[styles.main_container,stylesDefault.themePrimary]} keyboardShouldPersistTaps="handled">

            { this._renderCurrentStep() }

          </View>
          <TouchableOpacity style={{position:"absolute",bottom:16,right:16}} onPress={()=>{ this._nextStep() }} >
            <Image fadeDuration={10} style={{ width:40,height:40,resizeMode:Image.resizeMode.contain }} source={this.state.nextSrc} />
          </TouchableOpacity>
        </KeyboardAvoidingView>
      )
  }
}

const styles = StyleSheet.create({
  main_container:{
    flex:1
  },
  logo_container:{
    flex:1,
    alignItems:"center",
    justifyContent:"flex-end",
    paddingBottom:50,
    paddingTop:50
  },
  logo:{
    width:100,
    height:100
  },
  form_input:{
    flexDirection:"row",
    overflow:"hidden",
    marginBottom:16
  },
  icon:{
    width:20,
    height:20,
    resizeMode:Image.resizeMode.contain
  },
  text_input:{
    flex:1,
    borderBottomColor:"#fff"
  },
  input:{
    color:"#fff",
    paddingBottom:12,
    paddingTop:12,
    paddingLeft:4,
    paddingRight:8,
    fontSize:18
  },
  icon_input:{
    paddingLeft:20,
    paddingRight:20,
    justifyContent:"center",
    backgroundColor:"#4bae32"
  },
  button_container:{
    flexDirection:"row",
    marginTop:16,
    marginBottom:20
  },
  button_sign_up:{
    flex:1,
    alignItems:"center",
    justifyContent:"center",
    borderColor:"#dce1e5",
    borderWidth:2,
    borderRadius:10,
    overflow:"hidden",
    paddingTop:16,
    paddingBottom:16,
    marginRight:4,
    backgroundColor:"#ffffff"
  },
  button_log_in:{
    flex:1,
    backgroundColor:"#4bae32",
    borderColor:"#dce1e5",
    borderWidth:2,
    borderRadius:10,
    overflow:"hidden",
    paddingTop:16,
    paddingBottom:16,
    alignItems:"center",
    marginLeft:4
  },
  button_fb_log_in:{
    flex:1,
    flexDirection:"row",
    alignItems:"center",
    justifyContent:"center",
    backgroundColor:"#4267B2",
    borderColor:"#dce1e5",
    borderWidth:2,
    borderRadius:10,
    overflow:"hidden",
    paddingTop:16,
    paddingBottom:16,
    alignItems:"center",
    marginLeft:4
  },
  button_google_log_in:{
    flex:1,
    flexDirection:"row",
    alignItems:"center",
    justifyContent:"center",
    backgroundColor:"#ffffff",
    borderColor:"#dce1e5",
    borderWidth:2,
    borderRadius:10,
    overflow:"hidden",
    paddingTop:16,
    paddingBottom:16,
    alignItems:"center",
    marginLeft:4
  },
  button_log_in_text:{
    fontWeight:"bold",
    color:"#ffffff"
  },
  button_sign_up_text:{
    fontWeight:"bold",
    color:"#4bae32"
  }
});

const mapStateToProps = (state) => {
  return state
}

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: (action) => { dispatch(action) }
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(Welcome);
