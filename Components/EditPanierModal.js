import React from 'react';
import { View,Text,TouchableOpacity,StyleSheet,TextInput } from 'react-native';
import Modal from "react-native-modal";
import Spinner from './Tools/react-native-number-spinner-inline';

class EditPanierModal extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      newQuantite:this.props.panierItem.qte,
      minQuantite:this.props.panierItem.produit.minQte
    }
  }

  _close(){
    this.props.close();
  }

  _saveNote(note){
    this.setState(note);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({newQuantite:nextProps.panierItem.qte,minQuantite:nextProps.panierItem.produit.minQte});
  }

  render(){
    return(
      <Modal isVisible={ this.props.isShown }>
        <View style={{ flex: 1,alignItems:"center",justifyContent:"center" }}>
          <View style={{ backgroundColor:"#ffffff",borderRadius:4,padding:16 }}>

            <View style={{ alignItems:"center",justifyContent:"center" }}>
              <Text style={{ textAlign:"center",fontSize:18,color:"#00b2a9",fontFamily:"Barlow_Medium" }}>Quantité à commander pour le produit "{this.props.panierItem.produit.nom}" ?</Text>
            </View>

            <View style={{ backgroundColor:"#00000025", height:1, opacity:0.5,marginTop:8,marginBottom:8 }}></View>

            <View style={{ alignItems:"center",justifyContent:"center" }}>

                  <Spinner color="#636e72" value={ this.state.newQuantite } min={ this.state.minQuantite } onNumChange={(num)=>{this.setState({newQuantite:num})}} />

            </View>

            <View style={{ flexDirection:"row",alignItems:"center",justifyContent:"center",marginTop:8 }}>

              <TouchableOpacity style={[stylesDefault.fw_btn_blue,{paddingLeft:38,paddingRight:38}]} onPress={()=>{ this._close() }} >
                <Text style={stylesDefault.fw_btn_txt_sm}>{"Annuler"}</Text>
              </TouchableOpacity>


              <TouchableOpacity style={[stylesDefault.fw_btn,{marginLeft:16}]} onPress={()=>{ this.props.saveEdit(this.state.newQuantite,this.props.panierItem) }} >
                <Text style={stylesDefault.fw_btn_txt_sm}>{"Enregistrer"}</Text>
              </TouchableOpacity>

            </View>
          </View>
        </View>
      </Modal>
    )
  }

}

const styles = StyleSheet.create({
  form_input:{
    flexDirection:"row",
    borderColor:"#dce1e5",
    borderWidth:2,
    borderRadius:10,
    overflow:"hidden",
    marginBottom:16
  },
  text_input:{
    flex:1,
    backgroundColor:"#ffffff"
  },
  input:{
    justifyContent: "flex-start",
    paddingBottom:12,
    paddingTop:12,
    paddingLeft:24,
    paddingRight:8,
    fontSize:16
  }
});

export default EditPanierModal;
