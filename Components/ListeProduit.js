import React from 'react'
import BaseComponent from './BaseComponent';
import { StyleSheet, View, Image, Text, BackHandler, TextInput, TouchableOpacity,ScrollView,Alert,FlatList,Modal  } from 'react-native'
import stylesDefault from '../Styles/styleDefault'
import Loader from './Loader';
import ProduitItem from './ProduitItem';
import DrawerItem from './DrawerItem';
import { getListeProduit,ajouterFavoris } from '../API/ProduitAPI';
import { connect } from 'react-redux';
import { objectIsEmpty } from '../Helper/StandardHelper';
import Drawer from 'react-native-drawer';
import { AsyncStorage } from 'react-native'
import { LoginManager } from 'react-native-fbsdk';
import { GoogleSignin } from 'react-native-google-signin';


import DatePicker from 'react-native-datepicker';
import { CheckBox } from 'react-native-elements';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import DropdownAlert from 'react-native-dropdownalert';

import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
  renderers
} from 'react-native-popup-menu';

const CheckedOption = (props) => (
  <MenuOption value={props.value} onSelect={props.onSelect} style={{margin:0,padding:0}}>
    <Text style={{ color:"#636e72",paddingLeft:8,fontSize:16,backgroundColor:(props.checked)?"#dfe6e9aa":"#FFFFFF",paddingTop:8,paddingBottom:8,borderBottomColor:"#dfe6e944",borderBottomWidth:1 }}>{ props.text+" "+(props.checked ? '\u2713 ' : '')}</Text>
  </MenuOption>
);

class ListeProduit extends BaseComponent {
  _didFocusSubscription;
  _willBlurSubscription;

  constructor(props) {
    super(props);

    this._didFocusSubscription = props.navigation.addListener('didFocus', payload =>
      BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
    );

    this.menus = [
      {
        id:0,
        label:"Liste des produits",
        icon:require('../Images/icon/1.png'),
        actif:true,
        action:"#"
      },
      {
        id:1,
        label:"Mon Panier",
        icon:require('../Images/icon/2.png'),
        actif:false,
        action:"Panier"
      },
      {
        id:2,
        label:"Mes produits favoris",
        icon:require('../Images/icon/3.png'),
        actif:false,
        action:"ListeFavoris"
      },
      {
        id:5,
        label:"Mes commandes",
        icon:require('../Images/icon/4.png'),
        actif:false,
        action:"ListeCommande"
      },
      {
        id:4,
        label:"Mon profil",
        icon:require('../Images/icon/5.png'),
        actif:false,
        action:"Profil"
      },
      {
        id:3,
        label:"Déconnexion",
        icon:require('../Images/icon/6.png'),
        actif:false,
        action:"0"
      },
    ];

    this.typeMotsClePossible = {
      tous:0,
      produit:1,
      entreprise:2
    }

    this.filtreApplicable = [{label:"Régime alimentaire",values:[],toString:()=>{return "Régime alimentaire"}},{label:"Prix",min:-1,max:-1,toString:()=>{return "Prix"}},{label:"Type d'aliment",toString:()=>{return "Type d'aliment"}},{label:"Entreprise",toString:()=>{return "Entreprise"}}];

    this.triApplicable = [{label:"Date",toString:()=>{return "Date"}},{label:"Prix",toString:()=>{return "Prix"}},{label:"Réductions",toString:()=>{return "Réductions"}},{label:"Nouveautés",toString:()=>{return "Nouveautés"}},{label:"Régime alimentaire",toString:()=>{return "Régime alimentaire"}}];

    this.state = {
      prixMin:0,
      prixMax:30000,
      modalPrixVisible:false,
      modalInfoNutriVisible:false,
      modalTypeAlimentVisible:false,
      modalEntrepriseVisible:false,
      infoNutriSelected:[],
      typeAlimentSelected:[],
      entreprisesSelected:[],
      filtreApplique : [],
      triApplique : 0,
      date:"",
      filtre:{value:0,label:"Aucun"},
      produits:[],
      loading:false,
      userId :  "",
      userName : "",
      userFirstName : "",
      userEmail : "",
      userToken : "",
      moreDataAvailable : true,
      typeMotsCle:this.typeMotsClePossible.tous,
      reset:false,
      infoNutri:[],
      typeAliment:[],
      typeAlimentFiltered:[],
      entreprises:[],
      entreprisesFiltered:[],
      isFetching:true,
      motCle:"",
      triLabel:this.triApplicable[0].toString()
    }


    this.currentIndex = 0;

  }

  _back(){
    try {
      Alert.alert(
      'Quitter l\'application',
      'Voulez vous fermer l\'application?',
      [
        {text: 'Non', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        {text: 'Oui', onPress: () => BackHandler.exitApp()},
      ],
      { cancelable: false });

    } catch (e) {
      BackHandler.exitApp();
    }
    // return true;
  }

  async componentDidMount() {
    this._willBlurSubscription = this.props.navigation.addListener('willBlur', payload =>
      BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
    );

    this.checkPermission();
    this.createNotificationListeners();
  }

  onBackButtonPressAndroid = () => {
    this._back();
    return true;
  };

  componentWillUnmount() {
    this._didFocusSubscription && this._didFocusSubscription.remove();
    this._willBlurSubscription && this._willBlurSubscription.remove();
    this.messageListener();
    this.notificationListener();
    this.notificationOpenedListener();
  }

  closeControlPanel = () => {
    this._drawer.close()
  };

  openControlPanel = () => {
    this._drawer.open()
  };

  _findByMotsCle(motCle){
    this.setState({motCle:motCle},()=>{
      if(motCle==""){
        this._doSearch(null);
      }
    });
  }

  _doSearch(event){
    const motCle = this.state.motCle;

    if(motCle.length==0 || motCle.length>2){
      this.currentIndex=0;

      this.setState({reset:true,moreDataAvailable:true,motCle:motCle,isFetching:true},()=>{
        this._getListeProduit();
      });
    }
  }

  _checkData(){
    console.log("ListeProduit","dataIsReady : "+this.dataIsReady+" - current : "+this.props.activity);
    if(!this.dataIsReady && this._inListeProduit()){
      this.dataIsReady = true;
      this._getUserInfo();
    }
  }

  _addToFavoris = (produit)=>{

    produit.isFavoris = true;
    this.forceUpdate();

    ajouterFavoris({token:this.state.userToken,id_donation:produit.id,id_utilisateur:this.state.userId}).then(data=>{

      console.log(data);

      if(data.status!=1){
        produit.isFavoris = false;
        this.forceUpdate();

        this.setState({loading:false});
        Alert.alert(
          'Connexion interrompue',
          'Une erreur s\'est produite lors de l\'enregistrement de  votre préférence',
          [
            {text: 'Ok', onPress: () => console.log("ok") , style:"cancelable"},
          ],
          { cancelable: false }
        );
      }

    }).catch(error=>{
      produit.isFavoris = false;
      this.forceUpdate();

      this.setState({loading:false});
      Alert.alert(
        'Connexion interrompue',
        'Une erreur s\'est produite lors de l\'enregistrement de  votre préférence',
        [
          {text: 'Ok', onPress: () => console.log("ok") , style:"cancelable"},
        ],
        { cancelable: false }
      );
    });
  }

  _removeToFavoris = (produit)=>{

    produit.isFavoris = false;
    this.forceUpdate();

    ajouterFavoris({token:this.state.userToken,id_donation:produit.id,id_utilisateur:this.state.userId}).then(data=>{

      console.log(data);

      if(data.status!=1){
        produit.isFavoris = true;
        this.forceUpdate();

        this.setState({loading:false});
        Alert.alert(
          'Connexion interrompue',
          'Une erreur s\'est produite lors de l\'enregistrement de  votre préférence',
          [
            {text: 'Ok', onPress: () => console.log("ok") , style:"cancelable"},
          ],
          { cancelable: false }
        );
      }

    }).catch(error=>{
      produit.isFavoris = true;
      this.forceUpdate();

      this.setState({loading:false});
      Alert.alert(
        'Connexion interrompue',
        'Une erreur s\'est produite lors de l\'enregistrement de  votre préférence',
        [
          {text: 'Ok', onPress: () => console.log("ok") , style:"cancelable"},
        ],
        { cancelable: false }
      );
    });
  }

  _toggleFavoris = (produit)=>{
    if(produit.isFavoris){
      this._removeToFavoris(produit);
    }else{
      this._addToFavoris(produit);
    }
  }

  _showDetailProduit = (produitId)=>{

    this._startActivity("DetailProduit",{token:this.state.userToken,id_donation:produitId,id_utilisateur:this.state.userId,src:"ListeProduit"});
    this.dataIsReady = false;

    setTimeout(()=>{

      if(this.state.produits.length<=10){
        this.setState({produits:[]});
      }else{
        let newData = this.state.produits.filter((item,index)=> index<this.currentIndex );
        this.setState({produits:newData});
      }
    },1000);

  }

  _logout = ()=>{
    console.log("logout...");
    let self = this;
    AsyncStorage.multiRemove(['userId','userName',"userFirstName","userEmail","userToken"], (err) => {
      console.log(err)
    }).then(async (response)=>{

      this.dataIsReady = false;

      this.currentIndex = 0;

      this.setState({produits:[],loading:false});

      try{
        LoginManager.logout();
      }catch(error){
        console.log(error);
      }

      try {
        await GoogleSignin.revokeAccess();
        await GoogleSignin.signOut();
      } catch (error) {
        console.error(error);
      }
      this.setState({loading:false});

      const action = { type: "SET_PREFERENCES", value: {locate:-1,notify:-1} };
      self.props.dispatch(action);

      self.props.dispatch({ type: "CLEAR_PANIER", value: {} });

      self._startActivity("Login");
    });
  }

  _getUserInfo(){
    this.setState({loading:true});

    this._getUserDataInStorage((err, result) => {
      if(result!=null){
        this.setState({
          userId :  result[0][1],
          userName : result[1][1],
          userFirstName : result[2][1],
          userEmail : result[3][1],
          userToken : result[4][1]
        });
        this._getListeProduit(false,(this.props.navigation.state.params.mapData!=undefined && this.props.navigation.state.params.src=="CarteLocalisation"));
      }else{
        this.dataIsReady = false;
        this.setState({produits:[],loading:false});
        this._startActivity("Login");
      }
    });
  }

  _showPanier(){
    this.dataIsReady = false;
    this._startActivity('Panier');
    setTimeout(()=>{

      if(this.state.produits.length<=10){
        this.setState({produits:[]});
      }else{
        let newData = this.state.produits.filter((item,index)=> index<this.currentIndex );
        this.setState({produits:newData});
      }
    },1000);
  }

  _getListeProduit(reachedEnd=false,fromMap=false){

    let self = this;

    if(fromMap){
      this.setState({motCle : this.props.navigation.state.params.mapData.key});
    }

    if(reachedEnd){
      this.currentIndex += 10;
    }

    if(this.state!=null && this.state.userEmail!=null){
      if(!this.state.isFetching)
        this.setState({loading:true,isFetching:true});

      getListeProduit({token:this.state.userToken,indexStart:this.currentIndex,id_utilisateur:this.state.userId,motCle:this.state.motCle,prix_max:this.state.prixMax,prix_min:this.state.prixMin,typeMotsCle:this.state.typeMotsCle,date_limite:this.state.date,info_nutr:this._getJoinedInfoNutr(),typeAliment:this._getJoinedTypeAliment(),entreprises:this._getJoinedEntreprise(),sorter:this.state.triApplique}).then(data=>{

          console.log("ListeProduit::result",data);

          let status = data.status;
          let produitsFromServer = data.data;

          if(fromMap && produitsFromServer.length==1){
            this.setState({
              loading:false,
              motCle:""
            },()=>{
              this._showDetailProduit(produitsFromServer[0].id_donation);
            });
            return;
          }

          if(status==1){

            let inutri = self.state.infoNutri, entr = self.state.entreprises, ta = self.state.typeAliment;

            if(inutri.length==0 && entr.length==0 && ta.length==0){
              const choices = data.utils;
              inutri = choices.infoNutri;
              entr = choices.entreprise;
              ta = choices.typeProduit;
            }

            let copyOfCurrentProduits = this.state.produits;

            if(self.state.reset)
              copyOfCurrentProduits = [];

            console.log("motCle",this.state.motCle);
            console.log("copyOfCurrentProduits",copyOfCurrentProduits);

            if(produitsFromServer.length>0){

              for(let i=0;i<produitsFromServer.length;i++){
                copyOfCurrentProduits.push({
                  id:produitsFromServer[i].id_donation,
                  nom:produitsFromServer[i].nom_produit,
                  categorie:produitsFromServer[i].type_aliment,
                  prix:produitsFromServer[i].prix,
                  prixNormal:produitsFromServer[i].prixNormal,
                  datePeremption:produitsFromServer[i].dateLimite,
                  reduction:produitsFromServer[i].pourcentage_reduction,
                  isFavoris:produitsFromServer[i].isFavoris,
                  photo:produitsFromServer[i].photo,
                  adresseRamassage:(objectIsEmpty(produitsFromServer[i].adresse_rammassage))?{label:"Non défini",ville:""}:produitsFromServer[i].adresse_rammassage,
                  optionLivraison:{etat:0,prix:1000},
                  qte:parseInt(produitsFromServer[i].quantite),
                  minQte:parseInt(produitsFromServer[i].minimum_qte_vente),
                  entreprise:{
                    nom:produitsFromServer[i].entrepriseName,
                    logo:produitsFromServer[i].logoEntreprise
                  }
                });
              }

              let currentSorter = self.state.triApplique;

              this.setState({
                loading:false,
                produits:copyOfCurrentProduits,
                reset:false,
                infoNutri:inutri,
                entreprises:entr,
                entreprisesFiltered:entr,
                typeAliment:ta,
                typeAlimentFiltered:ta,
                // triApplique: (reachedEnd)?currentSorter:-1,
                isFetching:false
              },()=>{
                if(reachedEnd){
                  self._toggleSorter(currentSorter);
                }
              });

            }else if(reachedEnd && this.state.moreDataAvailable && produitsFromServer.length==0){
              this.currentIndex -= 10;
              this.setState({loading:false,moreDataAvailable:false,reset:false});
            }else{
              this.setState({
                loading:false,
                produits:[],
                reset:false,
                infoNutri:inutri,
                entreprises:entr,
                entreprisesFiltered:entr,
                typeAliment:ta,
                typeAlimentFiltered:ta,
                isFetching:false
              });
            }

          }else if(status==0){
            Alert.alert(
              'Session expirée',
              'Votre session a expiré.Veuillez vous reconnecter!',
              [
                {text: 'Ok', onPress: () => {
                  this.dataIsReady = false;
                  setTimeout(()=>{
                    this.setState({produits:[],loading:false,isFetching:false});
                  },500);
                  self._logout();
                }, style:"cancelable"}
              ],
              { cancelable: false }
            );
          }else{
            this.setState({loading:false,isFetching:false});
            Alert.alert(
              'Connexion interrompue',
              'Une erreur est survenue lors de la récupération de la liste des produits en vente',
              [
                {text:"Quitter",onPress:()=>{ this._back() }},
                {text: 'Réessayer', onPress: () => {
                  self.dataIsReady = false;
                  self._checkData();
                }, style:"cancelable"},
              ],
              { cancelable: false }
            );
          }

      }).catch(error=>{
        console.log("ListeProduit::error",error);
        this.setState({loading:false,isFetching:false});
        Alert.alert(
          'Connexion interrompue',
          'Une erreur est survenue lors de la récupération de la liste des produits en vente',
          [
            {text:"Quitter",onPress:()=>{ this._back() }},
            {text: 'Réessayer', onPress: () => {
              self.dataIsReady = false;
              self._checkData();
            }, style:"cancelable"},
          ],
          { cancelable: false }
        );
      });
    }
  }

  _getJoinedInfoNutr(){
    let result = "";
    const infoNutriSelected = this.state.infoNutriSelected;
    for(let i=0;i<infoNutriSelected.length;i++){
      result+="#"+infoNutriSelected[i].id;
    }
    return result;
  }

  _getJoinedTypeAliment(){
    let result = "";
    const typeAlimentSelected = this.state.typeAlimentSelected;
    for(let i=0;i<typeAlimentSelected.length;i++){
      result+="#"+typeAlimentSelected[i].id;
    }
    return result;
  }

  _getJoinedEntreprise(){
    let result = "";
    const entreprisesSelected = this.state.entreprisesSelected;
    for(let i=0;i<entreprisesSelected.length;i++){
      result+="#"+entreprisesSelected[i].id;
    }
    return result;
  }

  _toggleFilter(filter){
    let filters = this.state.filtreApplique;
    let motCleToFind = this.state.typeMotsCle;
    let factor = 1;

    if(filters.indexOf(this.filtreApplicable[filter])==-1 && filter!=0 && filter!=2 && filter!=3){
      filters.push(this.filtreApplicable[filter]);
    }else if(filter!=0 && filter!=1){
      factor = -1;
      let temp = [];
      for(let i=0;i<filters.length;i++){
        if(filters[i]!=this.filtreApplicable[filter])
          temp.push(filters[i]);
      }
      filters = temp;
    }

    let self = this;

    switch (filter) {
      case 0:
        this.setState({modalInfoNutriVisible:true,filtreApplique:filters});
        break;
      case 1:
        this.setState({modalPrixVisible:true,filtreApplique:filters});
        break;
      case 2:
        this.setState({modalTypeAlimentVisible:true,filtreApplique:filters});
        break;
      case 3:
        this.setState({modalEntrepriseVisible:true,filtreApplique:filters});
        break;
      case 4:
        this.refs.pick_date.onPressDate();
        break;
      default:
        this.setState({filtreApplique:filters});
        break;
    }


  }

  _toggleSorter(sorter){
    let listProduits = this.state.produits;

    // switch (sorter) {
    //   case 1:
    //     ne
    //     this.setState({ produits : listProduits.sort(function(a,b){ return (a.prix>b.prix)?1:((a.prix<b.prix)?-1:0) }),triApplique:sorter,triLabel:this.triApplicable[sorter].toString() });
    //     break;
    //   //prix decroissant
    //   // case 1:
    //   //   this.setState({ produits : listProduits.sort(function(a,b){ return (a.prix>b.prix)?-1:((a.prix<b.prix)?1:0) }),triApplique:sorter,triLabel:this.triApplicable[sorter].toString() });
    //   //   break;
    //   case 0:
    //     this.setState({
    //       produits : listProduits.sort(function(a,b){
    //         let aDate = a.datePeremption.split("/").reverse().join("-");
    //         let bDate = b.datePeremption.split("/").reverse().join("-");
    //
    //         return new Date(aDate) - new Date(bDate);
    //       }),
    //       triApplique:sorter,triLabel:this.triApplicable[sorter].toString()
    //     });
    //     break;
    //   case 2:
    //     this.setState({ produits : listProduits.sort(function(a,b){ return (parseInt(a.reduction)>parseInt(b.reduction))?-1:((parseInt(a.reduction)<b.reduction(prix))?1:0) }),triApplique:sorter,triLabel:this.triApplicable[sorter].toString() });
    //     break;
    //   case 4:
    //     this.setState({modalInfoNutriVisible:true});
    //     break;
    //   //date decroissante
    //   // case 3:
    //   //   this.setState({
    //   //     produits : listProduits.sort(function(a,b){
    //   //       let aDate1 = a.datePeremption.split("/").reverse().join("-");
    //   //       let bDate1 = b.datePeremption.split("/").reverse().join("-");
    //   //
    //   //       return new Date(bDate1) - new Date(aDate1);
    //   //     }),
    //   //     triApplique:sorter,triLabel:this.triApplicable[sorter].toString()
    //   //   });
    //   //   break;
    //   default:
    //     break;
    // }

    if(sorter==4){
      this.setState({modalInfoNutriVisible:true});
    }else{
      this.setState({triApplique:sorter,triLabel:this.triApplicable[sorter].toString(),reset:true,moreDataAvailable:true},()=>{
        this.currentIndex = 0;
        this._getListeProduit();
      });
    }
  }

  _inFilterApplique(filter){
    return (this.state.filtreApplique.indexOf(this.filtreApplicable[filter])==-1)?false:true;
  }

  _setDateLimite(date){
    this.currentIndex=0;
    this.setState({date:date,loading:true,reset:true,moreDataAvailable:true},()=>{
      this.refs.filterMenu.close();
      this._getListeProduit();
    });
  }

  _setInfoNutri(info){

    let infoNutriSelected = this.state.infoNutriSelected;

    if(this.state.infoNutriSelected.indexOf(info)==-1){
      infoNutriSelected.push(info);
    }else{
      let temp = [];
      for(let i=0;i<infoNutriSelected.length;i++){
        if(infoNutriSelected[i]!=info)
          temp.push(infoNutriSelected[i]);
      }
      infoNutriSelected = temp;
    }

    this.setState({infoNutriSelected:infoNutriSelected,modalInfoNutriVisible:false,loading:true},()=>{
      this.setState({modalInfoNutriVisible:true,loading:false});
    });

  }

  _setTypeAliment(info){

    let typeAlimentSelected = this.state.typeAlimentSelected;

    if(this.state.typeAlimentSelected.indexOf(info)==-1){
      typeAlimentSelected.push(info);
    }else{
      let temp = [];
      for(let i=0;i<typeAlimentSelected.length;i++){
        if(typeAlimentSelected[i]!=info)
          temp.push(typeAlimentSelected[i]);
      }
      typeAlimentSelected = temp;
    }

    this.setState({typeAlimentSelected:typeAlimentSelected,modalTypeAlimentVisible:false,loading:true},()=>{
      this.setState({modalTypeAlimentVisible:true,loading:false});
    });


  }

  _setEntreprise(info){

    let entreprisesSelected = this.state.entreprisesSelected;

    if(this.state.entreprisesSelected.indexOf(info)==-1){
      entreprisesSelected.push(info);
    }else{
      let temp = [];
      for(let i=0;i<entreprisesSelected.length;i++){
        if(entreprisesSelected[i]!=info)
          temp.push(entreprisesSelected[i]);
      }
      entreprisesSelected = temp;
    }

    this.setState({entreprisesSelected:entreprisesSelected,modalEntrepriseVisible:false,loading:true},()=>{
      this.setState({modalEntrepriseVisible:true,loading:false});
    });


  }

  _saveFilterInfoNutri(){

    let filters = this.state.filtreApplique;

    if(filters.indexOf(this.filtreApplicable[0])==-1 && this.state.infoNutriSelected.length!=0){
      filters.push(this.filtreApplicable[0]);
    }else if(filters.indexOf(this.filtreApplicable[0])!=-1 && this.state.infoNutriSelected.length==0){
      let temp = [];
      for(let i=0;i<filters.length;i++){
        if(filters[i]!=this.filtreApplicable[0])
          temp.push(filters[i]);
      }
      filters = temp;
    }

    this.setState({filtreApplique:filters,modalInfoNutriVisible:false,reset:true,moreDataAvailable:true});
    this.currentIndex = 0;
    this._getListeProduit();

  }

  _saveFilterTypeAliment(){

    let filters = this.state.filtreApplique;

    if(filters.indexOf(this.filtreApplicable[2])==-1 && this.state.typeAlimentSelected.length!=0){
      filters.push(this.filtreApplicable[2]);
    }else if(filters.indexOf(this.filtreApplicable[2])!=-1 && this.state.typeAlimentSelected.length==0){
      let temp = [];
      for(let i=0;i<filters.length;i++){
        if(filters[i]!=this.filtreApplicable[2])
          temp.push(filters[i]);
      }
      filters = temp;
    }

    this.setState({filtreApplique:filters,modalTypeAlimentVisible:false,reset:true,moreDataAvailable:true});
    this.currentIndex = 0;
    this._getListeProduit();

  }

  _saveFilterEntreprise(){

    let filters = this.state.filtreApplique;

    if(filters.indexOf(this.filtreApplicable[3])==-1 && this.state.entreprisesSelected.length!=0){
      filters.push(this.filtreApplicable[3]);
    }else if(filters.indexOf(this.filtreApplicable[3])!=-1 && this.state.entreprisesSelected.length==0){
      let temp = [];
      for(let i=0;i<filters.length;i++){
        if(filters[i]!=this.filtreApplicable[3])
          temp.push(filters[i]);
      }
      filters = temp;
    }

    this.setState({filtreApplique:filters,modalEntrepriseVisible:false,reset:true,moreDataAvailable:true});
    this.currentIndex = 0;
    this._getListeProduit();

  }

  _infoNutriIsSelected(info){
    return this.state.infoNutriSelected.indexOf(info)!=-1;
  }

  _typeAlimentIsSelected(info){
    return this.state.typeAlimentSelected.indexOf(info)!=-1;
  }

  _entrepriseIsSelected(info){
    return this.state.entreprisesSelected.indexOf(info)!=-1;
  }

  _removeFilterPrix(){
    let filters = this.state.filtreApplique;

    let temp = [];
    for(let i=0;i<filters.length;i++){
      if(filters[i]!=this.filtreApplicable[1])
        temp.push(filters[i]);
    }
    filters = temp;
    this.setState({modalPrixVisible:false,prixMax:"",prixMin:""},()=>{ this._saveFilterPrix();this.setState({filtreApplique:filters}) });
  }

  _saveFilterPrix(){

    let filters = this.state.filtreApplique;

    if(filters.indexOf(this.filtreApplicable[1])==-1){
      filters.push(this.filtreApplicable[1]);
      this.setState({filtreApplique:filters,modalPrixVisible:false,reset:true,moreDataAvailable:true},()=>{
        this.currentIndex = 0;
        this._getListeProduit();
      });
    }else{
      this.setState({modalPrixVisible:false,reset:true,moreDataAvailable:true},()=>{
        this.currentIndex = 0;
        this._getListeProduit();
      });
    }

  }

  _renderFilterView(){
    this.refs.filterMenu.open();
  }

  _produitInPanier=(produit)=>{
    return this.props.panier.findIndex(item => item.produit.id === produit.id)!=-1;
  }

  _qteInPanier=(produit)=>{
    const indexProduit = this.props.panier.findIndex(item => item.produit.id === produit.id);
    if(indexProduit!=-1){
      return this.props.panier[indexProduit].qte;
    }
    return 0;
  }

  _addToPanier = (produit,qte)=>{

    const panier = this.props.panier;

    let doAdd = true;

    const panierItemIndex = panier.findIndex(item => item.produit.id === produit.id);

    if(panierItemIndex!=-1){
      if( panier[panierItemIndex].qte+qte > produit.qte){
          doAdd = false;
      }
    }else{
      qte=parseInt(produit.minQte);
    }

    if(doAdd){
      const action = { type: "ADD_IN_PANIER", value: {produit:produit,qte:qte} };
      this.props.dispatch(action);
      this.forceUpdate();
    }else{
      Alert.alert(
      'Quantité en stock insuffisante',
      'La quantité limite en stock pour le produit "'+produit.nom+'" a été atteint.',
      [
        {text: 'Ok', onPress: () => console.log('Cancel Pressed'), style: 'cancel'}
      ],
      { cancelable: true });
    }
  }

  _doAction = (action)=>{

    if(action=="0"){
      this._askLogout();
    }else if(action!="#"){

      this.closeControlPanel();

      this.dataIsReady = false;

      setTimeout(()=>{
        if(this.state.produits.length<=10){
          this.setState({produits:[]});
        }else{
          let newData = this.state.produits.filter((item,index)=> index<this.currentIndex );
          this.setState({produits:newData});
        }
      },1000);

      this._startActivity(action,{src:"ListeProduit"});
    }
  }

  _renderMainActionZone(){
    return (
      <View style={[{ paddingTop:8,paddingBottom:8,paddingLeft:8,paddingRight:8,borderBottomWidth:1,borderBottomColor:"#00000010" },stylesDefault.themePrimary]}>

        <View style={{ flexDirection:"row" }} >

          <TouchableOpacity style={{ paddingRight:10,alignItems:"center",justifyContent:"center" }} onPress = { ()=>{ this.openControlPanel() } }>
            <Image style={{ width:25,height:25,resizeMode:Image.resizeMode.contain,tintColor:"#fff" }} source={require('../Images/other/menu.png')} />
          </TouchableOpacity>

          <View style={{ flexDirection:"row",flex:1,paddingTop:0,paddingBottom:0,alignItems:"center" }}>
            <TouchableOpacity onPress={()=>{this._doSearch(null)}}>
              <Image style={{ width:25,height:25,resizeMode:Image.resizeMode.contain,tintColor:"#fff" }} source={require('../Images/other/search.png')} />
            </TouchableOpacity>
            <TextInput underlineColorAndroid="#ffffff" placeholderTextColor="#fff"  placeholder={"Chercher"} style={{ flex:1,paddingLeft:8,paddingRight:8,paddingTop:8,paddingBottom:8,fontSize:16,color:"#fff",fontFamily:"Barlow_Regular" }} onChangeText={(text) => this._findByMotsCle(text)} value={this.state.motCle} onSubmitEditing={(event)=>{this._doSearch(event)}}/>
          </View>

          <TouchableOpacity style={{ paddingLeft:10,alignItems:"center",justifyContent:"center" }} onPress={()=>{ this._doAction("ListeFavoris"); }}>
            <Image style={{ width:25,height:25,resizeMode:Image.resizeMode.contain,tintColor:"#fff",opacity:0.9 }} source={require('../Images/other/wish.png')} />
          </TouchableOpacity>

          <TouchableOpacity style={{ paddingLeft:10,alignItems:"center",justifyContent:"center" }} onPress={()=>{ this._showPanier() }}>
            <Image style={{ width:25,height:25,resizeMode:Image.resizeMode.contain,opacity:0.9 }} source={require('../Images/other/basket.png')} />
          </TouchableOpacity>

        </View>

        <View style={{ flexDirection:"row",marginTop:10,justifyContent:"center",marginBottom:4 }}>

          <TouchableOpacity style={{ backgroundColor:"#fff",borderRadius:100,paddingLeft:16,paddingRight:16,marginRight:8 }} onPress={()=> this.refs.trierMenu.open() }>

            <View style={{flexDirection:"row",alignItems:"center"}}>
              <Image style={{ width:14,height:14,resizeMode:Image.resizeMode.contain,tintColor:"#00b2a9" }} source={require('../Images/other/sort.png')} />

              <View style={{ marginLeft:8,width:1,height:32,backgroundColor:"#000",opacity:0.25 }}></View>

              <Menu ref="trierMenu" style={{ height:10,justifyContent:"center",width:85 }}>
                <MenuTrigger customStyles={{triggerText:{ color:"#00b2a9",marginLeft:8,fontFamily:"Barlow_Regular",fontSize:12 }}} text={this.state.triLabel.toUpperCase()} />
                <MenuOptions>

                  { this.triApplicable.map((tri,key)=>{
                      return <CheckedOption onSelect={() => this._toggleSorter(key)} checked={this.state.triApplique==key} text={this.triApplicable[key]} />;
                  }) }

                </MenuOptions>
              </Menu>

            </View>

          </TouchableOpacity>

          <TouchableOpacity style={{ backgroundColor:"#fff",borderRadius:100,paddingLeft:16,paddingRight:16,marginLeft:8 }} onPress={()=> this._doAction("CarteLocalisation") }>

            <View style={{flexDirection:"row",alignItems:"center"}}>
              <Image style={{ width:14,height:14,resizeMode:Image.resizeMode.contain,tintColor:"#00b2a9" }} source={require('../Images/other/map.png')} />

              <View style={{ marginLeft:8,width:1,height:32,backgroundColor:"#000",opacity:0.25 }}></View>

              <Text style={{ color:"#00b2a9",marginLeft:8,width:85,fontFamily:"Barlow_Regular",fontSize:12 }} >CARTE</Text>

            </View>

            { this._renderFilterValue() }

          </TouchableOpacity>

        </View>

      </View>
    )
  }

  _renderFilterValue(){
    if(this.state.filtre.value!=0){
      return(
        <View>
          <Text style={{ fontSize:12,color:"#636e72" }}>{ this.state.filtre.label }</Text>
        </View>
      )
    }
  }

  _renderListeZone(){

    return (
      <View style={{ flex:1 }}>

        { this._renderListe() }

        { this._renderEmptyResult() }

      </View>
    )
  }

  onRefreshProduits(){
    this.setState({ isFetching: true,reset:true,moreDataAvailable:true }, function() {
      this.currentIndex=0;
      this._getListeProduit()
    });
  }

  _renderListe(){
    if(this.state.produits.length!=0){
      return(
        <FlatList
          style = {{ flex:1 }}
          data={this.state.produits}
          keyExtractor={(item) => item.id.toString()}
          renderItem={({item}) => (
            <ProduitItem
              produit={item}
              favorisList = {false}
              showDetailProduit = { this._showDetailProduit }
              addToPanier = { this._addToPanier }
              produitInPanier = { this._produitInPanier }
              qteInPanier = { this._qteInPanier }
              toggleFavoris = { this._toggleFavoris }
            />
          )}
          onEndReachedThreshold={0.1}
          onEndReached={() => {
            console.log("onEndReached","moreDataAvailable : "+this.state.moreDataAvailable+" && produits.length : "+this.state.produits.length>8);
            if(this.state.moreDataAvailable && this.state.produits.length>8){
              this.setState({loading:true});
              this._getListeProduit(true);
            }
          }}
          onRefresh={() => this.onRefreshProduits()}
          refreshing={this.state.isFetching}
        />
      )
    }
  }

  _renderPubZone(){
    return (
      <View>
        <Text>pub</Text>
      </View>
    )
  }

  _renderModalInfoNutri(){
    return(
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.modalInfoNutriVisible}
        onRequestClose={() => this.setState({modalInfoNutriVisible:false})}>
        <View style={{ flex:1,justifyContent:"center",alignItems:"center",backgroundColor:"#00000099",flexDirection: 'column' }}>

          <View style={{width:250,height:300,display: 'flex',backgroundColor:"#ffffff",alignItems:"center",justifyContent:"center",borderRadius:4}}>
            <Text style={{ fontSize:15,color:"#4f4f4f",paddingTop:10,paddingBottom:8,fontFamily:"Barlow_Bold" }}>Filtre sur le régime alimentaire</Text>
            <FlatList
              ref="listInfoNutri"
              data={this.state.infoNutri}
              keyExtractor={(item) => item.id.toString()}
              renderItem={({ item }) => <TouchableOpacity style={{flex:1,flexDirection:"row",alignItems:"center"}} onPress={()=> this._setInfoNutri(item)}>
                  <CheckBox
                    title={item.label}
                    checkedColor="#00AFAA"
                    checked={this._infoNutriIsSelected(item)}
                    onPress={() => this._setInfoNutri(item)}
                    containerStyle = {{ height:22,borderColor:"#00000000",backgroundColor:"#00000000" }}
                    />

                </TouchableOpacity>
              }
            />
            <View style={{ flexDirection:"row",alignItems:"center",marginBottom:20,marginTop:8 }}>

              <TouchableOpacity style={[stylesDefault.fw_btn,{marginLeft:16}]} onPress={()=>{ this._saveFilterInfoNutri() }} >
                <Text style={stylesDefault.fw_btn_txt_sm}>{"VALIDER"}</Text>
              </TouchableOpacity>

            </View>
          </View>
        </View>
      </Modal>
    )
  }

  _renderModalEntreprise(){
    return(
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.modalEntrepriseVisible}
        onRequestClose={() => this.setState({modalEntrepriseVisible:false})}>
        <View style={{ flex:1,justifyContent:"center",alignItems:"center",backgroundColor:"#00000099",flexDirection: 'column' }}>

          <View style={{width:300,height:300,display: 'flex',backgroundColor:"#ffffff",alignItems:"center",justifyContent:"center",borderRadius:4}}>
            <Text style={{ fontWeight:"bold",fontSize:15,color:"#4f4f4f",paddingTop:10,paddingBottom:8 }}>Filtre par entreprises</Text>

            <View style={[styles.form_input,{flex:0,height:50}]}>
              <View style={styles.text_input}>
                <TextInput ref="typeAliment" underlineColorAndroid="#ffffff" style={styles.input} placeholder={"Rechercher..."} onChangeText={(text)=>{ this._filtrerChoixEntreprise(text)}} />
              </View>
            </View>

            <FlatList
              ref="listeEntreprise"
              data={this.state.entreprisesFiltered}
              keyExtractor={(item) => item.id.toString()}
              renderItem={({ item }) => <TouchableOpacity style={{flex:1,flexDirection:"row",alignItems:"center"}} onPress={()=> this._setEntreprise(item)}>
                  <CheckBox
                    title={item.label}
                    checkedColor="#00AFAA"
                    checked={this._entrepriseIsSelected(item)}
                    onPress={() => this._setEntreprise(item)}
                    containerStyle = {{ height:22,borderColor:"#00000000",backgroundColor:"#00000000" }}
                    />

                </TouchableOpacity>
              }
            />
            <View style={{ flexDirection:"row",alignItems:"center",marginBottom:10,marginTop:8 }}>

              <TouchableOpacity style={{flexDirection:"row",alignItems:"center",justifyContent:"center",backgroundColor:"#00AFAA",borderColor:"#dce1e5",borderWidth:2,borderRadius:10,paddingTop:10,paddingBottom:10,paddingLeft:16,paddingRight:16,marginLeft:4}} onPress={()=>{ this._saveFilterEntreprise() }}>
                <Text style={{fontSize:16,fontWeight:"bold",color:"#ffffff"}}>{"Valider"}</Text>
              </TouchableOpacity>

            </View>
          </View>
        </View>
      </Modal>
    )
  }

  _filtrerChoixEntreprise(motCle){
    let newData;

    if(motCle.length==0)
      newData = this.state.entreprises;
    else
      newData = this.state.entreprises.filter(function(entreprise){ return entreprise.label.toLowerCase().indexOf(motCle)!=-1 });

    this.setState({entreprisesFiltered:newData});

  }


/*            <View style={[styles.form_input,{flex:0,height:50}]}>
              <View style={styles.text_input}>
                <TextInput ref="typeAliment" underlineColorAndroid="#ffffff" style={styles.input} placeholder={"Rechercher..."} onChangeText={(text)=>{ this._filtrerChoixTypeAliment(text)}} />
              </View>
            </View>*/
  _renderModalTypeAliment(){
    return(
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.modalTypeAlimentVisible}
        onRequestClose={() => this.setState({modalTypeAlimentVisible:false})}>
        <View style={{ flex:1,justifyContent:"center",alignItems:"center",backgroundColor:"#00000099",flexDirection: 'column' }}>

          <View style={{width:250,height:240,display: 'flex',backgroundColor:"#ffffff",alignItems:"center",justifyContent:"center",borderRadius:4}}>
            <Text style={{ fontFamily:"Barlow_Bold",fontSize:16,color:"#4f4f4f",paddingTop:10,paddingBottom:8 }}>Filtre par type d'aliment</Text>

            <FlatList
              ref="listTypeAliment"
              data={this.state.typeAlimentFiltered}
              keyExtractor={(item) => item.id.toString()}
              renderItem={({ item }) => <TouchableOpacity style={{flex:1,flexDirection:"row",alignItems:"center"}} onPress={()=> this._setTypeAliment(item)}>
                  <CheckBox
                    title={item.label}
                    checkedColor="#00AFAA"
                    checked={this._typeAlimentIsSelected(item)}
                    onPress={() => this._setTypeAliment(item)}
                    containerStyle = {{ height:22,borderColor:"#00000000",backgroundColor:"#00000000" }}
                    />

                </TouchableOpacity>
              }
            />
            <View style={{ flexDirection:"row",alignItems:"center",marginBottom:20,marginTop:8 }}>

              <TouchableOpacity style={[stylesDefault.fw_btn,{marginLeft:16}]} onPress={()=>{ this._saveFilterTypeAliment() }} >
                <Text style={stylesDefault.fw_btn_txt_sm}>{"VALIDER"}</Text>
              </TouchableOpacity>

            </View>
          </View>
        </View>
      </Modal>
    )
  }

  _filtrerChoixTypeAliment(motCle){
    let newData;

    if(motCle.length==0)
      newData = this.state.typeAliment;
    else
      newData = this.state.typeAliment.filter(function(typeAliment){ return typeAliment.label.toLowerCase().indexOf(motCle)!=-1 });

    this.setState({typeAlimentFiltered:newData});

  }

  _renderModalPrix(){
    return(
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.modalPrixVisible}
        onRequestClose={() => this.setState({modalPrixVisible:false})}>

        <View style={{ flex:1,justifyContent:"center",alignItems:"center",backgroundColor:"#00000099"}}>

          <View style={{width:300,height:290,backgroundColor:"#ffffff",borderRadius:4}}>

            <View style={{ alignItems:"center",justifyContent:"center",marginTop:8 }}>
              <Text style={{ textAlign:"center",fontSize:15,color:"#636e72",fontWeight:"bold" }}>Filtre sur le prix</Text>
            </View>

            <View style={{ backgroundColor:"#00000025", height:1, opacity:0.5,marginTop:8,marginBottom:8 }}></View>

            <View style={{ flex:1,marginTop:8 }}>

              <Text style={[styles.label_input,stylesDefault.darkTextColor]}>Prix minimum</Text>
              <View style={styles.form_input}>
                <View style={styles.text_input}>
                  <TextInput ref="prixMax" underlineColorAndroid="#ffffff" style={styles.input} placeholder={"Saisir un prix"} onChangeText={(text)=>this.setState({prixMin:text})} value={ this.state.prixMin }  keyboardType={"numeric"} />
                </View>
              </View>

              <Text style={[styles.label_input,stylesDefault.darkTextColor]}>Prix maximum</Text>
              <View style={styles.form_input}>

                <View style={styles.text_input}>
                  <TextInput ref="prixMin" underlineColorAndroid="#ffffff" style={styles.input} placeholder={"Saisir un prix"} onChangeText={(text)=> this.setState({prixMax:text})} value={ this.state.prixMax }  keyboardType={"numeric"} />
                </View>

              </View>

            </View>

            <View style={{ flexDirection:"row",alignItems:"center",justifyContent:"center",marginBottom:16,marginTop:8 }}>

              <TouchableOpacity style={{flexDirection:"row",alignItems:"center",justifyContent:"center",backgroundColor:"#dfe6e9",borderColor:"#dce1e5",borderWidth:2,borderRadius:10,paddingTop:10,paddingBottom:10,paddingLeft:10,paddingRight:10,marginRight:8}} onPress={()=>{ this._removeFilterPrix() }}>
                <Text style={{fontSize:16,fontWeight:"bold",color:"#636e72"}}>Supprimer le filtre</Text>
              </TouchableOpacity>

              <TouchableOpacity style={{flexDirection:"row",alignItems:"center",justifyContent:"center",backgroundColor:"#00AFAA",borderColor:"#dce1e5",borderWidth:2,borderRadius:10,paddingTop:10,paddingBottom:10,paddingLeft:16,paddingRight:16,marginLeft:4}} onPress={()=>{ this._saveFilterPrix() }}>
                <Text style={{fontSize:16,fontWeight:"bold",color:"#ffffff"}}>{"Valider"}</Text>
              </TouchableOpacity>

            </View>
          </View>
        </View>
      </Modal>
    )
  }

  _renderEmptyResult(){
    if(this.state.produits.length==0 && !this.state.loading){
      return(
        <View style={{ flex:1,alignItems:"center",justifyContent:"center" }}>

          <View style={{ alignItems:"center",justifyContent:"center" }}>
            <Image source={ require('../Images/other/empty_box.png') } style={{ width:100,height:100,opacity:0.25 }} />
            <Text style={{ fontSize:16,color:"#4f4f4f",opacity:0.75,marginTop:16,fontFamily:"Barlow_Regular" }}>{ "Aucun produit n'a été trouvé" }</Text>
          </View>

        </View>
      )
    }
  }

  _renderControlPanelEmail(){
    if(this.state!=null && this.state.userEmail!=null){
      if(this.state.userEmail.indexOf("facebook")==-1){
        return(
          <View>
            <Text style={{ color:"#00b2a9",fontFamily:"Barlow_Regular" }}>{ this.state.userEmail }</Text>
          </View>
        )
      }else{
        return(
          <View style={{ flexDirection:"row",marginTop:4 }}>
            <Image source={require("../Images/fb_icon.png")} style={{ width:10,height:10,marginTop:5,tintColor:"#4267B2" }} />
            <Text style={[{marginLeft:4,color:"#00b2a9",fontFamily:"Barlow_Regular",fontSize:12}]}>Associé à mon compte facebook</Text>
          </View>
        )
      }
    }
  }

  _renderControlPanel(){
    return(
      <View style={{ flex:1,backgroundColor:"#00b2a9" }}>

        <View ref="header" style={{ backgroundColor:"#fff",position:"relative",paddingTop:16,paddingBottom:16 }}>

          <View style={{ paddingLeft:16,paddingTop:8,paddingBottom:8,flexDirection:"row" }}>

            <View style={{width:70,height:70,borderRadius:100,
              alignItems:"center"
            }}>
              <Image style={{ width:68,height:68,resizeMode:Image.resizeMode.contain,borderRadius:100 }} source={ require('../Images/other/avatar_placeholder.png') } />
            </View>

            <View style={{ flex:1,justifyContent:"center",paddingLeft:16 }}>
              <Text style={{ fontSize:20,fontFamily:"Barlow_Bold",color:"#00b2a9" }}>{ this.state.userName }</Text>
              { this._renderControlPanelEmail() }
            </View>

          </View>

        </View>

        <View style={{ flex:1,paddingTop:16,paddingLeft:8,paddingRight:8,paddingBottom:8 }}>
          <FlatList
            style = {{ flex:1 }}
            data={this.menus}
            keyExtractor={(item) => item.id.toString()}
            renderItem={({item}) => (
              <DrawerItem
                menu={item}
                doAction={ this._doAction }
              />
            )}
          />
        </View>

      </View>
    )
  }

/*<Loader loading={this.state.loading} />*/

  render() {
      this._checkData();
      return (
        <Drawer
          ref={(ref) => this._drawer = ref}
          tapToClose={true}
          openDrawerOffset={100}
          styles={drawerStyles}
          content={ this._renderControlPanel() }
        >
          <View style={{ flex:1,backgroundColor:"#ffffff" }} >

            <Loader loading={this.state.loading} />

            { this._renderMainActionZone() }

            { this._renderListeZone() }

            { this._renderModalInfoNutri() }

            { this._renderModalTypeAliment() }

            { this._renderModalEntreprise() }

            { this._renderModalPrix() }

            <DropdownAlert ref="alert" successColor="#00b2a9" closeInterval={7500} />
          </View>
        </Drawer>
      )
  }
}

const drawerStyles = {
  drawer: { shadowColor: '#000000', shadowOpacity: 0.8, shadowRadius: 3,paddingRight:2},
  main: {paddingLeft: 0},
}

const styles = StyleSheet.create({
  form_container:{
    flex:1,
    backgroundColor:"#FF0000"
  },
  form_input:{
    flex:1,
    flexDirection:"row",
    borderColor:"#dce1e5",
    borderWidth:2,
    borderRadius:10,
    overflow:"hidden",
    marginBottom:8,
    marginLeft:8,
    marginRight:8
  },
  text_input:{
    flex:1,
    backgroundColor:"#ffffff"
  },
  input:{
    paddingBottom:10,
    paddingTop:12,
    paddingLeft:24,
    paddingRight:8,
    fontSize:16,
    fontFamily:"Barlow_Bold"
  },
  icon:{
    width:20,
    height:20,
    resizeMode:Image.resizeMode.contain
  },
  icon_input:{
    paddingLeft:20,
    paddingRight:20,
    justifyContent:"center",
    backgroundColor:"#00AFAA"
  },
  label_input:{
    fontWeight:"bold",
    fontSize:16,
    marginBottom:2,
    marginLeft:8
  }
});

const mapStateToProps = (state) => {
  return state
}

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: (action) => { dispatch(action) }
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(ListeProduit);
