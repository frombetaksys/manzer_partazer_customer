
import React from 'react'
import BaseComponent from './BaseComponent'
import { StyleSheet, View, Image, Text,AsyncStorage,Alert } from 'react-native'
import { connect } from 'react-redux';
import stylesDefault from '../Styles/styleDefault'
import { DotsLoader } from 'react-native-indicator'
import FadeIn from 'react-native-fade-in-image';
import {Animated} from 'react-native';


class SplashScreen extends BaseComponent {

  constructor(props) {
    super(props);

    this.state = {
      showFirstView:true,
      fadeAnim:new Animated.Value(0)
    }
  }

  _fadeIn(){
    Animated.timing(          // Animate over time
      this.state.fadeAnim, // The animated value to drive
      {
        toValue: 1,           // Animate to opacity: 1 (opaque)
        duration: 1000,       //  2000ms
      }
    ).start();                // Starts the animation
  }

  _renderFirstView(){
    if(this.state.showFirstView){
      return(
        <View>
          <DotsLoader/>
        </View>
      )
    }else{
      this._fadeIn();
      return(
        <Animated.View style={{ opacity:this.state.fadeAnim }}>
          <Image style={styles.logo} source={require('../Images/logo.png')} />
        </Animated.View>
      )
    }
  }

  render() {
      return (
        <View style={[styles.main_container,stylesDefault.themePrimary]}>
          { this._renderFirstView() }
        </View>
      )
  }

  async componentDidMount(){
    console.log("componentDidMount...");

    setTimeout(()=>{

      this.setState({showFirstView:false},()=>{

        setTimeout(()=>{
          AsyncStorage.getItem('userId', (err, result) => {
            console.log(result);
            let destActivity = "Login";

            if(result!=null){
              if(this.props.locate==-1 && this.props.notify==-1)
                destActivity = "Welcome";
              else
                destActivity = "ListeProduit";
            }

            this.checkPermission();

            this._startActivity(destActivity);
          });
        },2000);

      });

    },2800);

  }

}

const styles = StyleSheet.create({
  main_container:{
    flex:1,
    justifyContent:"center",
    alignItems:"center"
  },
  logo:{
    width:150,
    height:150,
    resizeMode:Image.resizeMode.center
  },
  name_container:{
    flexDirection:"row",
    marginTop:16,
  },
  name1:{
    fontSize:20,
    color:"#343A40"
  },
  name2:{
    fontSize:20,
    fontWeight:"bold",
    color:"#343A40"
  }
})

const mapStateToProps = (state) => {
  return state
}

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: (action) => { dispatch(action) }
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(SplashScreen);
