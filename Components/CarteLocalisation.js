import React from 'react'
import BaseComponent from './BaseComponent';
import { StyleSheet, View, Text, Image, TouchableOpacity,ScrollView,Alert,ToastAndroid,BackHandler,TextInput,ListView } from 'react-native'
import { connect } from 'react-redux';
import Loader from './Loader';
import { mileSeparator,objectIsEmpty } from '../Helper/StandardHelper';
import stylesDefault from '../Styles/styleDefault'
import PrimaryButton from './Tools/PrimaryButton'
import Mapbox from '@mapbox/react-native-mapbox-gl';
import { geocode,reverseGeocode } from '../API/MapboxAPI';
import { getListeEntreprise } from '../API/ProduitAPI';
import { version } from 'react-native/package.json';
import { Keyboard } from 'react-native'
import Permissions from 'react-native-permissions'
import Geolocation from 'react-native-geolocation-service'



const mapToken = "pk.eyJ1IjoibmphcnkiLCJhIjoiY2p1ZHU0NWYzMDJ1MTRkbW16N2pmczh6YiJ9.i3r9euc0dZsYchVvQfdG5A";

Mapbox.setAccessToken(
	mapToken
);


class CarteLocalisation extends BaseComponent {

  _didFocusSubscription;
  _willBlurSubscription;

  constructor(props){
    super(props);

    this._didFocusSubscription = props.navigation.addListener('didFocus', payload =>
      BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
    );

		this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      dataSource: this.ds.cloneWithRows([]),
			currentPosition:{label:"",lngLat:[]},
			searchText:"",
			showSuggestion:false,
			stores:[],
			map:null,
			loading:false,
			step:0,
			userId :  "",
			userName : "",
			userFirstName : "",
			userEmail : "",
			userToken : ""
    };

  }

	_getUserInfo(){

		this._getUserDataInStorage((err, result) => {
			if(result!=null){
				this.setState({
					userId :  result[0][1],
					userName : result[1][1],
					userFirstName : result[2][1],
					userEmail : result[3][1],
					userToken : result[4][1]
				});
			}else{
				this.dataIsReady = false;
				this.setState({produits:[],loading:false});
				this._startActivity("Login");
			}
		});
	}

  componentDidMount() {
    this._willBlurSubscription = this.props.navigation.addListener('willBlur', payload =>
      BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
    );

		this._getUserInfo();

  }

  onBackButtonPressAndroid = () => {
		this._back();
    return true;
  };

	_back(){
		if(this.state.step==1){
			this.setState({
				step:0,
				stores:[],
				currentPosition:{label:"",lngLat:[]},
				searchText:""
			})
		}else{
			this._startActivity("ListeProduit");
		}
	}

  _geocoding(text){
		this.setState({searchText:text});

    if(text.length >=3 ){

      geocode(text,mapToken).then(res => {
          console.log("Geocoding",res);
					if(res.features.length>0){
						this.setState({dataSource:this.ds.cloneWithRows(res.features),showSuggestion:true});
					}else{
						this.setState({showSuggestion:false});
					}
			})
      .catch(err => console.log("Geocoding",err))

    }else{
			this.setState({showSuggestion:false,stores:[]});
			this
		}
  }

	_setCurrentPosition(position){
		this.setState({ currentPosition : position,searchText:position.label, showSuggestion:false },()=>{
			Keyboard.dismiss();
		});

	}

	_showOnProximity = ()=>{
		const position = this.state.currentPosition;
		const self = this;

		if(position.label=="")
			return;

		console.log("_showOnProximity",position);

		const map = this.refs.map;

		map.setCamera({
		  centerCoordinate: position.lngLat,
		  zoom: 14
		});


		this.setState({loading:true});

		//get store on proximity from server
		getListeEntreprise({token:this.state.userToken,lat:position.lngLat[1],lng:position.lngLat[0]}).then((response)=>{

			const entreprises = response.data;

			const coords = [];

			for(let i=0;i<entreprises.length;i++){
				coords.push({id:entreprises[i].id_entreprise+"_"+i,label:entreprises[i].nom_entreprise,center:[parseFloat(entreprises[i].longitude),parseFloat(entreprises[i].latitude)]})
			}

			console.log(coords);

			this.setState({
				step:1,
				loading:false,
				stores:coords
			})

		}).catch((error)=>{
			console.log(console.error());
			this.setState({
				loading:false
			},()=>{
				Alert.alert(
					'Connexion interrompue',
					'Une erreur est survenue lors de la récupération des données',
					[
						{text:"Quitter",onPress:()=>{ this._back() }},
						{text: 'Réessayer', onPress: () => {
							self.dataIsReady = false;
							self._showOnProximity();
						}, style:"cancelable"},
					],
					{ cancelable: false }
				);
			})

		})


	}

	_maPosition = ()=>{
		Permissions.check('location').then(response => {
      // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
      if(response=='authorized'){
        console.log("BaseComponent","start location");
				this.setState({loading:true});

        Geolocation.getCurrentPosition(
          (position)=>{
            console.log("BaseComponent","POSITION : ");
            console.log(position);
						reverseGeocode(position.coords.longitude,position.coords.latitude,mapToken).then(res => {
			          console.log("Geocoding",res);

								this.setState({loading:false});

								if(res.features.length>0){
									this._setCurrentPosition({label:res.features[0].text+", "+res.features[0].context[1].text,lngLat:res.features[0].center});
								}else{

								}
						})
			      .catch(err => {
							this.setState({loading:true});
							console.log("Geocoding",err)
						})
          },
          (error)=>{
						this.setState({loading:true});
            console.log("BaseComponent",error);
          },
          { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
        );
      }
    })
	}

  _renderActionBar(){
    return(
      <View style={[{ flexDirection:"row",paddingLeft:4,paddingTop:12,paddingBottom:12,paddingRight:4 },stylesDefault.themePrimary]}>

        <TouchableOpacity onPress={()=>{ this._back() }}>
          <Image style={{ tintColor:"#fff",width:25,height:25 }} source={ require("../Images/other/left_arrow.png") } />
        </TouchableOpacity>

        <View style={{ flex:1,alignItems:"center",justifyContent:"center" }}>
          <Text style={[{ color:"#fff",fontSize:18 },stylesDefault.barlowBlackItalic]}>{"Où puis-je sauver des produits?"}</Text>
        </View>

      </View>
    )
  }

	_renderSuggestion(){
		if(this.state.showSuggestion){
			return(
				<View style={{position: 'absolute', width: 260,top:52, backgroundColor: '#fff',zIndex:99,maxHeight:90,borderColor:"#dedede",borderLeftWidth:1,borderBottomWidth:1,borderRightWidth:1}}>
          <ListView
						keyboardShouldPersistTaps='handled'
            dataSource={this.state.dataSource}
            renderRow={(rowData, sectionId, rowId, highlightRow) =>
                  <TouchableOpacity
                    onPress={() => {
                        console.log(rowData);
												this._setCurrentPosition({label:rowData.text+", "+rowData.context[1].text,lngLat:rowData.center});
                      }
                    }
                    style={{ backgroundColor:"#fff",padding:8,borderBottomWidth:0.5,borderBottomColor:"#0001" }}
                    >
                      <Text>{rowData.text}, {rowData.context[1].text}</Text>
                   </TouchableOpacity>
               }
          />
      </View>
		)
		}
	}

  _renderSearchZone(){
    return(
      <View style={{ position:"absolute",left:0,right:0,alignItems:"center",justifyContent:"center",backgroundColor:"#fffd" }}>

        <View style={{
          flexDirection:"row",
          alignItems:"center",
          justifyContent:"center",
          borderRadius:10,
          paddingLeft:8,
          shadowColor: '#000',
          shadowOffset: { width: 100, height: 0 },
          shadowOpacity: 1,
          shadowRadius: 5,
          elevation: 2,
          marginTop:10,
          backgroundColor:"#fff"}}>

          <View>
            <TextInput style={[{ flex:1,alignItems:"center",justifyContent:"center",width:220,marginLeft:8,fontStyle:"italic",marginTop:8 },stylesDefault.barlowRegular]} underlineColorAndroid="#fff0" placeholderTextColor={"#000"} placeholder={"Où veux-tu chercher des produits?"} value={this.state.searchText} onChangeText={(text)=>{ this._geocoding(text) }}/>

          </View>

          <View style={[{paddingTop:12,paddingBottom:12,paddingLeft:10,paddingRight:10,borderTopRightRadius:10,borderBottomRightRadius:10},stylesDefault.themePrimary]}>
            <Image style={{ width:20,height:20 }} source={require("../Images/other/search.png")}/>
          </View>

        </View>

        <TouchableOpacity style={{ flexDirection:"row",marginTop:12,marginBottom:12 }} onPress={()=>{ this._maPosition() }}>

          <Image style={{ width:16,height:16 }} source={require("../Images/other/world_location_black.png")} />

          <Text style={[{ marginLeft:4,fontStyle:"italic" },stylesDefault.barlowRegular]}>Utiliser ma position actuelle</Text>

        </TouchableOpacity>

        <View style={{marginBottom:8,zIndex:1}}>
						<TouchableOpacity style={[{paddingLeft:25,paddingRight:25,paddingTop:10,paddingBottom:10,borderRadius:100,marginLeft:8,
			      marginRight:8,
			      shadowColor: '#000',
			      shadowOffset: { width: 0, height: 2 },
			      shadowOpacity: 1,
			      shadowRadius: 1,
			      elevation: 4,
			      marginBottom:6},stylesDefault.themePrimary]} onPress={()=>{this._showOnProximity()}}>
			        <Text style={[{ color:"#fff" },stylesDefault.barlowBold]}>{"Choisir ce lieu".toUpperCase()}</Text>
			      </TouchableOpacity>
        </View>

				{ this._renderSuggestion() }

      </View>
    )
  }

  _renderMapZone(){
    return(
      <View style={{ flex:1 }}>
        <Mapbox.MapView
					ref="map"
          styleURL={Mapbox.StyleURL.Street}
          zoomLevel={15}
          centerCoordinate={[47.52169,-18.91559]}
          style={{ flex:1 }}>

					{ this._renderAnnotations() }

        </Mapbox.MapView>
      </View>
    )
  }

  _renderAnnotation(store) {
		const id = "Store_"+store.id;
    const coordinate = store.center;
		const title = store.label;

		return (
			<Mapbox.PointAnnotation
				key={id}
				id={id}
				coordinate={coordinate}>
				<TouchableOpacity onPress={()=>{this._startActivity("ListeProduit",{src:"CarteLocalisation",mapData:{key:title}})}}>
					<Image style={{ width:30,height:30,resizeMode:Image.resizeMode.contain }} source={require("../Images/other/marker_fw.png")} />
				</TouchableOpacity>
				<Mapbox.Callout title={title} />
			</Mapbox.PointAnnotation>
		);
	}

	_renderNumberResult(){
		if(this.state.stores.length>0){
			return(
				<View style={{ alignItems:"center",position:"absolute",top:160,left:0,right:0 }}>
					<View style={[{paddingLeft:25,paddingRight:25,paddingTop:10,paddingBottom:10,borderRadius:100,marginLeft:8,
			      marginBottom:6,
						backgroundColor:"#00b2a9dd"}]}>
		        <Text style={[{ color:"#fff",fontSize:18 },stylesDefault.barlowBlackItalic]}>{(this.state.stores.length<10)?"0"+String(this.state.stores.length):this.state.stores.length} résultat{(this.state.stores.length==1)?"":"s"} trouvé{(this.state.stores.length==1)?"":"s"} !</Text>
		      </View>
				</View>
			)
		}
	}

	_renderAnnotations () {
	    const items = [];
			const stores = this.state.stores;

	    for (let i = 0; i < stores.length; i++) {
	      items.push(this._renderAnnotation(stores[i]));
	    }

	    return items;
	}

  render(){
    return(
      <View style={{ flex:1}}>
				<Loader loading={this.state.loading} />

				{ this._renderActionBar() }

        <View style={{ flex:1 }}>

          { this._renderMapZone() }

          { this._renderSearchZone() }

					{ this._renderNumberResult() }

        </View>

      </View>
    )
  }
}

const styles = StyleSheet.create({
  button_fb_log_in:{
    width:120,
    flexDirection:"row",
    alignItems:"center",
    justifyContent:"center",
    backgroundColor:"#00AFAA",
    borderColor:"#dce1e5",
    borderWidth:2,
    borderRadius:10,
    paddingTop:10,
    paddingBottom:10
  },
  button_log_in_text:{
    fontSize:16,
    fontWeight:"bold",
    color:"#ffffff"
  },
  annotationContainer: {
		width: 30,
		height: 30,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: 'white',
		borderRadius: 15
	},
	annotationFill: {
		width: 30,
		height: 30,
		borderRadius: 15,
		backgroundColor: 'blue',
		transform: [{ scale: 0.6 }]
	},
	autocompleteContainer: {
    backgroundColor: '#ffffff',
    borderWidth: 0,
  },
  itemText: {
    fontSize: 15,
    paddingTop: 5,
    paddingBottom: 5,
    margin: 2,
  }
})

const mapStateToProps = (state) => {
  return state
}

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: (action) => { dispatch(action) }
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(CarteLocalisation);
