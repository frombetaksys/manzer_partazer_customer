import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Modal,
  ActivityIndicator
} from 'react-native';
import { DotsLoader } from 'react-native-indicator'


const Loader = props => {
  const {
    loading,
    ...attributes
  } = props;

return (
    <Modal
      transparent={true}
      onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}
      visible={loading}>

      <View style={styles.modalBackground}>
        <View style={styles.activityIndicatorWrapper}>
          <DotsLoader/>
        </View>
      </View>

    </Modal>
  )
}

const styles = StyleSheet.create({
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'
  },
  activityIndicatorWrapper: {
    backgroundColor: '#fff',
    borderRadius: 4,
    padding:32,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around'
  }
});

export default Loader;
