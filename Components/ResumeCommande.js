import React from 'react';
import BaseComponent from './BaseComponent';
import { StyleSheet, View, Image, Text, BackHandler, TextInput, TouchableOpacity,ScrollView,Alert,FlatList,Modal  } from 'react-native'
import stylesDefault from '../Styles/styleDefault'
import Loader from './Loader';
import CommandeItem from './PanierItem';
import { mileSeparator } from '../Helper/StandardHelper';
import { connect } from 'react-redux';
import { envoyerCommande } from '../API/ProduitAPI';



class ResumeCommande extends BaseComponent {

  _didFocusSubscription;
  _willBlurSubscription;

  constructor(props) {
    super(props);

    this._didFocusSubscription = props.navigation.addListener('didFocus', payload =>
      BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
    );

    this.state={
      loading:false
    }
  }

  _back(){
    this._startActivity("Panier");
  }

  _getPrixTotal(){
    let result = 0;

    const panier = this.props.panier;

    for(let i=0;i<panier.length;i++){
      result+=(panier[i].qte*panier[i].produit.prix);
    }

    return result;
  }

  _getNbProduitTotal(){
    let result = 0;

    const panier = this.props.panier;

    for(let i=0;i<panier.length;i++){
      result+=panier[i].qte;
    }

    return result;
  }

  _getCoutLivraison = ()=>{
    const panier = this.props.panier;
    let result = 0;

    for(let i=0;i<panier.length;i++){
      if(panier[i].produit.optionLivraison.etat==1){
        result+=panier[i].produit.optionLivraison.prix;
      }
    }

    return result;

  }

  _getCoutTotal(){
    let result = 0;

    result = this._getPrixTotal() + this._getCoutLivraison();

    return result;
  }

  componentDidMount() {
    this._willBlurSubscription = this.props.navigation.addListener('willBlur', payload =>
      BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
    );
  }

  onBackButtonPressAndroid = () => {
    this._back();
    return true;
  };

  componentWillUnmount() {
    this._didFocusSubscription && this._didFocusSubscription.remove();
    this._willBlurSubscription && this._willBlurSubscription.remove();
  }

  _askEnvoyerCommande(){
    Alert.alert(
      'Confirmation de l\'envoie',
      'Voulez-vous vraiment envoyer votre commande ?',
      [
        {text: 'OUI', onPress: () => this._envoyerCommande()},
        {text: 'NON', onPress: () => console.log('NON Pressed'),style: 'cancel'},
      ],
      { cancelable: true }
    );
  }

  _envoyerCommande = ()=>{

    this.setState({loading:true});

    this._getUserDataInStorage((err, result) => {
      if(result!=null){

        let data = {
          id_utilisateur:result[0][1],
          token:result[4][1],
          produits:[]
        };

        const panier = this.props.panier;

        for(let i=0;i<panier.length;i++){
          data.produits.push({
            id:panier[i].produit.id,
            prix_achat:panier[i].produit.prix,
            qte:panier[i].qte,
            livraison:panier[i].produit.optionLivraison.etat
          })
        }

        console.log(data);

        envoyerCommande(data).then(remoteData=>{

          console.log(remoteData);

          let status = remoteData.status;

          this.setState({loading:false});

          if(status==1){

            Alert.alert(
              'Commande envoyée',
              'Merci '+result[2][1]+' d\'avoir choisi Foodwise! Tu as officiellement contribué à la lutte contre le gâchis! Bravo! \n\nTa commande a été enregistrée avec succès et ton bon d\'achat pour chaque entreprise se trouve dans ta liste de commande ! \n\nN\'oublie pas de le montrer quand tu récupères ton produit.\n\nNe sois pas en retard sinon le commerce peut remettre son produit en vente sans que tu puisse récupérer ton argent.',
              [
                {text: 'Liste de commande', onPress: () => {
                  const action = { type: "CLEAR_PANIER", value: {} };
                  this.props.dispatch(action);

                  this._startActivity("ListeCommande",{src:"ListeProduit"});
                }},
                {text: 'OK', onPress: () => {
                  const action = { type: "CLEAR_PANIER", value: {} };
                  this.props.dispatch(action);

                  this._startActivity("ListeProduit");
                }}
              ],
              { cancelable: false }
            );

          }else if(status==0){
            this._startActivity("Login");
          }else if(status==7){

            Alert.alert(
              'Erreur',
              'Impossible d\'enregistrer la commande.',
              [
                {text: 'OK', onPress: () => {
                }}
              ],
              { cancelable: true }
            );

          }else if(status==-5){

            let strProduitInsuffisant = "La quantité en stock des produits suivants sont insuffisantes :\n";

            let data = remoteData.data;

            for(let i=0;i<data.length;i++){
              strProduitInsuffisant += "-"+data[i].nom+"(disponible:"+data[i].quantite+")\n";
            }

            strProduitInsuffisant +="Veuillez revoir votre commande.Merci!";

            Alert.alert(
              'Quantité en stock insuffisante',
              strProduitInsuffisant,
              [
                {text: 'OK', onPress: () => {
                }}
              ],
              { cancelable: true }
            );

          }else{

            Alert.alert(
              'Erreur',
              'Impossible d\'enregistrer la commande.',
              [
                {text: 'OK', onPress: () => {

                }}
              ],
              { cancelable: true }
            );

          }

        }).catch(error=>{
          console.log(error);
          Alert.alert(
            'Erreur',
            'Une erreur est survenue lors de l\'envoie de la commande.',
            [
              {text: 'OK', onPress: () => {

              }}
            ],
            { cancelable: true }
          );
        });

      }else{
        this.dataIsReady = false;
        this.setState({loading:false});
        console.log("erreur lecture storage");
      }
    });

  }

  /*<View style={{ backgroundColor:"#4f4f4f", height:1, marginLeft:4,marginRight:4, opacity:0.1,marginTop:4 }}></View>

  <View style={{ padding:4,marginTop:4 }}>

    <View style={{ flexDirection:"row" }}>

      <Text style={[stylesDefault.darkTextColor,{fontStyle:"italic",fontSize:16}]}>Coût des livraisons</Text>
      <Text style={[{ flex:1,fontWeight:"bold",fontSize:16,textAlign:"right" },stylesDefault.darkTextColor]}>{ mileSeparator(this._getCoutLivraison()) } MGA</Text>

    </View>

  </View>*/

  _renderResume(){
    return(
      <View >
        <View style={{ marginTop:4,marginBottom:8,marginLeft:10,marginRight:10 }}>

          <View style={{ padding:4,marginTop:4 }}>

            <View style={{ }}>

              <Text style={[stylesDefault.darkTextColor,{fontSize:16,fontFamily:"Barlow_Italic"}]}>Prix total des produits commandés</Text>
              <Text style={[{ fontSize:16,textAlign:"right",fontFamily:"Barlow_BlackItalic" },stylesDefault.textThemePrimary]}>{ mileSeparator(this._getPrixTotal()) } Ar</Text>

            </View>

          </View>

          <View style={[{ height:1,opacity:0.5, marginLeft:4,marginRight:4,marginTop:4 },stylesDefault.themeBlue]}></View>

          <View style={{ padding:4,marginTop:4 }}>

            <View style={{  }}>

              <Text style={[stylesDefault.darkTextColor,{fontSize:16,fontFamily:"Barlow_Italic"}]}>Nombre total de produits commandés</Text>
              <Text style={[{ fontSize:16,textAlign:"right",fontFamily:"Barlow_BlackItalic" },stylesDefault.textThemePrimary]}>{ mileSeparator(this._getNbProduitTotal()) }</Text>

            </View>

          </View>

          <View style={[{ height:1,opacity:0.5, marginLeft:4,marginRight:4,marginTop:4 },stylesDefault.themeBlue]}></View>



        </View>

        <View style={[{ padding:8,alignItems:"center" },stylesDefault.themePrimary]}>

          <View style={{ flexDirection:"row",alignItems:"center",marginLeft:8,marginRight:8 }}>

            <Text style={[{fontSize:16,fontFamily:"Barlow_BlackItalic"},stylesDefault.textThemeYellow]}>Total à payer</Text>
            <Text style={[{ flex:1,fontSize:16,textAlign:"right",fontFamily:"Barlow_BlackItalic" },stylesDefault.textThemeYellow]}>{ mileSeparator(this._getCoutTotal()) } MGA</Text>

          </View>

        </View>

      </View>
    )
  }

  _renderListeZone(){

    return (
      <View style={{ flex:1,borderBottomWidth:1,borderTopWidth:1,borderColor:"#00000015" }}>
        <FlatList
          style={{ flex:1 }}
          data={this.props.panier}
          keyExtractor={(item) => item.produit.id.toString()}
          renderItem={({item}) => (
            <CommandeItem
              panierItem={item}
              showDetailProduit = { this._showDetailProduit }
              toggleFavoris = { this._toggleFavoris }
              removeFromPanier = { this._removeFromPanier }
              editPanier = { this._editPanier }
              editable = {false}
            />
          )}
          onEndReachedThreshold={0.1}
        />
      </View>
    )
  }

  render(){
    return(
      <View style={{ flex:1,backgroundColor:"#ffffff" }}>

        <View ref="actionBar" style={[{
          borderBottomWidth:1,borderBottomColor:"#00000012",flexDirection:"row",paddingTop:8,paddingBottom:8,alignItems:"center",justifyContent:"center" },stylesDefault.themePrimary]}>

          <TouchableOpacity style={{ padding:10 }} onPress={()=>{this._back()}}>
            <Image source={require("../Images/other/left_arrow.png")} style={{ height:20, width:20, resizeMode:Image.resizeMode.contain,tintColor:"#fff" }} />
          </TouchableOpacity>

          <Text style={{ flex:1,fontSize:20,color:"#fff",fontFamily:"Barlow_Bold" }}>
            Résumé de la commande
          </Text>

        </View>

        <Loader loading={this.state.loading} />

        { this._renderResume() }

        { this._renderListeZone() }

        <View style={{ alignItems:"center",marginBottom:8,marginTop:8 }}>

          <TouchableOpacity style={[stylesDefault.fw_btn,{opacity:(this.props.panier.length==0)?0.5:1}]} onPress={()=>{ this._askEnvoyerCommande() }} >
            <Text style={stylesDefault.fw_btn_txt}>{"Envoyer ma commande"}</Text>
            <Image style={{ marginLeft:8,width:15,height:15,resizeMode:Image.resizeMode.contain,tintColor:"#ffffff" }} source={require('../Images/other/right_arrow.png')} />
          </TouchableOpacity>

        </View>

      </View>
    )
  }

}

const mapStateToProps = (state) => {
  return state
}

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: (action) => { dispatch(action) }
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(ResumeCommande);
