import React from 'react';
import BaseComponent from './BaseComponent';
import { StyleSheet, View, Image, Text, BackHandler, TextInput, TouchableOpacity,ScrollView,Alert,FlatList,Modal  } from 'react-native'
import stylesDefault from '../Styles/styleDefault'
import Loader from './Loader';
import DetailCommandeItem from './PanierItem';
import { mileSeparator } from '../Helper/StandardHelper';
import { connect } from 'react-redux';
import { getDetailCommande } from '../API/ProduitAPI';



class DetailCommande extends BaseComponent {

  _didFocusSubscription;
  _willBlurSubscription;

  constructor(props) {
    super(props);

    this.dataIsReady = false;

    this._didFocusSubscription = props.navigation.addListener('didFocus', payload =>
      BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
    );

    this.state={
      loading:false,
      commande:{
        id_commande:0,
        etat:"...",
        montant_total:0,
        nombre_de_produits:0,
        cout_livraison:0,
        date:"../../....",
        produits:[]
      }
    }
  }

  _back(){
    this._startActivity("ListeCommande");
  }

  _getPrixTotal(){
    let result = 0;

    return result;
  }

  _getNbProduitTotal(){
    let result = 0;

    return result;
  }

  _getCoutLivraison = ()=>{
    let result = 0;

    return result;

  }

  _getCoutTotal(){
    return this.state.commande.cout_livraison+this.state.commande.montant_total;
  }

  componentDidMount() {
    this._willBlurSubscription = this.props.navigation.addListener('willBlur', payload =>
      BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
    );
  }

  onBackButtonPressAndroid = () => {
    this._back();
    return true;
  };

  componentWillUnmount() {
    this._didFocusSubscription && this._didFocusSubscription.remove();
    this._willBlurSubscription && this._willBlurSubscription.remove();
  }

  _checkData(){
    console.log("DetailCommande","dataIsReady : "+this.dataIsReady);
    if(!this.dataIsReady && this._inDetailCommande()){
      this.dataIsReady = true;
      this._getDetailCommande();
    }
  }

  _getDetailCommande(){
    let self = this;

    this.setState({loading:true});

    getDetailCommande({token:this.props.navigation.state.params.token,id_utilisateur:this.props.navigation.state.params.userId,commande_id:this.props.navigation.state.params.commande_id}).then(data=>{

        console.log(data);

        let status = data.status;

        if(status==1){

            this.setState({
              loading:false,
              commande:data.data
            });

        }else if(status==0){
          Alert.alert(
            'Session expirée',
            'Votre session a expiré.Veuillez vous reconnecter!',
            [
              {text: 'Ok', onPress: () => {
                this.setState({loading:false});
                self._logout();
              }, style:"cancelable"}
            ],
            { cancelable: false }
          );
        }else{
          this.setState({loading:false});
          Alert.alert(
            'Connexion interrompue',
            'Une erreur est survenue lors de la récupération des détails de votre commande',
            [
              {text: 'Réessayer', onPress: () => {
                self.dataIsReady = false;
                self._checkData();
              }, style:"cancelable"},
            ],
            { cancelable: false }
          );
        }

    }).catch(error=>{
      console.log(error);
      this.setState({loading:false});
      Alert.alert(
        'Connexion interrompue',
        'Une erreur est survenue lors de la récupération des détails de votre commande',
        [
          {text: 'Réessayer', onPress: () => {
            self.dataIsReady = false;
            self._checkData();
          }, style:"cancelable"},
        ],
        { cancelable: false }
      );
    });
  }

  /*<View style={{ backgroundColor:"#4f4f4f", height:1, marginLeft:4,marginRight:4, opacity:0.1,marginTop:4 }}></View>

  <View style={{ padding:4,marginTop:4 }}>

    <View style={{ flexDirection:"row" }}>

      <Text style={[stylesDefault.darkTextColor,{fontStyle:"italic",fontSize:16}]}>Coût de livraison</Text>
      <Text style={[{ flex:1,fontWeight:"bold",fontSize:16,textAlign:"right" },stylesDefault.darkTextColor]}>{ mileSeparator(this.state.commande.cout_livraison) } MGA</Text>

    </View>

  </View>*/

  _renderResume(){
    return(
      <View >
        <View style={{ marginTop:4,marginBottom:8,marginLeft:10,marginRight:10 }}>

          <View style={{ padding:4,marginTop:4 }}>

            <View style={{ }}>

              <Text style={[stylesDefault.darkTextColor,{fontSize:16,fontFamily:"Barlow_Italic"}]}>Date</Text>
              <Text style={[{ fontSize:16,textAlign:"right",fontFamily:"Barlow_BlackItalic" },stylesDefault.textThemePrimary]}>{ this.state.commande.date } à { this.state.commande.heure }</Text>

            </View>

          </View>

          <View style={[{ height:1,opacity:0.5, marginLeft:4,marginRight:4,marginTop:4 },stylesDefault.themeBlue]}></View>


          <View style={{ padding:4,marginTop:4 }}>

            <View style={{ }}>

              <Text style={[stylesDefault.darkTextColor,{fontSize:16,fontFamily:"Barlow_Italic"}]}>Prix total des produits commandés</Text>
              <Text style={[{ fontSize:16,textAlign:"right",fontFamily:"Barlow_BlackItalic" },stylesDefault.textThemePrimary]}>{ mileSeparator(this.state.commande.montant_total) } Ar</Text>

            </View>

          </View>

          <View style={[{ height:1,opacity:0.5, marginLeft:4,marginRight:4,marginTop:4 },stylesDefault.themeBlue]}></View>

          <View style={{ padding:4,marginTop:4 }}>

            <View style={{  }}>

              <Text style={[stylesDefault.darkTextColor,{fontSize:16,fontFamily:"Barlow_Italic"}]}>Nombre total de produits commandés</Text>
              <Text style={[{ fontSize:16,textAlign:"right",fontFamily:"Barlow_BlackItalic" },stylesDefault.textThemePrimary]}>{ mileSeparator(this.state.commande.nombre_de_produits) }</Text>

            </View>

          </View>

          <View style={[{ height:1,opacity:0.5, marginLeft:4,marginRight:4,marginTop:4 },stylesDefault.themeBlue]}></View>



        </View>

        <View style={[{ padding:8,alignItems:"center" },stylesDefault.themePrimary]}>

          <View style={{ flexDirection:"row",alignItems:"center",marginLeft:8,marginRight:8 }}>

            <Text style={[{fontSize:16,fontFamily:"Barlow_BlackItalic"},stylesDefault.textThemeYellow]}>Etat</Text>
            <Text style={[{ flex:1,fontSize:16,textAlign:"right",fontFamily:"Barlow_BlackItalic" },stylesDefault.textThemeYellow]}>{ this.state.commande.etat }</Text>

          </View>

        </View>

      </View>
    )
  }

  _renderListeZone(){

    return (
      <View style={{ flex:1,borderBottomWidth:1,borderTopWidth:1,borderColor:"#00000015" }}>
        <FlatList
          style={{ flex:1 }}
          data={this.state.commande.produits}
          keyExtractor={(item) => item.id_donation.toString()}
          renderItem={({item}) => (
            <DetailCommandeItem
              panierItem={{produit:item,qte:this.state.commande.nombre_de_produits}}
              showDetailProduit = { this._showDetailProduit }
              toggleFavoris = { this._toggleFavoris }
              removeFromPanier = { this._removeFromPanier }
              editPanier = { this._editPanier }
              editable = {false}
            />
          )}
          onEndReachedThreshold={0.1}
        />
      </View>
    )
  }

  render(){
    this._checkData();

    return(
      <View style={{ flex:1,backgroundColor:"#ffffff" }}>

      <View ref="actionBar" style={[{
        borderBottomWidth:1,borderBottomColor:"#00000012",flexDirection:"row",paddingTop:8,paddingBottom:8,alignItems:"center",justifyContent:"center" },stylesDefault.themePrimary]}>

        <TouchableOpacity style={{ padding:10 }} onPress={()=>{this._back()}}>
          <Image source={require("../Images/other/left_arrow.png")} style={{ height:20, width:20, resizeMode:Image.resizeMode.contain,tintColor:"#fff" }} />
        </TouchableOpacity>

        <Text style={{ flex:1,fontSize:20,color:"#fff",fontFamily:"Barlow_Bold" }}>
          Détail de la commande
        </Text>

      </View>

        <Loader loading={this.state.loading} />

        { this._renderResume() }

        { this._renderListeZone() }


      </View>
    )
  }

}

const mapStateToProps = (state) => {
  return state
}

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: (action) => { dispatch(action) }
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(DetailCommande);
