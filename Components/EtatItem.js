import React from 'react'
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native'


class EtatItem extends React.Component {

  render(){
    const { etatItem } = this.props;

    return(
      <TouchableOpacity style={[styles.main_container,{backgroundColor:(etatItem.ordre%2==0)?"#00000000":"#4bae3215"}]} onPress={ ()=> console.log("omeo detail") }>

        <Text style={{ flex:1,paddingTop:4,paddingBottom:4,color:"#636e72",fontSize:14,marginLeft:16 }}>
          { etatItem.ordre }#  { etatItem.label }
        </Text>

        <Text style={{ flex:1,textAlign:"center",paddingTop:4,paddingBottom:4,color:"#636e72",fontSize:14 }}>
          { etatItem.heure }
        </Text>

      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  main_container: {
    flexDirection: 'row',
    backgroundColor: '#FFFFFF',
    borderBottomWidth:1,
    borderColor:"#00000005",
    alignItems:"center",
    paddingTop:6,
    paddingBottom:6
  },
  title_text: {
    flex:1,
    fontSize:20,
    fontWeight: 'bold',
    flexWrap : 'wrap',
    color:"#4f4f4f"
  },
  favorite_image: {
      width: 40,
      height: 40

  }
})

export default EtatItem;
