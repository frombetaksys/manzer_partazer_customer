import React from 'react'
import BaseComponent from './BaseComponent';
import { StyleSheet, View, Image, Text, BackHandler, TextInput, TouchableHighlight,KeyboardAvoidingView, ScrollView,Alert,AsyncStorage } from 'react-native'
import { logIn,createAccount } from '../API/UtilisateurAPI';
import Loader from './Loader';
import { connect } from 'react-redux';
import { LoginManager,GraphRequest, GraphRequestManager, AccessToken } from 'react-native-fbsdk';
import { GoogleSignin, GoogleSigninButton } from 'react-native-google-signin';
import LoginModal from './LoginModal';
import stylesDefault from '../Styles/styleDefault'


class Login extends BaseComponent {
  _didFocusSubscription;
  _willBlurSubscription;


  constructor(props) {
    super(props);
    this._didFocusSubscription = props.navigation.addListener('didFocus', payload =>
      BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
    );

    this.state={
      id:"",
      nomLogin:"",
      prenom:"",
      emailLogin:"",
      passwordLogin:"",
      loading:false,
      showLoginModal:false,
      fb_text_color:"#fff",
      fb_icon_color:"#fff",
      sign_text_color:"#00b2a9",
      log_text_color:"#00b2a9",
    };
  }

  _signUp(){
    this._startActivity("Inscription");
  }

  _signInWithFB(){
    let self = this;
    LoginManager.logInWithReadPermissions(['public_profile']).then(
      function(result) {
        if (result.isCancelled) {
          console.log('Login cancelled');
        } else {
          console.log('Login success with permissions: '+result.grantedPermissions.toString());

          AccessToken.getCurrentAccessToken().then(
            (data) => {
              let accessToken = data.accessToken

              responseInfoCallback = (error, result) => {
                if (error) {
                  console.log(error);
                } else {
                  console.log(result)
                  self._enregistrerInfoSurServeur(result,"facebook");
                }
              }

              const infoRequest = new GraphRequest('/me',
                {
                  accessToken: accessToken,
                  parameters: {
                    fields: {
                      string: 'email,name,first_name,middle_name,last_name'
                    }
                  }
                },responseInfoCallback);
              // Start the graph request.
              new GraphRequestManager().addRequest(infoRequest).start()

            });

        }
      },
      function(error) {
        console.log('Login fail with error: ' + error);
      }
    );
  }

  _configureGoogleSignIn() {
    GoogleSignin.configure({
      webClientId: "249167550160-3d6t4l81go2adu5ioqitt8ue6ncc8hjm.apps.googleusercontent.com",
      offlineAccess: false,
    });
  }

  _signInWithGoogle = async () => {
    let self = this;
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      console.log(userInfo);
      self._enregistrerInfoSurServeur(userInfo.user,"google");
    } catch (error) {
      console.log(error);
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // sign in was cancelled
        console.log('cancelled');
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation in progress already
        console.log('in progress');
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        console.log('play services not available or outdated');
      } else {
        console.log('Something went wrong : '+error.toString());
      }
    }
  };

  _enregistrerInfoSurServeur = (info,type)=>{
    this.setState({loading:true});

    if(type==="facebook"){

      createAccount(info.last_name,info.first_name,info.id+"@facebook.com",info.id).then(data=>{

        console.log(data);

        this.setState({
          emailLogin:info.id+"@facebook.com",
          passwordLogin:info.id,
          loading:false
        },()=>{
            this._signIn(this.state.emailLogin,this.state.passwordLogin);
        });

      }).catch(error=>{
        console.log(error);
      })

    }else if(type==="google"){

      createAccount(info.familyName,info.givenName,info.email,info.id).then(data=>{

        console.log(data);

        this.setState({
          emailLogin:info.email,
          passwordLogin:info.id,
          loading:false
        },()=>{
            this._signIn(this.state.emailLogin,this.state.passwordLogin);
        });


      }).catch(error=>{
        console.log(error);
      })

    }

  }

  _signIn = (emailLogin,passwordLogin) => {


    if(emailLogin.length>0 && passwordLogin.length>0){
      this.setState({loading:true});
      logIn(emailLogin,passwordLogin).then(data=>{
        console.log(data);

        if(data.status==-1 || data.status==-3){
          Alert.alert(
            'Information incorrect',
            'E-mail ou mot de passe incorrect.',
            [
              {text: 'OK', onPress: () => console.log('OK Pressed'),style: 'cancel'},
            ],
            { cancelable: true }
          );
        }else if(data.status==1){
          this._saveUserInfo(data.data);
          AsyncStorage.multiSet([['consommateurId',''+data.data.user.id_consommateur],['userId',''+data.data.user.utilisateur_id],['userName',''+data.data.user.nom],['userFirstName',''+data.data.user.prenom],['userEmail',''+data.data.user.mail],['userToken',''+data.data.token]],(err)=>{
            console.log(err);
            if(err){
                console.log(err);
            }else{
              this.setState({loading:false,showLoginModal:false});
              this._startActivity("AuthentificationSocialNetwork",{name:data.data.user.prenom});
            }
          });
          // this._storeUserData(data.data.user.utilisateur_id,data.data.user.nom,data.data.user.prenom,data.data.user.mail,data.data.token).then((result)=>{
          //     this._startActivity("Profil");
          // });
        }

        this.setState({
          nomLogin:"",
          passwordLogin:"",
          loading:false
        });

      }).catch(error=>{
        console.log("LogInError",error);
        this.setState({
          loading:false
        },()=>{
          Alert.alert(
  					'Connexion interrompue',
  					'Problème de connexion avec le serveur',
  					[
  						{text: 'Ok', onPress: () => {}, style:"cancelable"},
  					],
  					{ cancelable: false }
  				);
        })

      })
    }else{
      Alert.alert(
        'Information incomplète',
        'Veuillez renseigner vos informations d\'identification',
        [
          {text: 'OK', onPress: () => console.log('OK Pressed'),style: 'cancel'},
        ],
        { cancelable: true }
      );
    }

  }

  componentDidUpdate() {

  }

  _saveUserInfo(data){
    console.log("_saveUserInfo.....");
    const action = { type: "SET_INFO", value: {idUser:data.user.utilisateur_id,token:data.token,userName:data.nom,userFirstName:data.prenom,email:data.mail} };
    this.props.dispatch(action);

    // const action2 = { type: "SET_ACTIVITY", value: {activity:"login"} };
    // this.props.dispatch(action2);
  }

  _back(){
    Alert.alert(
    'Quitter l\'application',
    'Voulez vous fermer l\'application?',
    [
      {text: 'Non', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
      {text: 'Oui', onPress: () => BackHandler.exitApp()},
    ],
    { cancelable: false });
    return true;
  }

  componentDidMount() {
    this._configureGoogleSignIn();
    this._willBlurSubscription = this.props.navigation.addListener('willBlur', payload =>
      BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
    );
  }

  onBackButtonPressAndroid = () => {
    this._back();
    return true;
  };

  componentWillUnmount() {
    this._didFocusSubscription && this._didFocusSubscription.remove();
    this._willBlurSubscription && this._willBlurSubscription.remove();
  }

  _storeUserData = async (userId,userName,userFirstName,userEmail,token) => {
    try {
      await AsyncStorage.multiSet([['userId','userName','userFirstName','userEmail','userToken'],[''+userId,''+userName,''+userFirstName,''+userEmail,''+token]]);
      // await AsyncStorage.setItem('userName', ''+userName);
      // await AsyncStorage.setItem('userFirstName', ''+userFirstName);
      // await AsyncStorage.setItem('userEmail', ''+userEmail);
      // await AsyncStorage.setItem('userToken', ''+token);
    } catch (error) {
      // Error saving data
      console.log(error);
    }
  }

  /*<View style={{ marginTop:8 }}>

    <TouchableHighlight style={styles.button_google_log_in} onPress={ this._signInWithGoogle }>
      <Image style={{ width:25,height:25,resizeMode:Image.resizeMode.contain,marginRight:8 }} source={require('../Images/google_icon.png')} />
      <Text style={ {fontWeight:"bold",color:"#000000"} }>{"Connexion avec Google"}</Text>
    </TouchableHighlight>

  </View>*/

  _toggleSignUpButtonAppearance(state){
    if(state==0){
      this.setState({sign_text_color:"#00b2a9"})
    }else{
      this.setState({sign_text_color:"#fff"});
    }
  }

  _toggleLogInButtonAppearance(state){
    if(state==0){
      this.setState({log_text_color:"#00b2a9"})
    }else{
      this.setState({log_text_color:"#fff"});
    }
  }

  _toggleFBButtonAppearance(state){
    if(state==0){
      this.setState({fb_text_color:"#fff",fb_icon_color:"#fff"})
    }else{
      this.setState({fb_text_color:"#4267B2",fb_icon_color:"#4267B2"});
    }
  }

  render() {
      return (
        <KeyboardAvoidingView style={{flex:1}} >
          <Loader loading={this.state.loading} />
          <View style={[styles.main_container,stylesDefault.themePrimary]}>
            <View style={styles.logo_container}>
              <Image style={styles.logo} source={require('../Images/logo.png')} />
            </View>
            <View style={styles.form_container}>

              <View style={styles.button_container}>

                <TouchableHighlight underlayColor={"#00b2a9"} style={[styles.button_sign_up,{marginRight:8}]} onPress={()=>{ this._startActivity("Inscription") }} onShowUnderlay={ ()=>{ this._toggleSignUpButtonAppearance(1) } } onHideUnderlay={ ()=>{ this._toggleSignUpButtonAppearance(0) } } >
                  <Text style={[{color:this.state.sign_text_color},styles.button_sign_up_text,stylesDefault.barlowRegular]}>{"Je suis nouveau"}</Text>
                </TouchableHighlight>

                <TouchableHighlight underlayColor={"#00b2a9"} style={styles.button_sign_up} onPress={()=>{ this._startActivity("Authentification") }} onShowUnderlay={ ()=>{ this._toggleLogInButtonAppearance(1) } } onHideUnderlay={ ()=>{ this._toggleLogInButtonAppearance(0) } }>
                  <Text style={[{color:this.state.log_text_color},styles.button_sign_up_text,stylesDefault.barlowRegular]}>{"Déjà inscrit ?"}</Text>
                </TouchableHighlight>

              </View>

              <View style={{ marginBottom:64,marginTop:4 }}>
                <TouchableHighlight underlayColor={"#fff"} style={styles.button_fb_log_in} onPress={()=>{ this._signInWithFB() }} onShowUnderlay={ ()=>{ this._toggleFBButtonAppearance(1) } } onHideUnderlay={ ()=>{ this._toggleFBButtonAppearance(0) } }>
                  <View style={{flexDirection:"row",alignItems:"center"}}>

                    <Image style={{ width:27,height:27,resizeMode:Image.resizeMode.contain,tintColor:this.state.fb_icon_color }} source={require('../Images/fb_icon.png')} />

                    <Text style={[{color:this.state.fb_text_color},styles.button_log_in_text,stylesDefault.barlowRegular]}>Se connecter avec <Text style={stylesDefault.barlowBold}>facebook</Text></Text>
                  </View>
                </TouchableHighlight>
              </View>

            </View>

            <LoginModal
              close={()=>{ this.setState({showLoginModal:false}) }}
              login={ this._signIn }
              show={ this.state.showLoginModal }
            />

          </View>
        </KeyboardAvoidingView>
      )
  }
}

const styles = StyleSheet.create({
  main_container:{
    flex:1
  },
  logo_container:{
    flex:1,
    alignItems:"center",
    justifyContent:"flex-end",
    paddingBottom:50,
    paddingTop:50
  },
  logo:{
    width:128,
    height:128,
    resizeMode:Image.resizeMode.center
  },
  form_container:{
    paddingLeft:24,
    paddingRight:24,
    flex:1,
    justifyContent:"flex-end"
  },
  form_input:{
    flexDirection:"row",
    borderColor:"#dce1e5",
    borderWidth:2,
    borderRadius:10,
    overflow:"hidden",
    marginBottom:16
  },
  icon:{
    width:20,
    height:20,
    resizeMode:Image.resizeMode.contain
  },
  text_input:{
    flex:1,
    backgroundColor:"#ffffff"
  },
  input:{
    paddingBottom:12,
    paddingTop:12,
    paddingLeft:24,
    paddingRight:8,
    fontSize:16
  },
  icon_input:{
    paddingLeft:20,
    paddingRight:20,
    justifyContent:"center",
    backgroundColor:"#00AFAA"
  },
  button_container:{
    flexDirection:"row",
    marginTop:8,
    marginBottom:20
  },
  button_sign_up:{
    flex:1,
    alignItems:"center",
    justifyContent:"center",
    borderColor:"#ffffff",
    borderWidth:2,
    borderRadius:100,
    overflow:"hidden",
    paddingTop:12,
    paddingBottom:12,
    backgroundColor:"#ffffff"
  },
  button_log_in:{
    flex:1,
    backgroundColor:"#00AFAA",
    borderColor:"#dce1e5",
    borderWidth:2,
    borderRadius:10,
    overflow:"hidden",
    paddingTop:16,
    paddingBottom:16,
    alignItems:"center",
    marginLeft:4
  },
  button_fb_log_in:{
    flex:1,
    flexDirection:"row",
    backgroundColor:"#4267B2",
    borderColor:"#4267B2",
    borderRadius:100,
    overflow:"hidden",
    paddingLeft:22,
    paddingTop:26,
    paddingBottom:26,
    alignItems:"center",
  },
  button_google_log_in:{
    flex:1,
    flexDirection:"row",
    alignItems:"center",
    justifyContent:"center",
    backgroundColor:"#ffffff",
    borderColor:"#dce1e5",
    borderWidth:2,
    borderRadius:10,
    overflow:"hidden",
    paddingTop:28,
    paddingBottom:28,
    alignItems:"center",
    marginLeft:4
  },
  button_log_in_text:{
    fontSize:16,
    flex:1,
    alignItems:"center",
    justifyContent:"center",
    textAlign:"center",
    paddingRight:2
  },
  button_sign_up_text:{
    fontSize:16
  }
});

const mapStateToProps = (state) => {
  return state
}

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: (action) => { dispatch(action) }
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(Login);
