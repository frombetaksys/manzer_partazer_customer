import React from 'react';
import { View,Text,TouchableOpacity,StyleSheet,TextInput,Image } from 'react-native';
import Modal from "react-native-modal";

class LoginModal extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      email:"",
      password:""
    }
  }

  _close(){
    this.props.close();
  }

  _login(){
    this.refs.passwordLogin.clear();
    this.props.login(this.state.email,this.state.password);
  }

  render(){
    return(
      <Modal isVisible={ this.props.show }>
        <View style={{ flex: 1,alignItems:"center",justifyContent:"center" }}>
          <View style={{ backgroundColor:"#ffffff",borderRadius:4,padding:16 }}>

            <View style={{ alignItems:"center",justifyContent:"center" }}>
              <Text style={{ textAlign:"center",fontSize:18,color:"#636e72" }}>Veuillez-vous identifier</Text>
            </View>

            <View style={{ backgroundColor:"#00000025", height:1, opacity:0.5,marginTop:8,marginBottom:16 }}></View>

            <View style={styles.form_container}>

              <View style={styles.form_input}>
                <View style={styles.text_input}>
                  <TextInput underlineColorAndroid="#ffffff" style={styles.input} placeholder={"E-MAIL"} onChangeText={(text)=>this.setState({email:text})} />
                </View>
                <View style={styles.icon_input}>
                  <Image style={styles.icon} source={require('../Images/icon_email.png')} />
                </View>
              </View>

              <View style={styles.form_input}>

                <View style={styles.text_input}>
                  <TextInput ref="passwordLogin" underlineColorAndroid="#ffffff" style={styles.input} placeholder={"MOT DE PASSE"} onChangeText={(text)=> this.setState({password:text})} secureTextEntry={true}/>
                </View>

                <View style={styles.icon_input}>
                  <Image style={styles.icon} source={require('../Images/icon_lock.png')} />
                </View>

              </View>

            </View>

            <View style={{ flexDirection:"row",alignItems:"center",justifyContent:"center" }}>

              <TouchableOpacity style={{flexDirection:"row",alignItems:"center",justifyContent:"center",backgroundColor:"#dfe6e9",borderColor:"#dce1e5",borderWidth:2,borderRadius:10,paddingTop:10,paddingBottom:10,paddingLeft:10,paddingRight:10,marginRight:8}} onPress={()=>{ this._close() }}>
                <Text style={{fontSize:16,fontWeight:"bold",color:"#636e72"}}>ANNULER</Text>
              </TouchableOpacity>

              <TouchableOpacity style={{flexDirection:"row",alignItems:"center",justifyContent:"center",backgroundColor:"#00AFAA",borderColor:"#dce1e5",borderWidth:2,borderRadius:10,paddingTop:10,paddingBottom:10,paddingLeft:10,paddingRight:10,marginLeft:8}} onPress={()=>{ this._login() }}>
                <Text style={{fontSize:16,fontWeight:"bold",color:"#ffffff"}}>SE CONNECTER</Text>
              </TouchableOpacity>

            </View>
          </View>
        </View>
      </Modal>
    )
  }

}

const styles = StyleSheet.create({
  form_container:{
  },
  form_input:{
    flexDirection:"row",
    borderColor:"#dce1e5",
    borderWidth:2,
    borderRadius:10,
    overflow:"hidden",
    marginBottom:16
  },
  text_input:{
    flex:1,
    backgroundColor:"#ffffff"
  },
  input:{
    justifyContent: "flex-start",
    paddingBottom:12,
    paddingTop:12,
    paddingLeft:24,
    paddingRight:8,
    fontSize:16
  },
  icon:{
    width:20,
    height:20,
    resizeMode:Image.resizeMode.contain
  },
  icon_input:{
    paddingLeft:20,
    paddingRight:20,
    justifyContent:"center",
    backgroundColor:"#00AFAA"
  },
});

export default LoginModal;
