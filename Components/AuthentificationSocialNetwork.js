import React from 'react'
import { StyleSheet, View, Image, Text, BackHandler, TextInput, TouchableOpacity,KeyboardAvoidingView, ScrollView,Alert,AsyncStorage,TouchableHighlight } from 'react-native'
import { logIn,createAccount } from '../API/UtilisateurAPI';
import Loader from './Loader';
import stylesDefault from "../Styles/styleDefault";
import { Dimensions } from 'react-native';
import BaseComponent from './BaseComponent';
import { connect } from 'react-redux';



const win = Dimensions.get('window');
const iW = 2309;
const iH = 874;
const ratio = win.width/iW;


class AuthentificationSocialNetwork extends BaseComponent {
  _didFocusSubscription;
  _willBlurSubscription;


  constructor(props) {
    super(props);

    this.state={
      id:"",
      nom:"",
      prenom:"",
      email:"",
      password:"",
      loading:false,
      nextSrc:require('../Images/icon/next.png'),
      checkBox:require('../Images/icon/checkbox_empty.png'),
      termsAccepted:false
    };

  }

  _signUp(){

  }

  _signInWithFB(){

  }

  _configureGoogleSignIn() {

  }

  _signInWithGoogle = async () => {

  };

  _enregistrerInfoSurServeur = (info,type)=>{

  }

  _signIn(){


  }

  componentDidUpdate() {

  }

  _saveUserInfo(data){

  }

  componentDidMount() {

  }

  onBackButtonPressAndroid = () => {
    return true;
  };

  componentWillUnmount() {

  }

  _storeUserData = async (userId,userName,userFirstName,userEmail,token) => {

  }

  _acceptTerms(){
    if(this.state.termsAccepted){
      this.setState({
        termsAccepted:false,
        checkBox:require('../Images/icon/checkbox_empty.png')
      });
    }else{
      this.setState({
        termsAccepted:true,
        checkBox:require('../Images/icon/checkbox.png')
      });
    }
  }

  _continue(){
    if(this.state.termsAccepted){
      this._startActivity("Welcome",{name:this.props.navigation.state.params.name});
    }else{
      Alert.alert(
        'Oups',
        "Avant de procéder,il faut d'abord remplir toutes les informations. Merci!",
        [
          {text: 'OK', onPress: () => console.log('OK Pressed'),style: 'cancel'},
        ],
        { cancelable: true }
      );
    }
  }

  render() {
      return (
        <KeyboardAvoidingView style={{flex:1}} >
          <Loader loading={this.state.loading} />
          <View style={[styles.main_container,stylesDefault.themePrimary]} keyboardShouldPersistTaps="handled">

            <View style={{ marginTop:60,paddingLeft:16,paddingRight:16 }}>
              <Text style={[{ fontSize:64,color:"#fff" },stylesDefault.barlowBlackItalic]}>Salama</Text>
            </View>

            <View style={{ flexDirection:"row" }}>
              <Text style={[{ flex:1,color:"#fff",fontSize:22,paddingLeft:150 },stylesDefault.barlowRegular]} > { this.props.navigation.state.params.name } !</Text>
            </View>

            <View style={{ marginTop:32 }}>
              <Image style={{ width:188,height:256,resizeMode:Image.resizeMode.contain }} source={require("../Images/main.png")}/>
            </View>

            <View style={{ flex:1,flexDirection:"row",alignItems:"center",paddingLeft:16,paddingRight:16 }}>
              <TouchableOpacity onPress={()=>{ this._acceptTerms() }}>
                <Image style={{ width:32,height:32,resizeMode:Image.resizeMode.contain,marginRight:8 }} source={this.state.checkBox} />
              </TouchableOpacity>
              <Text style={{ flex:1,color:"#fff",fontSize:16 }}>
                J'accepte les <Text style={{ textDecorationLine:"underline" }}>Termes et Conditions</Text> et la <Text style={{ textDecorationLine:"underline" }}>politique de confidentialité</Text>
              </Text>
            </View>

          </View>
          <TouchableOpacity style={{position:"absolute",bottom:16,right:16}} onPress={()=>{ this._continue() }} >
            <Image fadeDuration={10} style={{ width:40,height:40,resizeMode:Image.resizeMode.contain }} source={this.state.nextSrc} />
          </TouchableOpacity>
        </KeyboardAvoidingView>
      )
  }
}

const styles = StyleSheet.create({
  main_container:{
    flex:1
  },
  logo_container:{
    flex:1,
    alignItems:"center",
    justifyContent:"flex-end",
    paddingBottom:50,
    paddingTop:50
  },
  logo:{
    width:100,
    height:100
  },
  form_input:{
    flexDirection:"row",
    overflow:"hidden",
    marginBottom:16
  },
  icon:{
    width:20,
    height:20,
    resizeMode:Image.resizeMode.contain
  },
  text_input:{
    flex:1,
    borderBottomColor:"#fff"
  },
  input:{
    color:"#fff",
    paddingBottom:12,
    paddingTop:12,
    paddingLeft:4,
    paddingRight:8,
    fontSize:18
  },
  icon_input:{
    paddingLeft:20,
    paddingRight:20,
    justifyContent:"center",
    backgroundColor:"#4bae32"
  },
  button_container:{
    flexDirection:"row",
    marginTop:16,
    marginBottom:20
  },
  button_sign_up:{
    flex:1,
    alignItems:"center",
    justifyContent:"center",
    borderColor:"#dce1e5",
    borderWidth:2,
    borderRadius:10,
    overflow:"hidden",
    paddingTop:16,
    paddingBottom:16,
    marginRight:4,
    backgroundColor:"#ffffff"
  },
  button_log_in:{
    flex:1,
    backgroundColor:"#4bae32",
    borderColor:"#dce1e5",
    borderWidth:2,
    borderRadius:10,
    overflow:"hidden",
    paddingTop:16,
    paddingBottom:16,
    alignItems:"center",
    marginLeft:4
  },
  button_fb_log_in:{
    flex:1,
    flexDirection:"row",
    alignItems:"center",
    justifyContent:"center",
    backgroundColor:"#4267B2",
    borderColor:"#dce1e5",
    borderWidth:2,
    borderRadius:10,
    overflow:"hidden",
    paddingTop:16,
    paddingBottom:16,
    alignItems:"center",
    marginLeft:4
  },
  button_google_log_in:{
    flex:1,
    flexDirection:"row",
    alignItems:"center",
    justifyContent:"center",
    backgroundColor:"#ffffff",
    borderColor:"#dce1e5",
    borderWidth:2,
    borderRadius:10,
    overflow:"hidden",
    paddingTop:16,
    paddingBottom:16,
    alignItems:"center",
    marginLeft:4
  },
  button_log_in_text:{
    fontWeight:"bold",
    color:"#ffffff"
  },
  button_sign_up_text:{
    fontWeight:"bold",
    color:"#4bae32"
  }
});

const mapStateToProps = (state) => {
  return state
}

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: (action) => { dispatch(action) }
  }
}

// export default connect(mapStateToProps,mapDispatchToProps)(AuthentificationSocialNetwork);
export default connect(mapStateToProps,mapDispatchToProps)(AuthentificationSocialNetwork);
