import React from 'react';
import { View,Text,TouchableOpacity,StyleSheet,TextInput } from 'react-native';
import Modal from "react-native-modal";
import { Rating, AirbnbRating } from 'react-native-ratings';

class ReviewModal extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      comment:""
    }
  }

  _close(){
    this.props.close();
  }

  _saveNote(note){
    this.setState(note);
  }

  _pushReviews(){
    this.props.saveReview({ comment:this.state.comment });
  }

  render(){
    return(
      <Modal isVisible={ this.props.showRating }>
        <View style={{ flex: 1,alignItems:"center",justifyContent:"center" }}>
          <View style={{ backgroundColor:"#ffffff",borderRadius:4,padding:16 }}>

            <View style={{ alignItems:"center",justifyContent:"center" }}>
              <Text style={{ textAlign:"center",fontSize:18,color:"#636e72" }}>Pourquoi tu n'étais pas satisfait ? (Livraison tardé,livreur pas poli, qualité de la nouriture,...)</Text>
            </View>

            <View style={{ backgroundColor:"#00000025", height:1, opacity:0.5,marginTop:8,marginBottom:8 }}></View>

            <View style={{ alignItems:"center",justifyContent:"center",marginBottom:16,marginTop:8 }}>

              <View style={styles.form_input}>
                <View style={styles.text_input}>
                  <TextInput ref="comment" multiline={true} numberOfLines={4} underlineColorAndroid="#ffffff" style={styles.input} placeholder={"Saisir votre commentaire"} onChangeText={(text)=> this.setState({comment:text})}/>
                </View>
              </View>

            </View>

            <View style={{ flexDirection:"row",alignItems:"center",justifyContent:"center" }}>

              <TouchableOpacity style={{flexDirection:"row",alignItems:"center",justifyContent:"center",backgroundColor:"#dfe6e9",borderColor:"#dce1e5",borderWidth:2,borderRadius:10,paddingTop:10,paddingBottom:10,paddingLeft:10,paddingRight:10,marginRight:8}} onPress={()=>{ this._close() }}>
                <Text style={{fontSize:16,fontWeight:"bold",color:"#636e72"}}>ANNULER</Text>
              </TouchableOpacity>

              <TouchableOpacity style={{flexDirection:"row",alignItems:"center",justifyContent:"center",backgroundColor:"#00b894",borderColor:"#dce1e5",borderWidth:2,borderRadius:10,paddingTop:10,paddingBottom:10,paddingLeft:10,paddingRight:10,marginLeft:8}} onPress={()=>{ this._pushReviews() }}>
                <Text style={{fontSize:16,fontWeight:"bold",color:"#ffffff"}}>ENVOYER</Text>
              </TouchableOpacity>

            </View>
          </View>
        </View>
      </Modal>
    )
  }

}

const styles = StyleSheet.create({
  form_input:{
    flexDirection:"row",
    borderColor:"#dce1e5",
    borderWidth:2,
    borderRadius:10,
    overflow:"hidden",
    marginBottom:16
  },
  text_input:{
    flex:1,
    backgroundColor:"#ffffff"
  },
  input:{
    justifyContent: "flex-start",
    paddingBottom:12,
    paddingTop:12,
    paddingLeft:24,
    paddingRight:8,
    fontSize:16
  }
});

export default ReviewModal;
