import React from 'react'
import BaseComponent from './BaseComponent';
import { StyleSheet, View, Image, Text, BackHandler, TextInput, TouchableOpacity, ScrollView, Button, Alert, AsyncStorage } from 'react-native'
import stylesDefault from '../Styles/styleDefault';
import { connect } from 'react-redux';
import { getUserProfil } from '../API/UtilisateurAPI';
import Loader from './Loader';


class Profil extends BaseComponent {
  _didFocusSubscription;
  _willBlurSubscription;

  constructor(props) {
    super(props);
    this._didFocusSubscription = props.navigation.addListener('didFocus', payload =>
      BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
    );

    this.state = {
      userId:"-1",
      userToken:"",
      userName:"...",
      userFirstName:"...",
      userEmail:"...",
      numeroTel:"-1",
      alimentSauves:0.0,
      co2Evite:0.0,
      mobileBanking:"(Non défini)",
      adresseLivraison:"(Non défini)",
      preferenceAlimentaire:"(Non défini)",
      loading:false,
      dataIsLoaded:false
    }

  }

  _signUp(){
    this.setState({
      dataIsLoaded : false
    });
    this._startActivity("Inscription");
  }

  componentDidMount() {
    this._willBlurSubscription = this.props.navigation.addListener('willBlur', payload =>
      BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
    );
  }

  _back(){
    this._startActivity(this.props.navigation.state.params.src);
  }

  onBackButtonPressAndroid = () => {
    this._back();
    return true;
  };

  componentWillUnmount() {
    this._didFocusSubscription && this._didFocusSubscription.remove();
    this._willBlurSubscription && this._willBlurSubscription.remove();
  }

  _toggleChangeState(){
    console.log('Profil','activity : '+this.props.activity);

    if(this._inProfil() && !this.state.dataIsLoaded){
      this._getUserDataInStorage((err, result) => {
        if(result!=null){
          this.setState({
            userId :  result[0][1],
            userName : result[1][1],
            userFirstName : result[2][1],
            userEmail : result[3][1],
            userToken : result[4][1],
            dataIsLoaded : true,
            loading:true
          });
          this._getUserProfil();
        }else{
          this.setState({
            dataIsLoaded : false
          });
          this._startActivity("Login");
        }
      });

    }
  }

  _startActivity(destActivity,params={}){
    const action = { type: "SET_ACTIVITY", value: {activity:destActivity} };
    this.props.dispatch(action);
    this.setState({
      dataIsLoaded : false
    },()=>{
      this.props.navigation.navigate(destActivity,params);
    });
  }

  _editProfil(){
    this.setState({
      dataIsLoaded : false
    });
    this._startActivity("EditProfil",this.state);
  }

  _getUserProfil(){

    let self = this;

    this.setState({loading:true});

    getUserProfil({token:this.state.userToken,userId:this.state.userId}).then(data=>{
      console.log("Profil",data);

      if(data.status==1){
        const userData = data.data;
        this.setState({
          numeroTel:(userData.numero_telephone)?userData.numero_telephone:"-1",
          alimentSauves:(userData.quantite_aliment_sauve)?userData.quantite_aliment_sauve:"0.0",
          co2Evite:(userData.quantite_CO2_sauve)?userData.quantite_CO2_sauve:"0.0",
          mobileBanking:(userData.mobile_banking)?userData.mobile_banking:"(Non défini)",
          adresseLivraison:(userData.adresse_de_livraison)?userData.adresse_de_livraison:"(Non défini)",
          preferenceAlimentaire:(userData.preference_alimentaire)?this._renderPreferenceAlimentaire(userData.preference_alimentaire):"(Aucune)",
          loading:false
        });
      }else if(data.status==0){
        Alert.alert(
          'Session expirée',
          'Votre session a expiré. Veuillez vous reconnecter!',
          [
            {text: 'S\'identifier', onPress: () => {
              this.setState({
                dataIsLoaded : false
              });
              this._startActivity("Login");
            } },
          ],
          { cancelable: false }
        );
      }

    }).catch(error=>{
      console.log(error);
      this.setState({loading:false});
      Alert.alert(
        'Connexion interrompue',
        'Impossible de se connecter avec le serveur.',
        [
          {text: 'Réessayer', onPress: () => {
            self._getUserProfil();
          } , style:"cancelable"},
          {text: 'Annuler', onPress: () => {
            self._startActivity(this.props.navigation.state.params.src)
          }, style:"cancelable"},
        ],
        { cancelable: false }
      );
    });
  }

  _renderPreferenceAlimentaire(data){
    var result = "";

    for(var i=0;i<data.length;i++){
      result+="- "+data[i].label+"\n";
    }

    return result;
  }

  _renderEmailView(){
    if(this.state!=null && this.state.userEmail!=null){
      if(this.state.userEmail.indexOf("facebook")==-1){
        return(
          <View style={{ flexDirection:"row",marginTop:4 }}>
            <Image source={require("../Images/icon/8.png")} style={{ width:10,height:10,marginTop:5,tintColor:"#fff" }} />
            <Text style={[{marginLeft:4,color:"#fff",fontSize:14,fontFamily:"Barlow_Medium"}]}>{ this.state.userEmail }</Text>
          </View>
        );
      }else{
        return(
          <View style={{ flexDirection:"row",marginTop:4 }}>
            <Image source={require("../Images/fb_icon.png")} style={{ width:10,height:10,marginTop:5,tintColor:"#fff" }} />
            <Text style={[{marginLeft:4,color:"#fff",fontSize:14,fontFamily:"Barlow_Medium"}]}>{"Associé à mon compte facebook"}</Text>
          </View>
        );
      }
    }
  }

  _renderTelView(){
    if(this.state.numeroTel!="-1"){
      return (
        <View style={{ flexDirection:"row",marginTop:4 }}>
          <Image source={require("../Images/other/world_location.png")} style={{ width:10,height:10,marginTop:5,tintColor:"#00AFAA" }} />
          <Text style={[stylesDefault.darkTextColor,{marginLeft:4}]}>{ this.state.numeroTel }</Text>
        </View>
      );
    }
  }

  render() {

      this._toggleChangeState();
      return (
        <View style={styles.main_container}>

          <View ref="actionBar" style={[{
            flexDirection:"row",paddingTop:8,paddingBottom:8,alignItems:"center",justifyContent:"center" },stylesDefault.themePrimary]}>

            <TouchableOpacity style={{ padding:10 }} onPress={()=>{this._back()}}>
              <Image source={require("../Images/other/left_arrow.png")} style={{ height:20, width:20, resizeMode:Image.resizeMode.contain,tintColor:"#fff" }} />
            </TouchableOpacity>

            <Text style={[{ flex:1,fontSize:20,color:"#fff",fontFamily:"Barlow_Bold" }]}>
              Mon profil
            </Text>

          </View>

          <ScrollView style={[{ flex:1 },stylesDefault.themePrimary]}>

            <View ref="baseInfoContainer" style={[{ flex:1, flexDirection:"row",padding:10,alignItems:"center",marginBottom:8 },stylesDefault.themePrimary]}>
              <View style={{width:100,height:100,borderRadius:100,alignItems:"center",justifyContent:"center",marginLeft:4,marginRight:4}}>
                <Image source={require("../Images/other/avatar_placeholder.png")} ref="avatar" style={{ width:90,height:90, resizeMode:Image.resizeMode.contain,borderRadius:100 }}  />
              </View>

              <View ref="infoContainer" style={{alignSelf:"baseline",flex:1,marginLeft:8}}>

                <Text ref="userName" style={[ { fontSize :24,color:"#fff",fontFamily:"Barlow_Bold" }]}>{this.state.userName}</Text>

                {this._renderEmailView()}

                <Text style={[{ marginTop:8,fontSize:11,textAlign:"justify",color:"#fff",fontFamily:"Barlow_Regular" }]}>
                {"Merci "+this.state.userName+" d’avoir choisi un mode de consommation durable. En utilisant Manzer Partazer tu as déjà sauvées "+this.state.alimentSauves+" kg des aliments,c’est-à-dire tu as éviter "+this.state.co2Evite+" kg des emissions CO2 et en plus tu as soutenu la création des emplois verts locaux ! C’est un vrai impact positive ! Tu es un changemaker "+this.state.userName+" !"}
                </Text>

              </View>

            </View>

            <View style={{ backgroundColor:"#fff",paddingTop:8 }}>
              <View style={{ flexDirection:"row",alignItems:"center",paddingLeft:4,paddingRight:4 }}>
                <View style={[{ height:1, width:32 },stylesDefault.themePrimary]}></View>
                <Text style={[{ fontSize:18,paddingLeft:4,paddingRight:4,fontFamily:"Barlow_Bold" },stylesDefault.textThemePrimary]}>Rapport socio-environnemental</Text>
                <View style={[{ height:1,flex:1 },stylesDefault.themePrimary]}></View>
              </View>

              <View ref="rapportEnviroContainer" style={{ marginTop:4,marginBottom:8,marginLeft:10,marginRight:10 }}>

                <View style={styles.itemStyles}>

                  <Image source={require("../Images/icon/9.png")} ref="avatar" style={{ width:25,height:25, resizeMode:Image.resizeMode.contain }}  />

                  <View style={{ marginLeft:10 }}>

                    <Text style={[stylesDefault.darkTextColor,{fontFamily:"Barlow_Regular"}]}>Aliments sauvés</Text>
                    <Text style={[{ fontSize:16,fontFamily:"Barlow_Bold" },stylesDefault.textThemePrimary]}>{ this.state.alimentSauves } Kg</Text>

                  </View>

                </View>

                <View style={[{ height:1,flex:1,opacity:0.5 },stylesDefault.themeBlue]}></View>

                <View style={styles.itemStyles}>

                  <Image source={require("../Images/icon/10.png")} ref="avatar" style={{ width:25,height:25, resizeMode:Image.resizeMode.contain }}  />

                  <View style={{ marginLeft:10 }}>

                    <Text style={[stylesDefault.darkTextColor,{fontFamily:"Barlow_Regular"}]}>Emissions de CO2 éviter</Text>
                    <Text style={[{fontSize:16,fontFamily:"Barlow_Bold" },stylesDefault.textThemePrimary]}>{ this.state.co2Evite } Kg</Text>

                  </View>

                </View>

              </View>

              <View style={{ flexDirection:"row",alignItems:"center",paddingLeft:4,paddingRight:4 }}>
                <View style={[{ height:1, width:32 },stylesDefault.themePrimary]}></View>
                <Text style={[{ fontSize:18,paddingLeft:4,paddingRight:4,fontFamily:"Barlow_Bold" },stylesDefault.textThemePrimary]}>A propos</Text>
                <View style={[{ height:1,flex:1 },stylesDefault.themePrimary]}></View>
              </View>

              <View ref="aProposContainer" style={{ marginTop:4,marginBottom:8,marginLeft:10,marginRight:10 }}>

                <View style={styles.itemStyles}>

                  <Image source={require("../Images/icon/11.png")} ref="avatar" style={{ width:25,height:25, resizeMode:Image.resizeMode.contain }}  />

                  <View style={{ marginLeft:10 }}>

                    <Text style={[stylesDefault.darkTextColor,{fontFamily:"Barlow_Regular"}]}>Compte mobile banking</Text>
                    <Text style={[{fontSize:16,fontFamily:"Barlow_Bold" },stylesDefault.textThemePrimary]}>{ this.state.mobileBanking }</Text>

                  </View>

                </View>

                <View style={[{ height:1,flex:1,opacity:0.5 },stylesDefault.themeBlue]}></View>

                <View style={styles.itemStyles}>

                  <Image source={require("../Images/icon/12.png")} ref="avatar" style={{ width:25,height:25, resizeMode:Image.resizeMode.contain }}  />

                  <View style={{ marginLeft:10 }}>

                    <Text style={[stylesDefault.darkTextColor,{fontFamily:"Barlow_Regular"}]}>Adresse de livraison</Text>
                    <Text style={[{fontSize:16,fontFamily:"Barlow_Bold" },stylesDefault.textThemePrimary]}>{ this.state.adresseLivraison }</Text>

                  </View>

                </View>

                <View style={[{ height:1,flex:1,opacity:0.5 },stylesDefault.themeBlue]}></View>

                <View style={styles.itemStyles}>

                  <Image source={require("../Images/icon/13.png")} ref="avatar" style={{ width:25,height:25, resizeMode:Image.resizeMode.contain }}  />

                  <View style={{ marginLeft:10 }}>

                    <Text style={[stylesDefault.darkTextColor,{fontFamily:"Barlow_Regular"}]}>Préférence alimentaire</Text>
                    <Text style={[{fontSize:16,fontFamily:"Barlow_Bold" },stylesDefault.textThemePrimary]}>{ this.state.preferenceAlimentaire }</Text>

                  </View>

                </View>


              </View>
            </View>

          </ScrollView>

          <View style={[{ flexDirection:"row",paddingTop:8,paddingBottom:8,backgroundColor:"#fff" }]}>

            <TouchableOpacity style={{ flex:1,justifyContent:"center",alignItems:"center",paddingTop:4,paddingBottom:4 }} onPress={()=>{ this._startActivity("ListeCommande",{src:"Profil"}) }}>
                <Image source={require("../Images/other/order.png")} style={{ height:24, width:24, resizeMode:Image.resizeMode.contain,opacity:0.8,tintColor:"#00b2a9" }} />
                <Text style={[{ marginTop:4,fontSize:16,fontFamily:"Barlow_Bold" },stylesDefault.textThemePrimary]}>Commandes</Text>
            </TouchableOpacity>

            <TouchableOpacity style={{ flex:1,justifyContent:"center",alignItems:"center",paddingTop:4,paddingBottom:4 }} onPress={()=> this._startActivity("ListeFavoris",{src:"Profil"})}>
                <Image source={require("../Images/other/wishlist.png")} style={{ height:24, width:24, resizeMode:Image.resizeMode.contain,opacity:0.8,tintColor:"#00b2a9" }} />
                <Text style={[{ marginTop:4,fontSize:16,fontFamily:"Barlow_Bold" },stylesDefault.textThemePrimary]}>Liste de rêve</Text>
            </TouchableOpacity>

          </View>

          <TouchableOpacity style={[{ padding:10,width:60,height:60,borderRadius:100,alignItems:"center",justifyContent:"center",position:"absolute",bottom:75,right:10,shadowColor: '#000',
          shadowOffset: { width: 0, height: 2 },
          shadowOpacity: 1,
          shadowRadius: 1,
          elevation: 4,
          marginBottom:6 },stylesDefault.themePrimary]} onPress={()=>{ this._editProfil() }}>
            <Image source={require("../Images/other/edit.png")} style={{height:20,width:20,tintColor:"#ffffff"}} />
          </TouchableOpacity>

        </View>
      )
  }
}

/*<TouchableOpacity style={{ flex:1,justifyContent:"center",alignItems:"center",paddingTop:4,paddingBottom:4 }} onPress={()=> this._startActivity("SuiviLivraison",{src:"Profil"})}>
    <Image source={require("../Images/other/delivery.png")} style={{ height:20, width:20, resizeMode:Image.resizeMode.contain,opacity:0.8,tintColor:"#4f4f4f" }} />
    <Text style={[{ marginTop:4,fontSize:12,fontWeight:"bold",opacity:0.8 },stylesDefault.darkTextColor]}>Livraison</Text>
</TouchableOpacity>*/

const styles = StyleSheet.create({
  main_container:{
    flex:1
  },
  logo_container:{
    flex:1,
    alignItems:"center",
    justifyContent:"flex-end",
    paddingBottom:64,
  },
  logo:{
    width:100,
    height:100
  },
  form_container:{
    paddingLeft:32,
    paddingRight:32
  },
  form_input:{
    flexDirection:"row",
    borderColor:"#dce1e5",
    borderWidth:2,
    borderRadius:10,
    overflow:"hidden",
    marginBottom:16
  },
  icon:{
    width:20,
    height:20,
    resizeMode:Image.resizeMode.contain
  },
  text_input:{
    flex:1,
    backgroundColor:"#ffffff"
  },
  input:{
    paddingBottom:12,
    paddingTop:12,
    paddingLeft:24,
    paddingRight:8,
    fontSize:16
  },
  icon_input:{
    paddingLeft:20,
    paddingRight:20,
    justifyContent:"center",
    backgroundColor:"#00AFAA"
  },
  button_container:{
    flexDirection:"row",
    marginTop:16,
    marginBottom:64
  },
  button_sign_up:{
    flex:1,
    alignItems:"center",
    justifyContent:"center",
    borderColor:"#dce1e5",
    borderWidth:2,
    borderRadius:10,
    overflow:"hidden",
    paddingTop:16,
    paddingBottom:16,

    marginRight:4,
    backgroundColor:"#ffffff"
  },
  button_log_in:{
    flex:1,
    backgroundColor:"#00AFAA",
    borderColor:"#dce1e5",
    borderWidth:2,
    borderRadius:10,
    overflow:"hidden",
    paddingTop:16,
    paddingBottom:16,
    alignItems:"center",
    marginLeft:4
  },
  button_log_in_text:{
    fontWeight:"bold",
    color:"#ffffff"
  },
  button_sign_up_text:{
    fontWeight:"bold",
    color:"#00AFAA"
  },
  itemStyles:{
    flexDirection:"row",
    marginTop:4,
    marginBottom:4,
    alignItems:"center",
    padding:10
  }
});

const mapStateToProps = (state) => {
  return state
}

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: (action) => { dispatch(action) }
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(Profil);
