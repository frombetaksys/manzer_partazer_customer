import React, { Component } from 'react';
import { TouchableOpacity,Text } from 'react-native';
import stylesDefault from '../../Styles/styleDefault'


export default class PrimaryButton extends Component {
  render() {
    return (
      <TouchableOpacity style={[{paddingLeft:25,paddingRight:25,paddingTop:10,paddingBottom:10,borderRadius:100,marginLeft:8,
      marginRight:8,
      shadowColor: '#000',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 1,
      shadowRadius: 1,
      elevation: 4,
      marginBottom:6},stylesDefault.themePrimary]} onPress={this.props.onClick()}>
        <Text style={[{ color:"#fff" },stylesDefault.barlowBold]}>{this.props.text.toUpperCase()}</Text>
      </TouchableOpacity>
    );
  }
};
