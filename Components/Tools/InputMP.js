import React from 'react'
import { StyleSheet, View, Image, Text, BackHandler, TextInput, TouchableOpacity,ScrollView,KeyboardAvoidingView,Alert  } from 'react-native'
import stylesDefault from '../../Styles/styleDefault'


class InputMP extends React.Component {

  render() {
    return (
      <View>
        <Text style={[styles.label_input,stylesDefault.darkTextColor]}>{this.props.label}</Text>
        <View style={styles.form_input}>
          <View style={styles.text_input}>
            <TextInput ref="{this.props.ref}" underlineColorAndroid="#ffffff" style={styles.input} placeholder={this.props.placeholder} onChangeText={(text)=> this.props.changeState(text)}/>
          </View>
        </View>
      </View>
    )
  }

}

const styles = StyleSheet.create({
  main_container:{
    flex:1,
    marginTop:24
  },
  logo_container:{
    flex:1,
    alignItems:"center",
    justifyContent:"flex-end",
    paddingTop:16,
    paddingBottom:16
  },
  logo:{
    width:50,
    height:50
  },
  form_container:{
    paddingLeft:32,
    paddingRight:32
  },
  form_input:{
    flexDirection:"row",
    borderColor:"#dce1e5",
    borderWidth:2,
    borderRadius:10,
    overflow:"hidden",
    marginBottom:16
  },
  icon:{
    width:20,
    height:20,
    resizeMode:Image.resizeMode.contain
  },
  text_input:{
    flex:1,
    backgroundColor:"#ffffff"
  },
  input:{
    paddingBottom:12,
    paddingTop:12,
    paddingLeft:24,
    paddingRight:8,
    fontSize:16
  },
  icon_input:{
    paddingLeft:20,
    paddingRight:20,
    justifyContent:"center",
    backgroundColor:"#4bae32"
  },
  button_container:{
    flexDirection:"row",
    marginTop:16,
    marginBottom:64
  },
  button_sign_up:{
    flex:1,
    alignItems:"center",
    justifyContent:"center",
    borderColor:"#dce1e5",
    borderWidth:2,
    borderRadius:10,
    overflow:"hidden",
    paddingTop:16,
    paddingBottom:16,

    marginRight:4,
    backgroundColor:"#ffffff"
  },
  button_log_in:{
    flex:1,
    backgroundColor:"#4bae32",
    borderColor:"#dce1e5",
    borderWidth:2,
    borderRadius:10,
    overflow:"hidden",
    paddingTop:16,
    paddingBottom:16,
    alignItems:"center",
    marginLeft:4
  },
  button_log_in_text:{
    fontWeight:"bold",
    color:"#ffffff"
  },
  button_sign_up_text:{
    fontWeight:"bold",
    color:"#4f4f4f"
  },
  label_input:{
    fontWeight:"bold",
    fontSize:16,
    marginBottom:2
  }
});


export default InputMP
