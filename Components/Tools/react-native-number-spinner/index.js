import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Alert,
  Image
} from 'react-native';
var styles = require('./style')


class Spinner extends Component {

  state = {
    min: this.props.min,
    max: 99,
    default: 1,
    num: this.props.value,
    color: 'transparent',//old 636e72
    numColor: '#333',
    numBgColor: 'white',
    showBorder: true,
    fontSize: 14,
    btnFontSize: 16,
    buttonTextColor: '#000000',
    disabled: false,
    width: 50,
    height: 30,
    btn_height: 20
};
  constructor(props){
    super(props);
    this.setState({
      num:this.props.value
    });
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.disabled) {
      this.setState({
        disabled: nextProps.disabled
    });
    }
    if (nextProps.min) {
      this.setState({
        min: nextProps.min
    });
    }
    if (nextProps.max) {
      this.setState({
        max: nextProps.max
    });
    }
    if (nextProps.value !== false) {
      this.setState({
        num: nextProps.value
    });
    }
    if (nextProps.color !== false) {
      this.setState({
        color: nextProps.color
    });
    }
  }

  _onNumChange = (num) => {
    if (this.props.onNumChange) {
      this.props.onNumChange(num);
    }
};

  _increase = ()=>{
    if (this.state.disabled) return;

    if (this.state.max > this.state.num) {
      var num = this.state.num + 1;
      if (typeof this.state.value === 'undefined') {
        this.setState({
          num: num
        });


      }
      this._onNumChange(num);
    }else{
      Alert.alert(
        'Quantité d\'achat maximum atteinte',
        'La quantité que vous pouvez commander pour l\'achat de ce produit doit être au dessous de la quantité en stock ('+this.state.max+').',
        [
          {text: 'Ok', onPress: () => {}, style:"cancelable"}
        ],
        { cancelable: false }
      );
    }

};

  _decrease () {
    if (this.state.disabled) return;

    if (this.state.min < this.state.num) {
      var num = this.state.num - 1;
      if (typeof this.state.value === 'undefined') {
            this.setState({
              num: num
          });
      };

      this._onNumChange(num);
    }else{
      Alert.alert(
        'Quantité d\'achat minimum atteinte',
        'La quantité que vous pouvez commander pour l\'achat de ce produit doit être au delà de '+this.state.min+'.',
        [
          {text: 'Ok', onPress: () => {}, style:"cancelable"}
        ],
        { cancelable: false }
      );
    }
  };
/*<Text style={[styles.btnText,
    { color: this.state.buttonTextColor, fontSize: this.state.btnFontSize
    }]}>+</Text>*/
  render () {
    return (
      <View style={[styles.container,
        { borderColor: 'transparent' },
        { width: this.state.width },{ marginTop:8,marginBottom:8 } ]}>
        <TouchableOpacity
          style={[styles.btn,
            { backgroundColor: 'transparent' },
            { borderColor: this.state.showBorder ? this.state.color : 'transparent' },
            { height: this.state.btn_height }]}
          onPress={() => this._increase()}>
          <Image
              style={{ width:20,height:20,tintColor:"#545c69" }}
              source={require('./plus.png')}
            />
        </TouchableOpacity>
        <View style={[styles.num,
            { borderRadius:100, backgroundColor: "#00b2a9", height: this.state.height,width:this.state.height, marginTop:10, marginBottom:6
            }]}>
          <Text style={[styles.numText, {color: "#fff", fontSize: 18,fontWeight:"bold"}]}>{this.state.num}</Text>
        </View>
        <TouchableOpacity
          style={[styles.btn,
            { backgroundColor: 'transparent' },
            { borderColor: this.state.showBorder ? this.state.color : 'transparent' },
            { height: this.state.btn_height } ]}
          onPress={() => this._decrease()}>
          <Image
              style={{ width:20,height:20,tintColor:"#545c69" }}
              source={require('./minus.png')}
            />
        </TouchableOpacity>
      </View>
    )
  }
}

export default Spinner;
