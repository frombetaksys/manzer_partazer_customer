import React from 'react';
import BaseComponent from './BaseComponent';
import { StyleSheet, View, Image, Text, BackHandler, TextInput, TouchableOpacity,ScrollView,Alert,FlatList,Modal  } from 'react-native'
import stylesDefault from '../Styles/styleDefault'
import EtatItem from './EtatItem'
import { connect } from 'react-redux';
import RatingModal from './RatingModal';
import ReviewModal from './ReviewModal';
import SatisfactionModal from './SatisfactionModal';
import { sendFeedBack } from '../API/UtilisateurAPI';
import Loader from './Loader';



class SuiviLivraison extends BaseComponent {
  _didFocusSubscription;
  _willBlurSubscription;

  constructor(props) {
    super(props);

    this._didFocusSubscription = props.navigation.addListener('didFocus', payload =>
      BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
    );

    this.state = {
      showRating:false,
      showReviewing:false,
      showSatisfactionRating:true,
      userToken:"",
      userId:"",
      satisfaction:0,
      etats:[
        {
          ordre:3,
          label:"Commande livrée - Reçu signé par : Njary",
          heure:"17h00"
        },
        {
          ordre:2,
          label:"En route",
          heure:"16h00"
        },
        {
          ordre:1,
          label:"Ramassage des produits",
          heure:"15h20"
        }
      ],
      loading:false
    }

  }

  _back(){
    this._startActivity(this.props.navigation.state.params.src);
  }

  componentDidMount() {
    this._willBlurSubscription = this.props.navigation.addListener('willBlur', payload =>
      BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
    );
  }

  onBackButtonPressAndroid = () => {
    this._back();
    return true;
  };

  componentWillUnmount() {
    this._didFocusSubscription && this._didFocusSubscription.remove();
    this._willBlurSubscription && this._willBlurSubscription.remove();
  }

  _checkData(){
    console.log("SuiviLivraison","dataIsReady : "+this.dataIsReady);
    if(!this.dataIsReady && this._inSuiviLivraison()){
      this.dataIsReady = true;
      this._getUserInfo();
    }
  }

  _getUserInfo(){
    this.setState({loading:true});

    this._getUserDataInStorage((err, result) => {
      if(result!=null){
        this.setState({
          userId :  result[0][1],
          userToken : result[4][1]
        });
        this._getStatutLivraison();
      }else{
        this.dataIsReady = false;
        this.setState({produits:[],loading:false});
        this._startActivity("Login");
      }
    });
  }

  _getStatutLivraison(){
    this.setState({loading:false});
  }

  _saveRating = (data)=>{

    let self = this;

    console.log(data);

    let finalData = [
      {
        id_label_feedback:1,
        note:data.service,
        commentaire:""
      },
      {
        id_label_feedback:2,
        note:data.livraison,
        commentaire:""
      },
      {
        id_label_feedback:3,
        note:data.rapidite,
        commentaire:""
      },
      {
        id_label_feedback:4,
        note:data.qualiteNourriture,
        commentaire:""
      }
    ];

    this.setState({showRating:false,loading:true});
    sendFeedBack({token:this.state.userToken,id_utilisateur:this.state.userId,data:finalData}).then((data)=>{

      console.log(data);

      this.setState({loading:false});

    }).catch((error)=>{
      console.log(error);

      this.setState({loading:false});

      Alert.alert(
        'Connexion interrompue',
        'Une erreur s\'est produite lors de l\'enregistrement de  votre feedback',
        [
          {text: 'Réessayer', onPress: () => {
            self._saveRating(data);
          } , style:"cancelable"},
          {text: 'Annuler', onPress: () => console.log("ok") , style:"cancelable"},
        ],
        { cancelable: false }
      );
    })
  }

  _saveSatisfaction = (data)=>{
    console.log(data)
    if(data.satisfaction<=2){
      this.setState({showReviewing:true,satisfaction:data.satisfaction});
    }else{
      this.setState({satisfaction:data.satisfaction},()=>{
        this._saveReview({comment:""});
      });
    }

  }

  _saveReview = (data)=>{

    console.log(data);
    let self = this;

    console.log(data);

    let finalData = [
      {
        id_label_feedback:5,
        note:this.state.satisfaction,
        commentaire:data.comment
      }
    ];

    console.log(this.state);

    console.log("finalData",finalData);

    this.setState({showReviewing:false,loading:true});
    sendFeedBack({token:this.state.userToken,id_utilisateur:this.state.userId,data:finalData}).then((data)=>{

      console.log(data);

      this.setState({loading:false});

    }).catch((error)=>{
      console.log(error);

      this.setState({loading:false});

      Alert.alert(
        'Connexion interrompue',
        'Une erreur s\'est produite lors de l\'enregistrement de  votre feedback',
        [
          {text: 'Réessayer', onPress: () => {
            self._saveReview(data);
          } , style:"cancelable"},
          {text: 'Annuler', onPress: () => console.log("ok") , style:"cancelable"},
        ],
        { cancelable: false }
      );
    })
  }

  _closeReviewModal = ()=>{
    this.setState({showReviewing:false});
  }

  _renderCurrentStatus(){
    return(
      <View style={{ backgroundColor:"#ffffff",margin:8,borderRadius:4,borderWidth:2,borderColor:"#00000012",padding:8 }}>
        <Text style={{ color:"#636e72" }}>Etat en cours...</Text>
        <Text style={{ fontSize:24,marginTop:4,color:"#636e72" }}>{ this.state.etats[2].label }</Text>
        <View style={{ flexDirection:"row",marginTop:8 }}>
          <View style={{ flex:1,flexDirection:"row" }}>

          </View>
          <View style={{ alignItems:"center",justifyContent:"center" }}>
            <Text style={{ backgroundColor:"#00b894",paddingTop:4,paddingBottom:4,paddingLeft:8,paddingRight:8,color:"#ffffff",fontWeight:"bold",fontSize:15,borderRadius:2 }}>Jeudi 15 nov. 2018 à 16h29</Text>
          </View>
        </View>
      </View>
    )
  }

  _renderResumeStatusDaily(day){
    return(
      <View style={{ marginBottom:8 }}>

        <View ref="zoneDate" style={{ backgroundColor:"#00000012",padding:8 }}>
          <Text style={{ fontWeight:"bold",fontSize:16,color:"#636e72" }}>Jeudi 15 novembre 2018</Text>
        </View>

        <View style={{ flexDirection:"row" }}>

          <Text style={{ flex:1,textAlign:"center",fontWeight:"bold",paddingTop:4,paddingBottom:4,color:"#636e72",fontSize:16 }}>
            Etat
          </Text>

          <Text style={{ flex:1,textAlign:"center",fontWeight:"bold",paddingTop:4,paddingBottom:4,color:"#636e72",fontSize:16 }}>
            Heure
          </Text>

        </View>

        <View style={{ backgroundColor:"#00000025", height:1, opacity:0.5,marginTop:4 }}></View>

        <FlatList
          style={{ flex:1 }}
          data={this.state.etats}
          keyExtractor={(item) => item.ordre.toString()}
          renderItem={({item}) => (
            <EtatItem
              etatItem={item}
            />
          )}
        />

      </View>
    )
  }

  _renderResumeList(){
    let v = ["test1"];
    let self = this;

    const vData = v.map((day,index)=>{
      return(
        <View key={ day }>
          { self._renderResumeStatusDaily(day) }
        </View>
      )
    })

    return (
      <ScrollView style={{ flex:1 }}>
        { vData }
      </ScrollView>
    )
  }

  render(){
    this._checkData();

    return(
      <View style={{ flex:1,backgroundColor:"#ffffff" }}>

        <View ref="actionBar" style={[{
          borderBottomWidth:1,borderBottomColor:"#00000012",flexDirection:"row",paddingTop:8,paddingBottom:8,alignItems:"center",justifyContent:"center",backgroundColor:"#f0f0f0" }]}>

          <TouchableOpacity style={{ padding:10 }} onPress={()=>{ this._back() }}>
            <Image source={require("../Images/other/left_arrow.png")} style={{ height:20, width:20, resizeMode:Image.resizeMode.contain }} />
          </TouchableOpacity>

          <Text style={[{ flex:1,fontSize:20 },stylesDefault.darkTextColor]}>
            Suivi de ma commande
          </Text>

        </View>

        <Loader loading={this.state.loading} />

        { this._renderCurrentStatus() }

        { this._renderResumeList() }

        <RatingModal
          showRating={this.state.showRating}
          saveRating={this._saveRating}
        />

        <ReviewModal
          showRating={this.state.showReviewing}
          close={this._closeReviewModal}
          saveReview={this._saveReview}
        />

        <SatisfactionModal
          showRating={this.state.showSatisfactionRating}
          saveSatisfaction = { this._saveSatisfaction }
        />


      </View>
    )
  }

}

const mapStateToProps = (state) => {
  return state
}

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: (action) => { dispatch(action) }
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(SuiviLivraison);
