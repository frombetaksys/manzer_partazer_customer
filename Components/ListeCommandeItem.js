import React from 'react'
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native'
import { mileSeparator } from '../Helper/StandardHelper';


class ListeCommandeItem extends React.Component {

  render(){
    const { commande } = this.props;
    return(
      <TouchableOpacity style={styles.main_container} onPress={ ()=> this.props.showCommande(commande) }>

        <View style={{ justifyContent:"center",paddingLeft:8 }}>
          <View style={{ width:110,height:110,borderRadius:100,backgroundColor:"#dee6e9",alignItems:"center",justifyContent:"center" }}>
            <Text style={{ fontSize:40,fontFamily:"Barlow_Bold",color:'#b3bec4' }}>{ commande.date.split("/")[0] }</Text>
            <Text style={{ fontSize:20,fontFamily:"Barlow_Bold",color:'#b3bec4',marginTop:-4 }}>{ commande.labelMois }</Text>
          </View>
        </View>

        <View style={{ flex:1, paddingLeft:8, margin:5 }}>

          <View style={{ flexDirection:'row' }}>
            <Text style={ [styles.title_text,stylesDefault.textThemePrimary] }>Montant: { mileSeparator(commande.montant_total) } Ar</Text>
          </View>

          <View style={{ flex:1,flexDirection:"row",marginTop:4 }}>

            <View style={{ flex:1 }}>

              <View style={{ flexDirection:"row" }}>
                <Text style={{ color:'#969696',fontFamily:"Barlow_Regular" }}>Nombre de produits:</Text>
                <Text style={{ flex:1,color:'#969696',fontFamily:"Barlow_Regular" }}> {  commande.nombre_de_produits }</Text>
              </View>

              <View style={{ flexDirection:"row" }}>
                <Text style={{ color:'#969696',fontFamily:"Barlow_Regular" }}>Date:</Text>
                <Text style={{ flex:1,color:'#969696',fontFamily:"Barlow_Regular" }}>{ commande.date } à { commande.heure }</Text>
              </View>

            </View>

          </View>

          <View style={{ flexDirection:"row",marginTop:4 }}>
            <View style={{ flex:1,flexDirection:"row" }}>

            </View>
            <View style={{ flexDirection:"row",alignItems:"center",justifyContent:"center",backgroundColor:"#00AFAA",paddingTop:4,paddingBottom:4,paddingLeft:10,paddingRight:10,borderRadius:100 }}>
              <Text style={{ color:"#ffffff",fontSize:15,fontFamily:"Barlow_Bold" }}>{ commande.etat }</Text>
              <Image style={{ width:16,height:16,marginLeft:4,resizeMode:Image.resizeMode.contain }}
                source={require('../Images/icon/0.png')}
              />
            </View>
          </View>

        </View>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  main_container: {
    flexDirection:"row",
    backgroundColor: '#FFFFFF',
    borderRadius:8,
    marginLeft:8,
    marginRight:8,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 1,
    shadowRadius: 1,
    elevation: 4,
    marginTop:4,
    marginBottom:6,
    padding:8
  },
  title_text: {
    flex:1,
    fontSize:18,
    fontFamily:"Barlow_Bold",
    flexWrap : 'wrap',
  },
  favorite_image: {
      width: 40,
      height: 40

  }
})

export default ListeCommandeItem;
