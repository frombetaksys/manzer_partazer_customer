import React from 'react'
import BaseComponent from './BaseComponent';
import { StyleSheet, View, Image, Text, BackHandler, TextInput, TouchableOpacity,ScrollView,Alert,FlatList,Modal  } from 'react-native'
import stylesDefault from '../Styles/styleDefault'
import Loader from './Loader';
import { connect } from 'react-redux';
import RamassageItem from './RamassageItem';
import { mileSeparator } from '../Helper/StandardHelper';

class OptionRamassage extends BaseComponent {
  _didFocusSubscription;
  _willBlurSubscription;

  constructor(props) {
    super(props);

    this._didFocusSubscription = props.navigation.addListener('didFocus', payload =>
      BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
    );

    this.state ={
      coutTotal:0,
      loading:false
    }
  }

  _back(){
    this._startActivity("Panier");
  }

  _getCoutTotal = ()=>{
    const panier = this.props.panier;
    let result = 0;

    for(let i=0;i<panier.length;i++){
      if(panier[i].produit.optionLivraison.etat==1){
        result+=panier[i].produit.optionLivraison.prix;
      }
    }

    this.setState({coutTotal:result});

  }

  _renderListeZone(){

    return (
      <View style={{ flex:1 }}>
        <FlatList
          style={{ flex:1 }}
          data={this.props.panier}
          keyExtractor={(item) => item.produit.id.toString()}
          renderItem={({item}) => (
            <RamassageItem
              ramassageItem={item}
              getCoutTotal={ this._getCoutTotal }
            />
          )}
          onEndReachedThreshold={0.1}
        />
      </View>
    )
  }

  componentDidMount() {
    this._willBlurSubscription = this.props.navigation.addListener('willBlur', payload =>
      BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
    );
  }

  onBackButtonPressAndroid = () => {
    this._back();
    return true;
  };

  componentWillUnmount() {
    this._didFocusSubscription && this._didFocusSubscription.remove();
    this._willBlurSubscription && this._willBlurSubscription.remove();
  }

  render(){
    return(
      <View style={{ flex:1,backgroundColor:"#ffffff" }} >

        <View ref="actionBar" style={[{
          borderBottomWidth:1,borderBottomColor:"#00000012",flexDirection:"row",paddingTop:8,paddingBottom:8,alignItems:"center",justifyContent:"center",backgroundColor:"#f0f0f0" }]}>

          <TouchableOpacity style={{ padding:10 }} onPress={()=>{this._back()}}>
            <Image source={require("../Images/other/left_arrow.png")} style={{ height:20, width:20, resizeMode:Image.resizeMode.contain }} />
          </TouchableOpacity>

          <Text style={[{ flex:1,fontSize:20 },stylesDefault.darkTextColor]}>
            Option de ramassage
          </Text>

          <View style={{ marginRight:10 }}>
            <Text style={[{ textAlign:"right",fontSize:16,fontWeight:"bold" },stylesDefault.darkTextColor]}>
              Coût : { mileSeparator(this.state.coutTotal) } MGA
            </Text>
          </View>

        </View>

        <Loader loading={this.state.loading} />

        { this._renderListeZone() }

        <View>
          <TouchableOpacity style={styles.button_fb_log_in} onPress={()=>{ this._startActivity("ResumeCommande") }}>
            <Text style={styles.button_log_in_text}>{"SUIVANT"}</Text>
            <Image style={{ marginLeft:16,width:10,height:10,resizeMode:Image.resizeMode.contain,marginRight:8,tintColor:"#ffffff" }} source={require('../Images/other/right_arrow.png')} />
          </TouchableOpacity>
        </View>

      </View>
    )
  }

}

const styles = StyleSheet.create({
  main_container:{
    flex:1
  },
  logo_container:{
    flex:1,
    alignItems:"center",
    justifyContent:"flex-end",
    paddingBottom:50,
    paddingTop:50
  },
  logo:{
    width:100,
    height:100
  },
  form_container:{
    paddingLeft:32,
    paddingRight:32
  },
  form_input:{
    flexDirection:"row",
    borderColor:"#dce1e5",
    borderWidth:2,
    borderRadius:10,
    overflow:"hidden",
    marginBottom:16
  },
  icon:{
    width:20,
    height:20,
    resizeMode:Image.resizeMode.contain
  },
  text_input:{
    flex:1,
    backgroundColor:"#ffffff"
  },
  input:{
    paddingBottom:12,
    paddingTop:12,
    paddingLeft:24,
    paddingRight:8,
    fontSize:16
  },
  icon_input:{
    paddingLeft:20,
    paddingRight:20,
    justifyContent:"center",
    backgroundColor:"#4bae32"
  },
  button_container:{
    flexDirection:"row",
    marginTop:16,
    marginBottom:20
  },
  button_sign_up:{
    flex:1,
    alignItems:"center",
    justifyContent:"center",
    borderColor:"#dce1e5",
    borderWidth:2,
    borderRadius:10,
    overflow:"hidden",
    paddingTop:16,
    paddingBottom:16,

    marginRight:4,
    backgroundColor:"#ffffff"
  },
  button_log_in:{
    flex:1,
    backgroundColor:"#4bae32",
    borderColor:"#dce1e5",
    borderWidth:2,
    borderRadius:10,
    overflow:"hidden",
    paddingTop:16,
    paddingBottom:16,
    alignItems:"center",
    marginLeft:4
  },
  button_fb_log_in:{
    flex:1,
    flexDirection:"row",
    alignItems:"center",
    justifyContent:"center",
    backgroundColor:"#00b894",
    borderColor:"#dce1e5",
    borderWidth:2,
    borderRadius:10,
    overflow:"hidden",
    paddingTop:24,
    paddingBottom:24,
    alignItems:"center",
    marginLeft:4
  },
  button_google_log_in:{
    flex:1,
    flexDirection:"row",
    alignItems:"center",
    justifyContent:"center",
    backgroundColor:"#ffffff",
    borderColor:"#dce1e5",
    borderWidth:2,
    borderRadius:10,
    overflow:"hidden",
    paddingTop:16,
    paddingBottom:16,
    alignItems:"center",
    marginLeft:4
  },
  button_log_in_text:{
    fontWeight:"bold",
    color:"#ffffff"
  },
  button_sign_up_text:{
    fontWeight:"bold",
    color:"#4bae32"
  }
});

const mapStateToProps = (state) => {
  return state
}

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: (action) => { dispatch(action) }
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(OptionRamassage);
