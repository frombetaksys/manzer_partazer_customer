import React from 'react'
import { AsyncStorage } from 'react-native'
import { LoginManager } from 'react-native-fbsdk';
import { GoogleSignin } from 'react-native-google-signin';
import {  Alert } from 'react-native'
import Permissions from 'react-native-permissions'
import Geolocation from 'react-native-geolocation-service'
import firebase from 'react-native-firebase';
import type { RemoteMessage } from 'react-native-firebase';

class BaseComponent extends React.Component {

  constructor(props){
    super(props);
    this.isLogged = true;
    this.dataIsReady=false;
  }

  _askLogout = ()=>{
    Alert.alert(
      'Déconnexion',
      'Voulez-vous déconnecter votre compte ?',
      [
        {text: 'OUI', onPress: () => this._logout()},
        {text: 'NON', onPress: () => console.log('NON Pressed'),style: 'cancel'},
      ],
      { cancelable: true }
    );
  }

  _getUserDataInStorage(callback){
    AsyncStorage.multiGet(['userId','userName',"userFirstName","userEmail","userToken"], callback);
  }

  _logout = ()=>{
    // console.log("logout...");
    let self = this;
    AsyncStorage.multiRemove(['userId','userName',"userFirstName","userEmail","userToken"], (err) => {
      console.log(err)
    }).then(async (response)=>{

      self.dataIsReady = false;

      const action = { type: "SET_PREFERENCES", value: {locate:-1,notify:-1} };
      self.props.dispatch(action);

      self.props.dispatch({ type: "CLEAR_PANIER", value: {} });

      try{
        LoginManager.logout();
      }catch(error){
        console.log(error);
      }

      try {
        await GoogleSignin.revokeAccess();
        await GoogleSignin.signOut();
      } catch (error) {
        console.error(error);
      }

      // self._startActivity("Login");
    });
  }

  _startActivity(destActivity,params={}){
    const action = { type: "SET_ACTIVITY", value: {activity:destActivity} };
    this.props.dispatch(action);
    this.props.navigation.navigate(destActivity,params);
    this.dataIsReady=false;
  }

  _inProfil(){
    return this.props.activity == "Profil";
  }

  _inDetailProduit(){
    return this.props.activity == "DetailProduit";
  }

  _inListeProduit(){
    return this.props.activity == "ListeProduit";
  }

  _inListeFavoris(){
    return this.props.activity == "ListeFavoris";
  }

  _inListeCommande(){
    return this.props.activity == "ListeCommande";
  }

  _inDetailCommande(){
    return this.props.activity == "DetailCommande";
  }

  _inSuiviLivraison(){
    return this.props.activity == "SuiviLivraison";
  }

  _watchUserPosition = ()=>{
    Permissions.check('location').then(response => {
      // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
      if(response=='authorized'){
        console.log("BaseComponent","start location");
        Geolocation.getCurrentPosition(
          (position)=>{
            console.log("BaseComponent","POSITION : ");
            console.log(position);
          },
          (error)=>{
            console.log("BaseComponent",error);
          },
          { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
        );
      }
    })
  }

  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
        this.getToken();
    } else {
        this.requestPermission();
    }
  }

  async requestPermission() {
    try {
        await firebase.messaging().requestPermission();
        // User has authorised
        this.getToken();
    } catch (error) {
        // User has rejected permissions
        console.log('BaseComponent','permission rejected');
    }
  }

  async getToken() {
    let fcmToken = await AsyncStorage.getItem('fcmToken');
    if (!fcmToken) {
        fcmToken = await firebase.messaging().getToken();
        if (fcmToken) {
            // user has a device token
            await AsyncStorage.setItem('fcmToken', fcmToken);
        }
    }

    this._subscribeOnAllUserTopic();
  }

  _subscribeOnAllUserTopic(){
    const favoriteFood = this.props.favoriteFood;

    for(let i=0;i<favoriteFood.length;i++){
      firebase.messaging().subscribeToTopic(favoriteFood[i]);
    }

    console.log("BaseComponent::_subscribeOnAllUserTopic",favoriteFood)
  }

  async createNotificationListeners() {

    console.log("BaseComponent","createNotificationListeners...");

    /*
    * Triggered when a particular notification has been received in foreground
    * */
    this.notificationListener = firebase.notifications().onNotification((notification) => {
        const { title, body } = notification;
        this.showAlert(title, body);
        console.log("BaseComponent","messageonNotification......");
    });

    /*
    * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
    * */
    this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
        const { title, body } = notificationOpen.notification;
        this.showAlert(title, body);
        console.log("BaseComponent","onNotificationOpened......");
    });

    /*
    * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
    * */
    const notificationOpen = await firebase.notifications().getInitialNotification();
    if (notificationOpen) {
        const { title, body } = notificationOpen.notification;
        this.showAlert(title, body);
        console.log("BaseComponent","notificationOpen......");
    }
    /*
    * Triggered for data only payload in foreground
    * */
    this.messageListener = firebase.messaging().onMessage((message:RemoteMessage) => {
      //process data message
      this.showAlert("message","message  ooooo");
      console.log("BaseComponent",JSON.stringify(message));
    });
  }

  showAlert(title, body) {
    if(title!=undefined && body!=undefined){
      this.refs.alert.alertWithType('success', title, body);
    }

  }

}

export default BaseComponent;
