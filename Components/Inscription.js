import React from 'react'
import BaseComponent from './BaseComponent';
import { StyleSheet, View, Image, Text, BackHandler, TextInput, TouchableOpacity,ScrollView,KeyboardAvoidingView,Alert,AsyncStorage  } from 'react-native'
import stylesDefault from '../Styles/styleDefault'
import Loader from './Loader';
import { logIn,createAccount } from '../API/UtilisateurAPI';
import { connect } from 'react-redux';


class Inscription extends BaseComponent {
  _didFocusSubscription;
  _willBlurSubscription;

  constructor(props) {
    super(props);
    this._didFocusSubscription = props.navigation.addListener('didFocus', payload =>
      BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
    );

    this.reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/

    this.state = {
      nom:"",
      prenom:"",
      email:"",
      motDePasse:"",
      confirmMotDePasse:"",
      loading:false,
      nextSrc:require('../Images/icon/next.png'),
      checkBox:require('../Images/icon/checkbox_empty.png'),
      termsAccepted:false,
      step:1
    }
  }

  _signIn = (emailLogin,passwordLogin) => {


    if(emailLogin.length>0 && passwordLogin.length>0){
      this.setState({loading:true});
      logIn(emailLogin,passwordLogin).then(data=>{
        console.log(data);

        if(data.status==-1 || data.status==-3){
          Alert.alert(
            'Information incorrect',
            'E-mail ou mot de passe incorrect.',
            [
              {text: 'OK', onPress: () => console.log('OK Pressed'),style: 'cancel'},
            ],
            { cancelable: true }
          );
        }else if(data.status==1){
          this._saveUserInfo(data.data);
          AsyncStorage.multiSet([['consommateurId',''+data.data.user.id_consommateur],['userId',''+data.data.user.utilisateur_id],['userName',''+data.data.user.nom],['userFirstName',''+data.data.user.prenom],['userEmail',''+data.data.user.mail],['userToken',''+data.data.token]],(err)=>{
            console.log(err);
            if(err){
                console.log(err);
            }else{
              this.setState({loading:false,showLoginModal:false});
              this._startActivity("Welcome",{name:this.state.nom});
            }
          });
          // this._storeUserData(data.data.user.utilisateur_id,data.data.user.nom,data.data.user.prenom,data.data.user.mail,data.data.token).then((result)=>{
          //     this._startActivity("Profil");
          // });
        }

        this.setState({
          nomLogin:"",
          passwordLogin:"",
          loading:false
        });

      }).catch(error=>{
        console.log(error);
      })
    }else{
      Alert.alert(
        'Information incomplète',
        'Veuillez renseigner vos informations d\'identification',
        [
          {text: 'OK', onPress: () => console.log('OK Pressed'),style: 'cancel'},
        ],
        { cancelable: true }
      );
    }

  }

  _saveUserInfo(data){
    console.log("_saveUserInfo.....");
    const action = { type: "SET_INFO", value: {idUser:data.user.utilisateur_id,token:data.token,userName:data.nom,userFirstName:data.prenom,email:data.mail} };
    this.props.dispatch(action);

    const action2 = { type: "SET_ACTIVITY", value: {activity:"login"} };
    this.props.dispatch(action2);
  }

  _sInscrire(){
    console.log(this.state);

    // if(this.state.nom==""){
    //   this.refs.nom.focus();
    //   Alert.alert(
    //     'Information incomplète',
    //     'Veuillez indiquer votre nom de famille',
    //     [
    //       {text: 'OK', onPress: () => console.log('OK Pressed'),style: 'cancel'},
    //     ],
    //     { cancelable: true }
    //   );
    // }else
    if(this.state.nom==""){
      Alert.alert(
        'Information incomplète',
        'Veuillez indiquer votre prénom',
        [
          {text: 'OK', onPress: () => console.log('OK Pressed'),style: 'cancel'},
        ],
        { cancelable: true }
      );
    }else if(this.state.email==""){
      this.refs.email.focus();
      Alert.alert(
        'Information incomplète',
        'Veuillez indiquer votre adresse e-mail',
        [
          {text: 'OK', onPress: () => console.log('OK Pressed'),style: 'cancel'},
        ],
        { cancelable: true }
      );
    }else if(this.reg.test(this.state.email) === false){
      this.refs.email.focus();
      Alert.alert(
        'Information incomplète',
        'Veuillez saisir une adresse e-mail valide',
        [
          {text: 'OK', onPress: () => console.log('OK Pressed'),style: 'cancel'},
        ],
        { cancelable: true }
      );
    }else if(this.state.motDePasse==""){
      this.refs.password.focus();
      Alert.alert(
        'Information incomplète',
        'Veuillez définir votre mot de passe',
        [
          {text: 'OK', onPress: () => console.log('OK Pressed'),style: 'cancel'},
        ],
        { cancelable: true }
      );
    }
    // else if(this.state.confirmMotDePasse==""){
    //   this.refs.confirmPassword.focus();
    //   Alert.alert(
    //     'Information incomplète',
    //     'Veuillez confirmer votre mot de passe',
    //     [
    //       {text: 'OK', onPress: () => console.log('OK Pressed'),style: 'cancel'},
    //     ],
    //     { cancelable: true }
    //   );
    // }else if (this.state.motDePasse != this.state.confirmMotDePasse) {
    //   this.setState({
    //     motDePasse:"",
    //     confirmMotDePasse:""
    //   });
    //
    //   this.refs.password.focus();
    //
    //   Alert.alert(
    //     'Information non valide',
    //     'Les mots de passe ne correspondent pas. Veuillez re-définir votre mot de passe.',
    //     [
    //       {text: 'OK', onPress: () => console.log('OK Pressed'),style: 'cancel'},
    //     ],
    //     { cancelable: true }
    //   );
    // }
    else if(this.state.motDePasse.length<6){
      this.refs.password.focus();

      Alert.alert(
        'Information non valide',
        'Votre mot de passe doit contenir au moin 6 caractères',
        [
          {text: 'OK', onPress: () => console.log('OK Pressed'),style: 'cancel'},
        ],
        { cancelable: true }
      );
    }else{
      this._enregistrerInfoSurServeur();
    }

  }

  _enregistrerInfoSurServeur(){
    this.setState({loading:true});

    createAccount(this.state.nom,this.state.prenom,this.state.email,this.state.motDePasse).then(data=>{

      this.setState({loading:false});

      console.log(data);

      if(data.status==1){
        this._signIn(this.state.email,this.state.motDePasse);
      }else if(data.status==-2){
        this.refs.email.focus();
        Alert.alert(
          'Information invalide',
          'L\'adresse e-mail "'+this.state.email+'" est déjà utiliser par un compte enregistrer sur Foodwise!',
          [
            {text: 'OK', onPress: () => console.log('OK Pressed'),style: 'cancel'},
          ],
          { cancelable: true }
        );
      }else if(data.status==-3){
        Alert.alert(
          'Information incomplète',
          'Veuillez remplir correctement tous les champs.',
          [
            {text: 'OK', onPress: () => console.log('OK Pressed'),style: 'cancel'},
          ],
          { cancelable: true }
        );
      }

    }).catch(error=>{
      console.log(error);
    })

  }

  componentDidMount() {
    this._willBlurSubscription = this.props.navigation.addListener('willBlur', payload =>
      BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
    );
  }

  onBackButtonPressAndroid = () => {
    return false;
  };

  componentWillUnmount() {
    this._didFocusSubscription && this._didFocusSubscription.remove();
    this._willBlurSubscription && this._willBlurSubscription.remove();
  }

  _acceptTerms(){
    if(this.state.termsAccepted){
      this.setState({
        termsAccepted:false,
        checkBox:require('../Images/icon/checkbox_empty.png')
      });
    }else{
      this.setState({
        termsAccepted:true,
        checkBox:require('../Images/icon/checkbox.png')
      });
    }
  }

  _nextStep(){

    if(this.state.nom==""){
      Alert.alert(
        'Oups',
        "Avant de procéder,il faut d'abord remplir toutes les informations. Merci!",
        [
          {text: 'OK', onPress: () => console.log('OK Pressed'),style: 'cancel'},
        ],
        { cancelable: true }
      );
      return;
    }

    if(this.state.step==2){
      if(this.state.termsAccepted){
        this._sInscrire();
      }else{
        Alert.alert(
          'Oups',
          "Avant de procéder,il faut d'abord remplir toutes les informations. Merci!",
          [
            {text: 'OK', onPress: () => console.log('OK Pressed'),style: 'cancel'},
          ],
          { cancelable: true }
        );
      }
      return;
    }

    this.setState({
      step:this.state.step+1
    });
  }

  _renderCurrentStep(){
    const step = this.state.step;
    if(step==1){
      return(
        <View style={{marginLeft:16,marginRight:16}}>
          <View style={{ flexDirection:"row",paddingLeft:4,paddingRight:16 }}>
            <Text style={[{ flex:1,color:"#fff",fontSize:32 },stylesDefault.barlowRegular]} >Comment veux-tu qu'on t'appelle?</Text>
          </View>

          <View style={{ marginTop:32 }}>
            <View style={styles.form_input}>
              <View style={styles.text_input}>
                <TextInput underlineColorAndroid="#ffffff" style={[styles.input,stylesDefault.barlowRegular]} placeholderTextColor={"#ffffff9a"} placeholder={"Saisir ici ton nom"} onChangeText={(text)=>this.setState({nom:text})} />
              </View>
            </View>
          </View>
        </View>
      )
    }else if(step==2){
      return(
        <View style={{ flex:1 }}>

          <View style={{ flexDirection:"row",marginLeft:16,marginRight:16 }}>
            <Text style={[{ flex:1,color:"#fff",fontSize:22,paddingLeft:150 },stylesDefault.barlowRegular]} > { this.state.nom } !</Text>
          </View>

          <View style={{ marginTop:32 }}>
            <Image style={{ width:147,height:200,resizeMode:Image.resizeMode.contain }} source={require("../Images/main.png")}/>
          </View>

          <View style={{ marginLeft:16,marginRight:16 }}>

            <View style={styles.form_input}>
              <View style={styles.text_input}>
                <TextInput ref="email" underlineColorAndroid="#ffffff" style={[styles.input,stylesDefault.barlowRegular]} placeholderTextColor={"#fff"} placeholder={"Mail"} onChangeText={(text)=>this.setState({email:text})} />
              </View>
            </View>

            <View style={styles.form_input}>

              <View style={styles.text_input}>
                <TextInput ref="password" underlineColorAndroid="#ffffff" style={[styles.input,stylesDefault.barlowRegular]} placeholderTextColor={"#fff"} placeholder={"Mots de passe"} onChangeText={(text)=> this.setState({motDePasse:text})} secureTextEntry={true}/>
              </View>

            </View>

          </View>

          <View style={{ flexDirection:"row",alignItems:"center",paddingLeft:16,paddingRight:16 }}>
            <TouchableOpacity onPress={()=>{ this._acceptTerms() }}>
              <Image style={{ width:32,height:32,resizeMode:Image.resizeMode.contain,marginRight:8 }} source={this.state.checkBox} />
            </TouchableOpacity>
            <Text style={{ flex:1,color:"#fff",fontSize:16 }}>
              J'accepte les <Text style={{ textDecorationLine:"underline" }}>Termes et Conditions</Text> et la <Text style={{ textDecorationLine:"underline" }}>politique de confidentialité</Text>
            </Text>
          </View>

        </View>
      )
    }
  }

  render() {
      return (
        <KeyboardAvoidingView style={{flex:1}} >
          <Loader loading={this.state.loading} />
          <ScrollView style={[styles.main_container,stylesDefault.themePrimary]} keyboardShouldPersistTaps="handled">

            <View style={{ marginTop:50,paddingLeft:4,paddingRight:16,marginLeft:16,marginRight:16 }}>
              <Text style={[{ fontSize:64,color:"#fff" },stylesDefault.barlowBlackItalic]}>Salama { (this.state.step==1)?"!":"" }</Text>
            </View>

            { this._renderCurrentStep() }

          </ScrollView>
          <TouchableOpacity style={{position:"absolute",bottom:16,right:16}} onPress={()=>{ this._nextStep() }} >
            <Image fadeDuration={10} style={{ width:40,height:40,resizeMode:Image.resizeMode.contain }} source={this.state.nextSrc} />
          </TouchableOpacity>
        </KeyboardAvoidingView>
      )
  }
}

const styles = StyleSheet.create({
  main_container:{
    flex:1
  },
  logo_container:{
    flex:1,
    alignItems:"center",
    justifyContent:"flex-end",
    paddingBottom:50,
    paddingTop:50
  },
  logo:{
    width:100,
    height:100
  },
  form_input:{
    flexDirection:"row",
    overflow:"hidden",
    marginBottom:16
  },
  icon:{
    width:20,
    height:20,
    resizeMode:Image.resizeMode.contain
  },
  text_input:{
    flex:1,
    borderBottomColor:"#fff"
  },
  input:{
    color:"#fff",
    paddingBottom:12,
    paddingTop:12,
    paddingLeft:4,
    paddingRight:8,
    fontSize:18
  },
  icon_input:{
    paddingLeft:20,
    paddingRight:20,
    justifyContent:"center",
    backgroundColor:"#4bae32"
  },
  button_container:{
    flexDirection:"row",
    marginTop:16,
    marginBottom:20
  },
  button_sign_up:{
    flex:1,
    alignItems:"center",
    justifyContent:"center",
    borderColor:"#dce1e5",
    borderWidth:2,
    borderRadius:10,
    overflow:"hidden",
    paddingTop:16,
    paddingBottom:16,
    marginRight:4,
    backgroundColor:"#ffffff"
  },
  button_log_in:{
    flex:1,
    backgroundColor:"#4bae32",
    borderColor:"#dce1e5",
    borderWidth:2,
    borderRadius:10,
    overflow:"hidden",
    paddingTop:16,
    paddingBottom:16,
    alignItems:"center",
    marginLeft:4
  },
  button_fb_log_in:{
    flex:1,
    flexDirection:"row",
    alignItems:"center",
    justifyContent:"center",
    backgroundColor:"#4267B2",
    borderColor:"#dce1e5",
    borderWidth:2,
    borderRadius:10,
    overflow:"hidden",
    paddingTop:16,
    paddingBottom:16,
    alignItems:"center",
    marginLeft:4
  },
  button_google_log_in:{
    flex:1,
    flexDirection:"row",
    alignItems:"center",
    justifyContent:"center",
    backgroundColor:"#ffffff",
    borderColor:"#dce1e5",
    borderWidth:2,
    borderRadius:10,
    overflow:"hidden",
    paddingTop:16,
    paddingBottom:16,
    alignItems:"center",
    marginLeft:4
  },
  button_log_in_text:{
    fontWeight:"bold",
    color:"#ffffff"
  },
  button_sign_up_text:{
    fontWeight:"bold",
    color:"#4bae32"
  }
});

const mapStateToProps = (state) => {
  return state
}

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: (action) => { dispatch(action) }
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(Inscription);
