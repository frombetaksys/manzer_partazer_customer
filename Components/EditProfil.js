import React from 'react'
import BaseComponent from './BaseComponent';
import { StyleSheet, View, Image, Text, BackHandler, TextInput, TouchableOpacity,ScrollView,KeyboardAvoidingView,Alert, AsyncStorage  } from 'react-native'
import stylesDefault from '../Styles/styleDefault'
import Loader from './Loader';
import { connect } from 'react-redux';
import { editProfil } from '../API/UtilisateurAPI';




class EditProfil extends BaseComponent {
  _didFocusSubscription;
  _willBlurSubscription;

  constructor(props) {
    super(props);

    this.state = {
      consommateurId:"",
      userId:"",
      nom: "",
      prenom:"",
      email:"",
      numTel:"",
      adresseLivraison:"",
      mobileBanking:"",
      token:"",
      loading:false
    }

    AsyncStorage.multiGet(['userId',"userToken","consommateurId"], (err, result) => {
      if(result!=null){
        console.log(result);
        this.setState({
          userId :  result[0][1],
          token : result[1][1],
          consommateurId : result[2][1]
        })
      }else{
        this._startActivity('Login');
      }
    });
  }

  _back(){
    this._startActivity("Profil");
  }

  componentDidMount() {
    this.setState({
      nom:this.props.navigation.state.params.userName,
      prenom:this.props.navigation.state.params.userFirstName,
      email:this.props.navigation.state.params.userEmail,
      numTel:(this.props.navigation.state.params.numeroTel=="-1")?"":this.props.navigation.state.params.numeroTel,
      adresseLivraison:(this.props.navigation.state.params.adresseLivraison=="(Non défini)")?"":this.props.navigation.state.params.adresseLivraison,
      mobileBanking:(this.props.navigation.state.params.mobileBanking=="(Non défini)")?"":this.props.navigation.state.params.mobileBanking
    })
  }

  _saveEdit(){
    this.setState({loading:true});

    editProfil(this.state).then(data=>{

      console.log("EditProfil::data > "+data);

      this.setState({loading:false});
      this._back();

    }).catch(error=>{
      console.log(error);
      this.setState({loading:false});

    });
  }

  _renderMailForm(){
    if(this.state.email.indexOf("@facebook")==-1){
      return(
        <View>
          <Text style={[styles.label_input,stylesDefault.textThemePrimary]}>E-mail</Text>
          <View style={styles.form_input}>
            <View style={styles.text_input}>
              <TextInput ref="email" underlineColorAndroid="#dce1e5" style={styles.input} placeholder={"Saisir votre adresse e-mail"} textContentType={"emailAddress"} value={ this.state.email }  keyboardtype={"email-address"} onChangeText={(text)=> this.setState({email:text})}/>
            </View>
          </View>
        </View>
      )
    }
  }

  //<Text style={{fontSize:20,fontFamily:"Barlow_Bold"}}>Changer la photo</Text>

  render() {
      return (
        <KeyboardAvoidingView style={{flex:1,backgroundColor:"#fff"}}>
          <Loader loading={this.state.loading} />
          <View ref="actionBar" style={[{
            flexDirection:"row",paddingTop:8,paddingBottom:8,alignItems:"center",justifyContent:"center" },stylesDefault.themePrimary]}>

            <TouchableOpacity style={{ padding:10 }} onPress={()=>{this._back()}}>
              <Image source={require("../Images/other/left_arrow.png")} style={{ height:20, width:20, resizeMode:Image.resizeMode.contain,tintColor:"#fff" }} />
            </TouchableOpacity>

            <Text style={[{ flex:1,fontSize:20,color:"#fff",fontFamily:"Barlow_Bold" }]}>
              Mise à jour de mon profil
            </Text>

            <TouchableOpacity style={{ padding:10 }} onPress={()=>{this._saveEdit()}}>
              <Image source={require("../Images/icon/save.png")} style={{ height:20, width:20, resizeMode:Image.resizeMode.contain,tintColor:"#fff" }} />
            </TouchableOpacity>

          </View>
          <ScrollView style={styles.main_container} keyboardShouldPersistTaps="handled">
            <View style={styles.logo_container}>
              <Image style={styles.logo} source={require('../Images/other/avatar_placeholder.png')} />

            </View>

            <View style={styles.form_container}>
              <Text style={[styles.label_input,stylesDefault.textThemePrimary]}>Nom</Text>
              <View style={styles.form_input}>
                <View style={styles.text_input}>
                  <TextInput ref="nom" underlineColorAndroid="#dce1e5" style={styles.input} placeholder={"Saisir votre nom"} value={ this.state.nom } onChangeText={(text)=> this.setState({nom:text})}/>
                </View>
              </View>

              <Text style={[styles.label_input,stylesDefault.textThemePrimary]}>Prénom(s)</Text>
              <View style={styles.form_input}>
                <View style={styles.text_input}>
                  <TextInput ref="prenom" underlineColorAndroid="#dce1e5" style={styles.input} placeholder={"Saisir votre prénom"} value={ this.state.prenom } onChangeText={(text)=> this.setState({prenom:text})}/>
                </View>
              </View>

              { this._renderMailForm() }


              <Text style={[styles.label_input,stylesDefault.textThemePrimary]}>Numéro téléphone</Text>
              <View style={styles.form_input}>
                <View style={styles.text_input}>
                  <TextInput ref="numTel" underlineColorAndroid="#dce1e5" style={styles.input} placeholder={"Saisir votre numéro de télephone"} textContentType={"telephoneNumber"}  value={ this.state.numTel }  keyboardtype={"phone-pad"} onChangeText={(text)=> this.setState({numTel:text})}/>
                </View>
              </View>

              <Text style={[styles.label_input,stylesDefault.textThemePrimary]}>Mobile Banking(Numéro téléphone)</Text>
              <View style={styles.form_input}>
                <View style={styles.text_input}>
                  <TextInput ref="mobilebanking" underlineColorAndroid="#dce1e5" style={styles.input} placeholder={"(ex : 03X XX XXX XX)"} textContentType={"telephoneNumber"} value={ this.state.mobileBanking }  keyboardtype={"phone-pad"} onChangeText={(text)=> this.setState({mobileBanking:text})}/>
                </View>
              </View>

              <Text style={[styles.label_input,stylesDefault.textThemePrimary]}>Adresse de livraison</Text>
              <View style={styles.form_input}>
                <View style={styles.text_input}>
                  <TextInput ref="adresseLivraison" underlineColorAndroid="#dce1e5" style={styles.input} placeholder={"Adresse de livraison de vos commandes"} value={ this.state.adresseLivraison } onChangeText={(text)=> this.setState({adresseLivraison:text})}/>
                </View>
              </View>

            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      )
  }
}

const styles = StyleSheet.create({
  main_container:{
    flex:1
  },
  logo_container:{
    flex:1,
    alignItems:"center",
    justifyContent:"flex-end",
    paddingTop:32,
    paddingBottom:32,
    borderRadius:100
  },
  logo:{
    width:100,
    height:100,
    borderRadius:100,
    marginBottom:16
  },
  form_container:{
    paddingLeft:8,
    paddingRight:8
  },
  form_input:{
    flexDirection:"row",
    overflow:"hidden",
    marginBottom:16
  },
  icon:{
    width:20,
    height:20,
    resizeMode:Image.resizeMode.contain
  },
  text_input:{
    flex:1
  },
  input:{
    fontSize:16,
    fontFamily:"Barlow_Regular"
  },
  icon_input:{
    paddingLeft:20,
    paddingRight:20,
    justifyContent:"center",
    backgroundColor:"#4bae32"
  },
  button_container:{
    flexDirection:"row",
    marginTop:16,
    marginBottom:64
  },
  button_sign_up:{
    flex:1,
    alignItems:"center",
    justifyContent:"center",
    borderColor:"#dce1e5",
    borderWidth:2,
    borderRadius:10,
    overflow:"hidden",
    paddingTop:16,
    paddingBottom:16,

    marginRight:4,
    backgroundColor:"#ffffff"
  },
  button_log_in:{
    flex:1,
    backgroundColor:"#4bae32",
    borderColor:"#dce1e5",
    borderWidth:2,
    borderRadius:10,
    overflow:"hidden",
    paddingTop:16,
    paddingBottom:16,
    alignItems:"center",
    marginLeft:4
  },
  button_log_in_text:{
    fontWeight:"bold",
    color:"#ffffff"
  },
  button_sign_up_text:{
    fontWeight:"bold",
    color:"#4f4f4f"
  },
  label_input:{
    fontFamily:"Barlow_BlackItalic",
    fontSize:16
  }
  });

const mapStateToProps = (state) => {
  return state
}

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: (action) => { dispatch(action) }
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(EditProfil);
