import React from 'react'
import BaseComponent from './BaseComponent';
import { StyleSheet, View, Image, Text, BackHandler, TextInput, TouchableOpacity,ScrollView,Alert,FlatList,Modal  } from 'react-native'
import stylesDefault from '../Styles/styleDefault'
import Loader from './Loader';
import ProduitItem from './ProduitItem';
import DrawerItem from './DrawerItem';
import { getListeFavoris,ajouterFavoris } from '../API/ProduitAPI';
import { connect } from 'react-redux';
import { objectIsEmpty } from '../Helper/StandardHelper';


class ListeFavoris extends BaseComponent {
  _didFocusSubscription;
  _willBlurSubscription;

  constructor(props) {
    super(props);

    this._didFocusSubscription = props.navigation.addListener('didFocus', payload =>
      BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
    );

    this.state = {
      prixMin:0,
      prixMax:30000,
      modalPrixVisible:false,
      modalInfoNutriVisible:false,
      infoNutriSelected:[],
      filtreApplique : [],
      date:"",
      filtre:{value:0,label:"Aucun"},
      produits:[],
      loading:false,
      userId :  "",
      userName : "",
      userFirstName : "",
      userEmail : "",
      userToken : "",
      moreDataAvailable : true,
      reset:false
    }

    this.dataIsReady = false;

  }


  _back(){
    this.dataIsReady = false;

    // const destActivity = this.props.navigation.state.params.src;

    this._startActivity("ListeProduit");
  }

  componentDidMount() {
    this._willBlurSubscription = this.props.navigation.addListener('willBlur', payload =>
      BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
    );
  }

  onBackButtonPressAndroid = () => {
    this._back();
    return true;
  };

  componentWillUnmount() {
    this._didFocusSubscription && this._didFocusSubscription.remove();
    this._willBlurSubscription && this._willBlurSubscription.remove();
  }

  _checkData(){
    console.log("ListeFavoris","dataIsReady : "+this.dataIsReady);
    if(!this.dataIsReady && this._inListeFavoris()){
      this.dataIsReady = true;
      this._getUserInfo();
    }
  }

  _addToFavoris = (produit)=>{

    produit.isFavoris = true;
    this.forceUpdate();

    ajouterFavoris({token:this.state.userToken,id_donation:produit.id,id_utilisateur:this.state.userId}).then(data=>{

      console.log(data);

      if(data.status!=1){
        produit.isFavoris = false;
        this.forceUpdate();

        this.setState({loading:false});
        Alert.alert(
          'Connexion interrompue',
          'Une erreur s\'est produite lors de l\'enregistrement de  votre préférence',
          [
            {text: 'Ok', onPress: () => console.log("ok") , style:"cancelable"},
          ],
          { cancelable: false }
        );
      }

    }).catch(error=>{
      produit.isFavoris = false;
      this.forceUpdate();

      this.setState({loading:false});
      Alert.alert(
        'Connexion interrompue',
        'Une erreur s\'est produite lors de l\'enregistrement de  votre préférence',
        [
          {text: 'Ok', onPress: () => console.log("ok") , style:"cancelable"},
        ],
        { cancelable: false }
      );
    });
  }

  _removeToFavoris = (produit)=>{

    produit.isFavoris = false;
    this.forceUpdate();

    ajouterFavoris({token:this.state.userToken,id_donation:produit.id,id_utilisateur:this.state.userId}).then(data=>{

      console.log(data);

      if(data.status!=1){
        produit.isFavoris = true;
        this.forceUpdate();

        this.setState({loading:false});
        Alert.alert(
          'Connexion interrompue',
          'Une erreur s\'est produite lors de l\'enregistrement de  votre préférence',
          [
            {text: 'Ok', onPress: () => console.log("ok") , style:"cancelable"},
          ],
          { cancelable: false }
        );
      }

    }).catch(error=>{
      produit.isFavoris = true;
      this.forceUpdate();

      this.setState({loading:false});
      Alert.alert(
        'Connexion interrompue',
        'Une erreur s\'est produite lors de l\'enregistrement de  votre préférence',
        [
          {text: 'Ok', onPress: () => console.log("ok") , style:"cancelable"},
        ],
        { cancelable: false }
      );
    });
  }

  _toggleFavoris = (produit)=>{
    if(produit.isFavoris){
      this._removeToFavoris(produit);
    }else{
      this._addToFavoris(produit);
    }
  }

  _showDetailProduit = (produitId)=>{
    this._startActivity("DetailProduit",{token:this.state.userToken,id_donation:produitId,id_utilisateur:this.state.userId,src:"ListeFavoris"});
    this.dataIsReady = false;
    setTimeout(()=>{
      if(this.state.produits.length<=10){
        this.setState({produits:[]});
      }else{
        let newData = this.state.produits.filter((item,index)=> index<this.currentIndex );
        this.setState({produits:newData});
      }
    },1000);

  }

  _getUserInfo(){
    this.setState({loading:true});

    this._getUserDataInStorage((err, result) => {
      if(result!=null){
        this.setState({
          userId :  result[0][1],
          userName : result[1][1],
          userFirstName : result[2][1],
          userEmail : result[3][1],
          userToken : result[4][1]
        });
        this._getListeProduit();
      }else{
        this.dataIsReady = false;
        this.setState({produits:[],loading:false});
        this._startActivity("Login");
      }
    });
  }

  _showPanier(){
    this.dataIsReady = false;
    this._startActivity('Panier');
    setTimeout(()=>{

      if(this.state.produits.length<=10){
        this.setState({produits:[]});
      }else{
        let newData = this.state.produits.filter((item,index)=> index<this.currentIndex );
        this.setState({produits:newData});
      }
    },1000);
  }

  _getListeProduit(reachedEnd=false){

    let self = this;

    if(reachedEnd){
      this.currentIndex += 10;
    }

    getListeFavoris({token:this.state.userToken,id_utilisateur:this.state.userId}).then(data=>{

        console.log(data);

        let status = data.status;
        let produitsFromServer = data.data;

        if(status==1){

          let copyOfCurrentProduits = this.state.produits;

          if(self.state.reset)
            copyOfCurrentProduits = [];

          if(produitsFromServer.length>0){

            for(let i=0;i<produitsFromServer.length;i++){
              copyOfCurrentProduits.push({
                id:produitsFromServer[i].id_donation,
                nom:produitsFromServer[i].nom_produit,
                categorie:produitsFromServer[i].type_aliment,
                prix:produitsFromServer[i].prix,
                prixNormal:produitsFromServer[i].prixNormal,
                datePeremption:produitsFromServer[i].dateLimite,
                reduction:produitsFromServer[i].pourcentage_reduction,
                isFavoris:produitsFromServer[i].isFavoris,
                photo:produitsFromServer[i].photo,
                adresseRamassage:(objectIsEmpty(produitsFromServer[i].adresse_rammassage))?{label:"Non défini",ville:""}:produitsFromServer[i].adresse_rammassage,
                optionLivraison:{etat:0,prix:1000},
                qte:parseInt(produitsFromServer[i].quantite),
                minQte:parseInt(produitsFromServer[i].minimum_qte_vente),
                entreprise:{
                  nom:produitsFromServer[i].entrepriseName,
                  logo:produitsFromServer[i].logoEntreprise
                }
              });
            }

            this.setState({
              loading:false,
              produits:copyOfCurrentProduits,
              reset:false
            });

          }else if(reachedEnd && this.state.moreDataAvailable && produitsFromServer.length==0){
            this.currentIndex -= 10;
            this.setState({loading:false,moreDataAvailable:false,reset:false});
          }else{
            this.setState({
              loading:false,
              produits:[],
              reset:false
            });
          }

        }else if(status==0){
          Alert.alert(
            'Session expirée',
            'Votre session a expiré.Veuillez vous reconnecter!',
            [
              {text: 'Ok', onPress: () => {
                this.setState({loading:false});
                self._logout();
              }, style:"cancelable"}
            ],
            { cancelable: false }
          );
        }else{
          this.setState({loading:false});
          Alert.alert(
            'Connexion interrompue',
            'Une erreur est survenue lors de la récupération de la liste des produits en vente',
            [
              {text: 'Réessayer', onPress: () => {
                self.dataIsReady = false;
                self._checkData();
              }, style:"cancelable"},
            ],
            { cancelable: false }
          );
        }

    }).catch(error=>{
      console.log(error);
      this.setState({loading:false});
      Alert.alert(
        'Connexion interrompue',
        'Une erreur est survenue lors de la récupération de la liste des produits en vente',
        [
          {text: 'Réessayer', onPress: () => {
            self.dataIsReady = false;
            self._checkData();
          }, style:"cancelable"},
        ],
        { cancelable: false }
      );
    });
  }

  _produitInPanier=(produit)=>{
    return this.props.panier.findIndex(item => item.produit.id === produit.id)!=-1;
  }

  _qteInPanier=(produit)=>{
    const indexProduit = this.props.panier.findIndex(item => item.produit.id === produit.id);
    if(indexProduit!=-1){
      return this.props.panier[indexProduit].qte;
    }
    return 0;
  }

  _addToPanier = (produit,qte)=>{
    const action = { type: "ADD_IN_PANIER", value: {produit:produit,qte:qte} };
    this.props.dispatch(action);
    this.forceUpdate();
  }

  _doAction = (action)=>{
    if(action=="0"){
      this._askLogout();
    }else if(action!="#"){
      this._startActivity(action);
    }
  }

  _renderMainActionZone(){
    return (
      <View style={{ paddingTop:8,paddingBottom:8,paddingLeft:8,paddingRight:8,backgroundColor:"#f0f0f0",borderBottomWidth:1,borderBottomColor:"#00000010" }}>

        <View style={{ flexDirection:"row" }} >

          <TouchableOpacity style={{ paddingRight:10,alignItems:"center",justifyContent:"center" }} onPress = { ()=>{ this.openControlPanel() } }>
            <Image style={{ width:25,height:25,resizeMode:Image.resizeMode.contain,tintColor:"#4f4f4f",opacity:0.9 }} source={require('../Images/other/menu.png')} />
          </TouchableOpacity>

          <View style={{ flexDirection:"row",flex:1,backgroundColor:"#ffffff",borderRadius:100,paddingLeft:20,paddingRight:20,paddingTop:0,paddingBottom:0,alignItems:"center" }}>
            <TextInput underlineColorAndroid="#ffffff" placeholderTextColor="#4f4f4f"  placeholder={"Saisir un mots clé..."} style={{ flex:1,paddingTop:8,paddingBottom:8,fontSize:15 }} onChangeText={(text) => this._findByMotsCle(text)} />
            <Image style={{ width:20,height:20,resizeMode:Image.resizeMode.contain,tintColor:"#4f4f4f" }} source={require('../Images/other/search.png')} />
          </View>

        </View>

        <View style={{ flexDirection:"row",marginTop:8 }}>

          <TouchableOpacity style={{ flex:1,alignItems:"center",justifyContent:"center",backgroundColor:"#dfe6e9aa",borderRadius:100,paddingTop:8,paddingBottom:8,paddingLeft:16,paddingRight:16,marginRight:2 }}>

            <View style={{flexDirection:"row",alignItems:"center",justifyContent:"center"}}>
              <Image style={{ width:14,height:14,resizeMode:Image.resizeMode.contain,tintColor:"#636e72",opacity:0.9 }} source={require('../Images/other/calendar.png')} />

              <Text style={{ color:"#636e72",fontWeight:"bold",marginLeft:8 }}>Date limite</Text>

              <Image style={{ marginLeft:8,width:14,height:14,resizeMode:Image.resizeMode.contain,tintColor:"#636e72",opacity:0.5 }} source={require('../Images/other/down_arrow.png')} />

            </View>

            <View style={{height:10,flexDirection:"row",alignItems:"center",justifyContent:"center"}}>

              <DatePicker
                date={this.state.date}
                mode="date"
                placeholder="Choisir une date"
                format="DD/MM/YYYY"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                showIcon={false}
                customStyles={{
                  dateInput:{
                    borderWidth:0,
                    padding:0
                  },
                  dateText:{
                    color:"#636e72",
                    fontSize:12
                  }
                }}
                onDateChange={(date) => { this._setDateLimite(date) }}
              />

            </View>

          </TouchableOpacity>

          <TouchableOpacity style={{ flex:1,alignItems:"center",justifyContent:"center",backgroundColor:"#dfe6e9aa",borderRadius:100,paddingTop:8,paddingBottom:8,paddingLeft:16,paddingRight:16,marginLeft:2 }} onPress={()=> this._renderFilterView() }>

            <View style={{flexDirection:"row",alignItems:"center",justifyContent:"center"}}>
              <Image style={{ width:14,height:14,resizeMode:Image.resizeMode.contain,tintColor:"#636e72",opacity:0.9 }} source={require('../Images/other/filter.png')} />

              <Menu ref="filterMenu" style={{ height:10,alignItems:"center",justifyContent:"center" }}>
                <MenuTrigger customStyles={{triggerText:{ color:"#636e72",fontWeight:"bold",marginLeft:8 }}} text='Filtre' />
                <MenuOptions>

                  <CheckedOption onSelect={() => this._toggleFilter(0)} checked={this._inFilterApplique(0)} text={this.filtreApplicable[0]} />
                  <CheckedOption onSelect={() => this._toggleFilter(1)} checked={this._inFilterApplique(1)} text={this.filtreApplicable[1]} />
                  <CheckedOption onSelect={() => this._toggleFilter(2)} checked={this._inFilterApplique(2)} text={this.filtreApplicable[2]}/>
                  <CheckedOption onSelect={() => this._toggleFilter(3)} checked={this._inFilterApplique(3)} text={this.filtreApplicable[3]}/>
                </MenuOptions>
              </Menu>

              <Image style={{ marginLeft:8,width:14,height:14,resizeMode:Image.resizeMode.contain,tintColor:"#636e72",opacity:0.5 }} source={require('../Images/other/down_arrow.png')} />

            </View>

            { this._renderFilterValue() }

          </TouchableOpacity>

        </View>

      </View>
    )
  }

  _renderListeZone(){

    return (
      <View style={{ flex:1 }}>

        { this._renderListe() }

        { this._renderEmptyResult() }

      </View>
    )
  }

  _renderListe(){
    if(this.state.produits.length!=0){
      return(
        <FlatList
          style = {{ flex:1 }}
          data={this.state.produits}
          keyExtractor={(item) => item.id.toString()}
          renderItem={({item}) => (
            <ProduitItem
              produit={item}
              favorisList = {true}
              showDetailProduit = { this._showDetailProduit }
              addToPanier = { this._addToPanier }
              produitInPanier = { this._produitInPanier }
              qteInPanier = { this._qteInPanier }
              toggleFavoris = { this._toggleFavoris }
            />
          )}
          onEndReachedThreshold={0.1}
          onEndReached={() => {
            console.log("onEndReached","moreDataAvailable : "+this.state.moreDataAvailable+" && produits.length : "+this.state.produits.length>8);
            if(this.state.moreDataAvailable && this.state.produits.length>8){
              this.setState({loading:true});
              this._getListeProduit(true);
            }
          }}
        />
      )
    }
  }

  _renderEmptyResult(){
    if(this.state.produits.length==0 && !this.state.loading){
      return(
        <View style={{ flex:1,alignItems:"center",justifyContent:"center" }}>

          <View style={{ alignItems:"center",justifyContent:"center" }}>
            <Image source={ require('../Images/other/empty_box.png') } style={{ width:100,height:100,opacity:0.25 }} />
            <Text style={{ fontSize:16,color:"#4f4f4f",opacity:0.75,marginTop:16 }}>{ "Aucun produit n'a été trouvé" }</Text>
          </View>

        </View>
      )
    }
  }

  render() {
      this._checkData();
      return (
          <View style={{ flex:1,backgroundColor:"#ffffff" }} >

          <View ref="actionBar" style={[{
            flexDirection:"row",paddingTop:8,paddingBottom:8,alignItems:"center",justifyContent:"center" },stylesDefault.themePrimary]}>

            <TouchableOpacity style={{ padding:10 }} onPress={()=>{this._back()}}>
              <Image source={require("../Images/other/left_arrow.png")} style={{ height:20, width:20, resizeMode:Image.resizeMode.contain,tintColor:"#fff" }} />
            </TouchableOpacity>

            <Text style={[{ flex:1,fontSize:20,color:"#fff",fontFamily:"Barlow_Bold" }]}>
              Mes produits favoris
            </Text>

          </View>

            <Loader loading={this.state.loading} />

            { this._renderListeZone() }

          </View>
      )
  }
}

const drawerStyles = {
  drawer: { shadowColor: '#000000', shadowOpacity: 0.8, shadowRadius: 3},
  main: {paddingLeft: 3},
}

const styles = StyleSheet.create({

});

const mapStateToProps = (state) => {
  return state
}

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: (action) => { dispatch(action) }
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(ListeFavoris);
