import React from 'react';
import { View,Text,TouchableOpacity } from 'react-native';
import Modal from "react-native-modal";
import { Rating, AirbnbRating } from 'react-native-ratings';

class SatisfactionModal extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      isVisible:this.props.showRating,
      satisfaction:0
    }
  }

  _close(){
    this.setState({isVisible:false});
  }

  _saveNote(note){
    this.setState(note);
  }

  _pushReviews(){
    this.props.saveSatisfaction({satisfaction:this.state.satisfaction});
    this.setState({ isVisible : false});
  }

  render(){
    return(
      <Modal isVisible={ this.state.isVisible }>
        <View style={{ flex: 1,alignItems:"center",justifyContent:"center" }}>
          <View style={{ backgroundColor:"#ffffff",borderRadius:4,padding:16 }}>

            <View style={{ alignItems:"center",justifyContent:"center" }}>
              <Text style={{ textAlign:"center",fontSize:18,color:"#636e72" }}>Wanne give a feedback to Tovo congratulating him for his good service or improving his service through a constructive remark ?</Text>
            </View>

            <View style={{ backgroundColor:"#00000025", height:1, opacity:0.5,marginTop:8,marginBottom:8 }}></View>

            <View style={{ alignItems:"center",justifyContent:"center",marginBottom:16,marginTop:8 }}>

              <AirbnbRating
                count={5}
                defaultRating={0}
                size={30}
                onFinishRating={(rating)=>{ this._saveNote({ satisfaction:rating }) }}
              />

            </View>


            <View style={{ flexDirection:"row",alignItems:"center",justifyContent:"center",marginTop:8 }}>

              <TouchableOpacity style={{flexDirection:"row",alignItems:"center",justifyContent:"center",backgroundColor:"#dfe6e9",borderColor:"#dce1e5",borderWidth:2,borderRadius:10,paddingTop:10,paddingBottom:10,paddingLeft:10,paddingRight:10,marginRight:8}} onPress={()=>{ this._close() }}>
                <Text style={{fontSize:16,fontWeight:"bold",color:"#636e72"}}>ANNULER</Text>
              </TouchableOpacity>

              <TouchableOpacity style={{flexDirection:"row",alignItems:"center",justifyContent:"center",backgroundColor:"#00b894",borderColor:"#dce1e5",borderWidth:2,borderRadius:10,paddingTop:10,paddingBottom:10,paddingLeft:10,paddingRight:10,marginLeft:8}} onPress={()=>{ this._pushReviews() }}>
                <Text style={{fontSize:16,fontWeight:"bold",color:"#ffffff"}}>ENVOYER</Text>
              </TouchableOpacity>

            </View>
          </View>
        </View>
      </Modal>
    )
  }

}

export default SatisfactionModal;
