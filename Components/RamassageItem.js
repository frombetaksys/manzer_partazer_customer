import React from 'react'
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native'
import { mileSeparator } from '../Helper/StandardHelper';
import RadioGroup from 'react-native-radio-buttons-group';



class RamassageItem extends React.Component {

  _setModeLivraison(data,produit){
    if(data[0].selected){
      produit.optionLivraison.etat = 0;
    }else if(data[1].selected){
      produit.optionLivraison.etat = 1;
    }
    this.props.getCoutTotal();
  }

  _renderOptionLivraison(produit){
    const choiceLivraison = [
      {
        label:"Non",
        value:0,
        color:"#00b894",
        selected:(produit.optionLivraison.etat==0)?true:false
      },
      {
        label:"Oui ("+mileSeparator(produit.optionLivraison.prix)+"MGA)",
        value:1,
        color:"#00b894",
        selected:(produit.optionLivraison.etat==1)?true:false
      }
    ]

    if(produit.optionLivraison.etat<0){
      return(
        <View style={{ justifyContent:"center",alignItems:"center" }}>
          <Text style={{ justifyContent:"center",alignItems:"center",opacity:0.75 }}>(Aucune option de livraison)</Text>
        </View>
      )
    }else{
      return(
        <View style={{ justifyContent:"center",alignItems:"center" }}>
          <Text style={{ justifyContent:"center",alignItems:"center" }}>Demander une livraison :</Text>
          <View style={{ flexDirection:"row" }}>
            <RadioGroup layout="row" flexDirection="row" radioButtons={choiceLivraison} onPress={(data)=>{ this._setModeLivraison(data,produit) }} />
          </View>
        </View>
      )
    }
  }

  render(){
    const { ramassageItem } = this.props;

    const produit = ramassageItem.produit;

    return(
      <TouchableOpacity style={styles.main_container} onPress={ ()=> console.log("omeo detail") }>
        <View style={{ flexDirection:"row",alignItems:"center" }}>
          <Image style={{ width:110,height:110,resizeMode:Image.resizeMode.cover,borderRadius:4,backgroundColor:"#00000010" }}
            source={(produit.photo=="-1")?require('../Images/other/no_img_placeholder.jpg'):{uri:produit.photo}}
          />
          <View style={{ flex:1, paddingLeft:8, margin:5 }}>

            <View style={{ flexDirection:'row' }}>
              <Text style={ styles.title_text }>{ produit.nom }</Text>
            </View>

            <View style={{ flex:1,flexDirection:"row" }}>

              <View style={{ flex:1 }}>


                <View style={{ flexDirection:"row" }}>
                  <Text style={{ color:'#969696',textDecorationLine:"underline" }}>Date limite:</Text>
                  <Text style={{ flex:1,color:'#969696' }}> { produit.datePeremption }</Text>
                </View>

                <View style={{ flexDirection:"row" }}>
                  <Text style={{ color:'#969696',textDecorationLine:"underline" }}>Prix unité :</Text>
                  <Text style={{ flex:1,color:'#969696' }}> {mileSeparator(produit.prix)} MGA</Text>
                </View>

                <View style={{ flexDirection:"row" }}>
                  <Text style={{ color:'#969696',textDecorationLine:"underline" }}>Réduction :</Text>
                  <Text style={{ flex:1,color:'#969696' }}> -{ produit.reduction }%</Text>
                </View>

                <View style={{ flexDirection:"row" }}>
                  <Text style={{ color:'#969696',textDecorationLine:"underline" }}>Qté :</Text>
                  <Text style={{ flex:1,color:'#969696' }}> {ramassageItem.qte}</Text>
                </View>

              </View>

            </View>

            <View style={{ flexDirection:"row" }}>
              <View style={{ flex:1,flexDirection:"row" }}>

              </View>
              <View style={{ alignItems:"center",justifyContent:"center" }}>
                <Text style={{ backgroundColor:"#00b894",paddingTop:4,paddingBottom:4,paddingLeft:8,paddingRight:8,color:"#ffffff",fontWeight:"bold",fontSize:15,borderRadius:2 }}>{ mileSeparator(produit.prix*ramassageItem.qte) } MGA</Text>
              </View>
            </View>

          </View>
        </View>
        { this._renderOptionLivraison(produit) }
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  main_container: {
    backgroundColor: '#FFFFFF',
    borderBottomWidth:1,
    borderColor:"#00000015",
    alignItems:"center",
    paddingTop:6,
    paddingBottom:6,
    paddingLeft:6,
    paddingRight:6,
    marginLeft:8,
    marginRight:8,
  },
  title_text: {
    flex:1,
    fontSize:20,
    fontWeight: 'bold',
    flexWrap : 'wrap',
    color:"#4f4f4f"
  },
  favorite_image: {
      width: 40,
      height: 40

  }
})

export default RamassageItem;
