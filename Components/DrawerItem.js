import React from 'react'
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native'

class DrawerItem extends React.Component {

  render(){
    const { menu } = this.props;
    return(
      <TouchableOpacity style={{ flexDirection:"row",marginTop:4,marginBottom:4,padding:8,alignItems:"center",backgroundColor:(menu.actif)?"transparent":"transparent",borderRadius:4 }} onPress={ ()=> this.props.doAction(menu.action) }>

        <Image style={{ width:24,height:24,resizeMode:Image.resizeMode.contain }} source={ menu.icon } />

        <Text style={{ marginLeft:16,fontSize:14,color:"#fff",fontFamily:"Barlow_Bold" }}>{ menu.label }</Text>

      </TouchableOpacity>
    )
  }
}


export default DrawerItem;
