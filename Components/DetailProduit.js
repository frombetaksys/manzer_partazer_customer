import React from 'react'
import BaseComponent from './BaseComponent';
import { StyleSheet, View, Text, Image, TouchableOpacity,ScrollView,Alert,ToastAndroid,BackHandler } from 'react-native'
import Spinner from './Tools/react-native-number-spinner';
import { connect } from 'react-redux';
import { getDetailProduit,ajouterFavoris,ajouterVueProduit } from '../API/ProduitAPI';
import Loader from './Loader';
import { mileSeparator,objectIsEmpty } from '../Helper/StandardHelper';
import stylesDefault from '../Styles/styleDefault'


class DetailProduit extends BaseComponent {

  _didFocusSubscription;
  _willBlurSubscription;

  constructor(props){
    super(props);

    this._didFocusSubscription = props.navigation.addListener('didFocus', payload =>
      BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
    );

    this.produit = {};

    this.state={
      photoProduit:"-1",
      nomProduit:"...",
      nombre:0,
      dateLimite:"../../....",
      prixNormal:"...",
      prixReduit:"...",
      reduction:"...",
      quantiteSauve:1,
      entrepriseName:"...",
      vue:0,
      logoEntreprise:"",
      isFavoris:false,
      adresseRamassage:"...",
      periode_recuperation:null,
      min:1,
      max:1,
      dataIsReady:false,
      contact:"(Non renseigné)"
    }

  }

  componentDidMount() {
    this._willBlurSubscription = this.props.navigation.addListener('willBlur', payload =>
      BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
    );
  }

  onBackButtonPressAndroid = () => {
    this._backToList();
    return true;
  };

  _checkData(){
    console.log("DetailProduit","dataIsReady : "+this.dataIsReady);
    console.log("DetailProduit","_inDetailProduit : "+this._inDetailProduit());

    console.log("CurrentActivity","==> "+this.props.activity);

    if(!this.dataIsReady && this._inDetailProduit()){
      this.dataIsReady = true;
      this._getDetailProduit();
      this._viewProduit();
    }
  }

  _getDetailProduit(){

    console.log("DetailProduit","_getDetailProduit");

    this.setState({loading:true});

    getDetailProduit({token:this.props.navigation.state.params.token,id_donation:this.props.navigation.state.params.id_donation,id_utilisateur:this.props.navigation.state.params.id_utilisateur}).then(data=>{

      console.log(data);

      let status = data.status;
      let produit = data.data;

      this.produit = produit;

      const panier = this.props.panier;

      const panierItemIndex = panier.findIndex(item => item.produit.id === produit.id_donation);

      console.log("DetailProduit",panierItemIndex);

      if(status==1){
        this.setState({
          nomProduit:produit.nom_produit,
          nombre:produit.quantite,
          dateLimite:produit.dateLimite,
          prixNormal:produit.prixNormal,
          prixReduit:produit.prixReduit,
          reduction:produit.pourcentage_reduction+"%",
          quantiteSauve:(panierItemIndex!=-1)?parseInt(panier[panierItemIndex].qte):parseInt(produit.minimum_qte_vente),
          min:parseInt(produit.minimum_qte_vente),
          max:parseInt(produit.quantite),
          entrepriseName:produit.entrepriseName,
          vue:produit.vue,
          logoEntreprise:produit.logoEntreprise,
          photoProduit:produit.photo,
          isFavoris:produit.isFavoris,
          loading:false,
          adresseRamassage:(objectIsEmpty(produit.adresse_rammassage))?{label:"Non défini",ville:""}:produit.adresse_rammassage,
          periode_recuperation:produit.date_recup,
          dataIsReady:true,
          description:produit.description,
          contact:produit.responsable_entreprise.telephone
        });

      }else if(status==0){
        Alert.alert(
          'Session expirée',
          'Votre session a expiré.Veuillez vous reconnecter!',
          [
            {text: 'Ok', onPress: () => {
              this.setState({loading:false});
              self._logout();
            }, style:"cancelable"}
          ],
          { cancelable: false }
        );
      }else{
        this.setState({loading:false});
        Alert.alert(
          'Connexion interrompue',
          'Une erreur est survenue lors de la récupération des détails du produit.',
          [
            {text: 'Réessayer', onPress: () => {
              self.dataIsReady = false;
              self._checkData();
            }, style:"cancelable"},
            {text: 'Retour', onPress: () => {
              this._backToList();
            }, style:"cancelable"},
          ],
          { cancelable: false }
        );
      }

    }).catch(error=>{
      console.log(error);
      this.setState({loading:false});
      Alert.alert(
        'Connexion interrompue',
        'Impossible de se connecter avec le serveur.',
        [
          {text: 'Ok', onPress: () => this._backToList() , style:"cancelable"},
        ],
        { cancelable: false }
      );
    })

  }

  _addToPanier = ()=>{
    let produit = {
      id:this.produit.id_donation,
      nom:this.produit.nom_produit,
      categorie:this.produit.type_aliment,
      prix:this.produit.prixReduit,
      prixNormal:this.produit.prixNormal,
      datePeremption:this.produit.dateLimite,
      reduction:this.produit.pourcentage_reduction,
      isFavoris:this.produit.isFavoris,
      photo:this.produit.photo,
      adresseRamassage:(objectIsEmpty(this.produit.adresse_rammassage))?{label:"Non défini",ville:""}:this.produit.adresse_rammassage,
      optionLivraison:{etat:0,prix:1000},
      qte:parseInt(this.produit.quantite),
      minQte:parseInt(this.produit.minimum_qte_vente)
    }

    // const panier = this.props.panier;

    let doAdd = true;

    // const panierItemIndex = panier.findIndex(item => item.produit.id === produit.id);
    //
    // if(panierItemIndex!=-1){
    //   if( panier[panierItemIndex].qte+this.state.quantiteSauve > produit.qte){
    //       doAdd = false;
    //   }
    // }

    if(doAdd){
      const action = { type: "PUT_IN_PANIER", value: {produit:produit,qte:this.state.quantiteSauve} };
      this.props.dispatch(action);

      Alert.alert(
        'Produit sauvé',
        'Le produit a été ajouté à ton panier !',
        [
          {text: 'Continuer', onPress: () => this._backToList() , style:"cancelable"},
          {text: 'Aller au panier', onPress: () => this._startActivity("Panier") },
        ],
        { cancelable: false }
      );
    }else{
      Alert.alert(
      'Quantité en stock insuffisante',
      'La quantité limite en stock pour le produit "'+produit.nom+'" a été atteint.',
      [
        {text: 'Ok', onPress: () => console.log('Cancel Pressed'), style: 'cancel'}
      ],
      { cancelable: true });
    }
  }

  _addToFavoris(){
    this.setState({loading:true});

    ajouterFavoris({token:this.props.navigation.state.params.token,id_donation:this.props.navigation.state.params.id_donation,id_utilisateur:this.props.navigation.state.params.id_utilisateur}).then(data=>{

      console.log(data);

      if(data.status==1){
        this.setState({
          isFavoris:true,
          loading:false
        });
      }else{
        this.setState({loading:false});
        Alert.alert(
          'Connexion interrompue',
          'Une erreur s\'est produite lors de l\'enregistrement de  votre préférence',
          [
            {text: 'Ok', onPress: () => console.log("ok") , style:"cancelable"},
          ],
          { cancelable: false }
        );
      }

    }).catch(error=>{
      this.setState({loading:false});
      Alert.alert(
        'Connexion interrompue',
        'Une erreur s\'est produite lors de l\'enregistrement de  votre préférence',
        [
          {text: 'Ok', onPress: () => console.log("ok") , style:"cancelable"},
        ],
        { cancelable: false }
      );
    });
  }

  _viewProduit(){

    console.log("DetailProduit","_viewProduit");

    ajouterVueProduit({token:this.props.navigation.state.params.token,id_donation:this.props.navigation.state.params.id_donation,id_utilisateur:this.props.navigation.state.params.id_utilisateur}).then(data=>{

    }).catch(error=>{

    });

  }

  _removeToFavoris(){
    this.setState({loading:true});

    ajouterFavoris({token:this.props.navigation.state.params.token,id_donation:this.props.navigation.state.params.id_donation,id_utilisateur:this.props.navigation.state.params.id_utilisateur}).then(data=>{

      if(data.status==1){
        this.setState({
          isFavoris:false,
          loading:false
        })
      }else{
        this.setState({loading:false});
        Alert.alert(
          'Connexion interrompue',
          'Une erreur s\'est produite lors de l\'enregistrement de  votre préférence',
          [
            {text: 'Ok', onPress: () => console.log("ok") , style:"cancelable"},
          ],
          { cancelable: false }
        );
      }

    }).catch(error=>{
      this.setState({loading:false});
      Alert.alert(
        'Connexion interrompue',
        'Une erreur s\'est produite lors de l\'enregistrement de  votre préférence',
        [
          {text: 'Ok', onPress: () => console.log("ok") , style:"cancelable"},
        ],
        { cancelable: false }
      );
    });
  }

  _updateQuantiteSauve = (num)=>{
    console.log(num);
    this.setState({quantiteSauve:num});
  }


  _getImgSrc(){
    console.log("DetailProduit","_getImgSrc : "+this.state.photoProduit);
    if(this.state.photoProduit=="-1"){
      return require('../Images/other/no_img_placeholder.jpg');
    }else{
      return {uri:this.state.photoProduit};
    }
  }

  _toggleFavoris(){

    if(this.state.isFavoris){
      this._removeToFavoris();
    }else{
      this._addToFavoris();
    }

  }

  _backToList(){
    this.dataIsReady = false;
    console.log("DetailProduit","calling... _backToList");
    this._startActivity(this.props.navigation.state.params.src,{src:"DetailProduit"});
  }

  _renderPeriodeRecuperation(periode){
    return(
      <Text style={[{ color:"#545c69",fontSize:15 },stylesDefault.barlowRegular]}>-  {periode.jour} : {periode.horaire_debut} {"à"} {periode.horaire_fin}</Text>
    )
  }

  _renderListPeriodeRecuperation(){
    const periode_recuperation = this.state.periode_recuperation;
    const self = this;

    if(periode_recuperation!=null && periode_recuperation.length>0){

      const vData =  periode_recuperation.map((periode,index)=>{
        return(
          <View key={periode.id_periode_recuperation_commande}>
            { self._renderPeriodeRecuperation(periode)}
          </View>
        )
      })

      return (
        <View>
          { vData }
        </View>
      )
    }
  }

  _renderDescription(){
    if(this.state.description!=""){
      return(
        <View style={{ flex:1,flexDirection:"row" }}>
          <Text style={[{ color:"#545c69",flex:1,fontSize:12,textAlign:"justify",marginTop:8,marginBottom:8 },stylesDefault.barlowRegular]}>{this.state.description}</Text>

          <View style={{ paddingRight:10,paddingLeft:10,alignItems:"center",justifyContent:"center" }}>

            <Text style={[{ color:"#545c69",fontSize:24 },stylesDefault.barlowBold]}>{ mileSeparator(this.state.prixReduit*this.state.quantiteSauve) } Ar</Text>
            <Text style={[{ color:"#545c69",opacity:0.8,textDecorationLine:"line-through" },stylesDefault.barlowRegular]}>{ mileSeparator(this.state.prixNormal) } Ar</Text>

          </View>
        </View>
      )
    }else{
      return(
        <View style={{ flex:1,flexDirection:"row",justifyContent:"center",alignItems:"center" }} >
          <View style={{ flex:1,paddingRight:10,paddingLeft:10,alignItems:"center",justifyContent:"center" }}>

            <Text style={[{ color:"#545c69",fontSize:24 },stylesDefault.barlowBold]}>{ mileSeparator(this.state.prixReduit*this.state.quantiteSauve) } Ar</Text>
            <Text style={[{ color:"#545c69",opacity:0.8,textDecorationLine:"line-through" },stylesDefault.barlowRegular]}>{ mileSeparator(this.state.prixNormal) } Ar</Text>

          </View>
        </View>
      )
    }
  }

  /*<Image style={{ marginLeft:4,width:15,height:15,resizeMode:Image.resizeMode.contain,tintColor:"#ffffff",opacity:0.9 }} source={require('../Images/other/eye.png')} />*/

  render(){
    this._checkData();
    return(
      <View style={{ flex:1,backgroundColor:"#fff" }}>
          <Loader loading={this.state.loading} />

          <View style={{ height:256 }}>
            <Image style={{ flex:1,resizeMode:Image.resizeMode.cover }}
              source={this._getImgSrc()}
            />

            <View style={{ position:"absolute",left:0,right:0,flexDirection:"row",paddingRight:4,backgroundColor:"#00b2a977" }}>

              <TouchableOpacity onPress={()=>{ this._backToList() }}>
                <Image style={{ tintColor:"#fff",width:20,height:20,margin:10 }} source={ require("../Images/other/left_arrow.png") } />
              </TouchableOpacity>

              <View style={{ flex:1,flexDirection:"row-reverse" }}>
                <TouchableOpacity style={{ padding:10,alignItems:"center",justifyContent:"center" }} onPress={()=> this._toggleFavoris() }>
                  <Image style={{ width:25,height:25,resizeMode:Image.resizeMode.contain,tintColor:(this.state.isFavoris)?"#F05423":"#fff",opacity:0.9 }} source={require('../Images/other/wish.png')} />
                </TouchableOpacity>
              </View>

            </View>


          </View>

          <View style={[{ flexDirection:"row",alignItems:"center",padding:8 },stylesDefault.themePrimary]}>
            <Text style={[{ flex:1,fontSize:20,color:"#ffffff" },stylesDefault.barlowBold]}>{ this.state.nomProduit }</Text>

            <View style={{ flexDirection:"row",paddingLeft:4,paddingRight:4 }}>

              <View style={{ alignItems:"center",marginLeft:16 }}>
                <Text style={[{ fontSize:20 },stylesDefault.textThemeYellow,stylesDefault.barlowBold]}>-{ this.state.reduction }</Text>
              </View>

            </View>

          </View>

          <ScrollView style={{ flex:1,backgroundColor:"#ffffff",opacity:(this.state.dataIsReady)?1:0 }}>

            <View style={{ backgroundColor:"#ffffff",marginBottom:10 }}>

              <View style={{ flexDirection:"row",paddingLeft:8,paddingRight:8,paddingTop:8,paddingBottom:2 }}>

                { this._renderDescription() }

                <View style={{ justifyContent:"center" }}>
                  <Spinner color="#636e72" value={ this.state.quantiteSauve } min={ this.state.min } max={ this.state.max } value={ this.state.quantiteSauve } onNumChange={(num)=>{this._updateQuantiteSauve(num)}} />
                </View>

              </View>

              <View style={[{ height:1,marginLeft:8,marginRight:8 },stylesDefault.themePrimary]}></View>

              <View style={{ flexDirection:"row",paddingLeft:8,paddingRight:8,paddingBottom:8,paddingTop:16,alignItems:"center" }}>

                <View style={{width:64,height:64,borderRadius:100,
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 2 },
                shadowOpacity: 1,
                shadowRadius: 1,
                elevation: 4,
                marginBottom:6,
                alignItems:"center",
                marginLeft:8,
                marginRight:16
                }}>
                  <Image style={[{ width:64,height:64,resizeMode:Image.resizeMode.cover,borderRadius:100 }]} source={{uri:this.state.logoEntreprise}} />
                </View>

                <View style={{ flex:1 }}>

                  <View style={{ flexDirection:"row",alignItems:"center" }}>

                    <Image style={{ width:24,height:24,resizeMode:Image.resizeMode.contain,marginLeft:8,marginRight:8 }} source={require('../Images/icon/location.png')} />

                    <View>
                      <Text style={[{ color:"#545c69",flex:1,fontSize:15 },stylesDefault.barlowRegular]}>
                        { this.state.adresseRamassage.label }
                      </Text>
                      <Text style={[{ color:"#545c69",flex:1,fontSize:15 },stylesDefault.barlowRegular]}>
                        { this.state.adresseRamassage.ville }
                      </Text>
                    </View>

                  </View>

                  <View style={{ flexDirection:"row",alignItems:"center",marginTop:16 }}>

                    <Image style={{ width:24,height:24,resizeMode:Image.resizeMode.contain,marginLeft:8,marginRight:8 }} source={require('../Images/icon/clock.png')} />

                    <View>
                      { this._renderListPeriodeRecuperation() }
                    </View>

                  </View>

                  <View style={{ flexDirection:"row",alignItems:"center",marginTop:16 }}>

                    <Image style={{ width:24,height:24,resizeMode:Image.resizeMode.contain,marginLeft:8,marginRight:8 }} source={require('../Images/icon/phone.png')} />

                    <View>
                      <Text style={[{ color:"#545c69",flex:1,fontSize:15 },stylesDefault.barlowRegular]}>
                        +261330772464
                      </Text>
                    </View>

                  </View>

                </View>

              </View>

            </View>

          </ScrollView>

          <View style={{ alignItems:"center",marginBottom:8,marginTop:8 }}>

              <TouchableOpacity style={[{flexDirection:"row",paddingLeft:25,paddingRight:25,paddingTop:10,paddingBottom:10,borderRadius:100,marginLeft:8,
              marginRight:8,
              shadowColor: '#000',
              shadowOffset: { width: 0, height: 2 },
              shadowOpacity: 1,
              shadowRadius: 1,
              elevation: 4,
              marginBottom:8,
              alignItems:"center"},stylesDefault.themePrimary]} onPress={()=>{this._addToPanier()}}>
                <Image style={{ width:20,height:20,resizeMode:Image.resizeMode.contain,opacity:0.9 }} source={require('../Images/other/basket.png')} />
                <Text style={[{ marginLeft:8,color:"#fff",fontSize:18 },stylesDefault.barlowBold]}>SAUVER !</Text>
              </TouchableOpacity>

          </View>

        </View>
    )
  }
}

const styles = StyleSheet.create({
  button_fb_log_in:{
    width:120,
    flexDirection:"row",
    alignItems:"center",
    justifyContent:"center",
    backgroundColor:"#00AFAA",
    borderColor:"#dce1e5",
    borderWidth:2,
    borderRadius:10,
    paddingTop:10,
    paddingBottom:10
  },
  button_log_in_text:{
    fontSize:16,
    fontWeight:"bold",
    color:"#ffffff"
  }
})

const mapStateToProps = (state) => {
  return state
}

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: (action) => { dispatch(action) }
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(DetailProduit);
