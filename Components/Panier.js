import React from 'react'
import BaseComponent from './BaseComponent';
import { StyleSheet, View, Image, Text, BackHandler, TextInput, TouchableOpacity,ScrollView,Alert,FlatList,Modal  } from 'react-native'
import stylesDefault from '../Styles/styleDefault'
import Loader from './Loader';
import { connect } from 'react-redux';
import PanierItem from './PanierItem';
import EditPanierModal from './EditPanierModal';
import { mileSeparator } from '../Helper/StandardHelper';


class Panier extends BaseComponent {
  _didFocusSubscription;
  _willBlurSubscription;

  constructor(props) {
    super(props);

    this._didFocusSubscription = props.navigation.addListener('didFocus', payload =>
      BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
    );

    this.state ={
      loading:false,
      isEditMode:false,
      selectedPanierItem:{qte:3,produit:{minQte:1}}
    }
  }

  _back(){
    this._startActivity("ListeProduit");
  }

  componentDidMount() {
    this._willBlurSubscription = this.props.navigation.addListener('willBlur', payload =>
      BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
    );
  }

  onBackButtonPressAndroid = () => {
    this._back();
    return true;
  };

  _removeFromPanier = (produitId)=>{
    Alert.alert(
    'Confirmation',
    'Voulez-vous vraiment supprimer cet article ?',
    [
      {text: 'Oui', onPress: () => {
          const action = { type: "REMOVE_FROM_PANIER", value: {id:produitId} };
          this.props.dispatch(action);
          Alert.alert(
            'Succès',
            'Le produit a été supprimé !',
            [
              {text:'Ok',style:'cancel'}
            ],
            { cancelable: true }
          );
        }
      },
      {text:'Non',style:'cancel'}
    ],
    { cancelable: true });
  }


  _editPanier = (panierItem)=>{
    this.setState({selectedPanierItem:panierItem,isEditMode:true});
  }

  _saveEdit = (newQte,panierItem)=>{
    const panier = this.props.panier;
    let doAdd = true;

    const panierItemIndex = panier.findIndex(item => item.produit.id === panierItem.produit.id);

    if(panierItemIndex!=-1){
      if( newQte > panierItem.produit.qte){
          doAdd = false;
      }
    }

    if(doAdd){
      const action = { type: "EDIT_PANIER", value: {id:panierItem.produit.id,newQte:newQte} };
      this.props.dispatch(action);
      this.setState({isEditMode:false});
    }else{
      Alert.alert(
      'Quantité en stock insuffisante',
      'La quantité limite en stock pour le produit "'+panierItem.produit.nom+'" a été atteint.',
      [
        {text: 'Ok', onPress: () => console.log('Cancel Pressed'), style: 'cancel'}
      ],
      { cancelable: true });
    }

  }

  _getPrixTotal(){
    let result = 0;

    const panier = this.props.panier;

    for(let i=0;i<panier.length;i++){
      result+=(panier[i].qte*panier[i].produit.prix);
    }




    return result;
  }

  _getEconomie(){
    let result = 0;

    const panier = this.props.panier;

    for(let i=0;i<panier.length;i++){
      result+=(panier[i].qte*panier[i].produit.prixNormal);
    }


    return result - this._getPrixTotal();
  }

  _goToOptionRamassage(){
    if(this._getPrixTotal()>0){
      // this._startActivity("OptionRamassage");
      this._startActivity("ResumeCommande",{src:"Panier"});
    }
  }

  componentWillUnmount() {
    this._didFocusSubscription && this._didFocusSubscription.remove();
    this._willBlurSubscription && this._willBlurSubscription.remove();
  }

  _renderListeZone(){

    return (
      <View style={{ flex:1 }}>
        { this._renderListe() }
        { this._renderEmptyResult() }
      </View>
    )
  }

  _renderListe = ()=>{
    if(this.props.panier.length!=0){
      return(
        <FlatList
          style={{ flex:1 }}
          data={this.props.panier}
          keyExtractor={(item) => item.produit.id.toString()}
          renderItem={({item}) => (
            <PanierItem
              panierItem={item}
              showDetailProduit = { this._showDetailProduit }
              toggleFavoris = { this._toggleFavoris }
              removeFromPanier = { this._removeFromPanier }
              editPanier = { this._editPanier }
              editable = { true }
            />
          )}
          onEndReachedThreshold={0.1}
        />
      )
    }
  }

  _renderEmptyResult = ()=>{
    if(this.props.panier.length==0){
      return(
        <View style={{ flex:1,alignItems:"center",justifyContent:"center" }}>

          <View style={{ alignItems:"center",justifyContent:"center" }}>
            <Image source={ require('../Images/other/empty_box.png') } style={{ width:100,height:100,opacity:0.25 }} />
            <Text style={{ fontSize:16,color:"#4f4f4f",opacity:0.75,marginTop:16,fontFamily:"Barlow_Regular" }}>{ "Aucun produit dans votre panier" }</Text>
          </View>

        </View>
      )
    }
  }

  render(){
    return(
      <View style={{ flex:1,backgroundColor:"#ffffff" }} >

        <View ref="actionBar" style={[{
          borderBottomWidth:1,borderBottomColor:"#00000012",flexDirection:"row",paddingTop:8,paddingBottom:8,alignItems:"center",justifyContent:"center" },stylesDefault.themePrimary]}>

          <TouchableOpacity style={{ padding:10 }} onPress={()=>{this._back()}}>
            <Image source={require("../Images/other/left_arrow.png")} style={{ height:20, width:20, resizeMode:Image.resizeMode.contain,tintColor:"#fff" }} />
          </TouchableOpacity>

          <Text style={{ flex:1,fontSize:20,color:"#fff",fontFamily:"Barlow_Bold" }}>
            Mon panier
          </Text>

        </View>

        <Loader loading={this.state.loading} />

        <EditPanierModal
          isShown = {this.state.isEditMode}
          close = { ()=>{ this.setState({isEditMode:false})} }
          panierItem = { this.state.selectedPanierItem }
          saveEdit = { this._saveEdit }
        />

        { this._renderListeZone() }

        <View>

          <View style={[{flexDirection:"row-reverse",marginBottom:16},stylesDefault.themePrimary]}>
            <View style={[{ paddingRight:10,paddingTop:4,paddingBottom:4 }]}>
              <Text style={{ fontSize:16,color:"#fff",fontFamily:"Barlow_BlackItalic" }}>
                Total : { mileSeparator(this._getPrixTotal()) } Ar
              </Text>
              <Text style={{ fontSize:14,opacity:0.75,color:"#fff",fontFamily:"Barlow_Italic" }}>
                Economisé : { mileSeparator(this._getEconomie()) } Ar
              </Text>
            </View>
         </View>

         <View style={{ flexDirection:"row",justifyContent:"center" ,marginBottom:8}}>
            <TouchableOpacity style={[stylesDefault.fw_btn,{opacity:(this.props.panier.length==0)?0.5:1}]} onPress={()=>{ this._goToOptionRamassage() }} >
              <Text style={stylesDefault.fw_btn_txt}>{"Valider"}</Text>
              <Image style={{ marginLeft:8,width:15,height:15,resizeMode:Image.resizeMode.contain,tintColor:"#ffffff" }} source={require('../Images/other/right_arrow.png')} />
            </TouchableOpacity>
         </View>

        </View>

      </View>
    )
  }
}

const styles = StyleSheet.create({
  main_container:{
    flex:1
  },
  logo_container:{
    flex:1,
    alignItems:"center",
    justifyContent:"flex-end",
    paddingBottom:50,
    paddingTop:50
  },
  logo:{
    width:100,
    height:100
  },
  form_container:{
    paddingLeft:32,
    paddingRight:32
  },
  form_input:{
    flexDirection:"row",
    borderColor:"#dce1e5",
    borderWidth:2,
    borderRadius:10,
    overflow:"hidden",
    marginBottom:16
  },
  icon:{
    width:20,
    height:20,
    resizeMode:Image.resizeMode.contain
  },
  text_input:{
    flex:1,
    backgroundColor:"#ffffff"
  },
  input:{
    paddingBottom:12,
    paddingTop:12,
    paddingLeft:24,
    paddingRight:8,
    fontSize:16
  },
  icon_input:{
    paddingLeft:20,
    paddingRight:20,
    justifyContent:"center",
    backgroundColor:"#00AFAA"
  },
  button_container:{
    flexDirection:"row",
    marginTop:16,
    marginBottom:20
  },
  button_sign_up:{
    flex:1,
    alignItems:"center",
    justifyContent:"center",
    borderColor:"#dce1e5",
    borderWidth:2,
    borderRadius:10,
    overflow:"hidden",
    paddingTop:16,
    paddingBottom:16,

    marginRight:4,
    backgroundColor:"#ffffff"
  },
  button_log_in:{
    flex:1,
    backgroundColor:"#00AFAA",
    borderColor:"#dce1e5",
    borderWidth:2,
    borderRadius:10,
    overflow:"hidden",
    paddingTop:16,
    paddingBottom:16,
    alignItems:"center",
    marginLeft:4
  },
  button_fb_log_in:{
    flex:1,
    flexDirection:"row",
    alignItems:"center",
    justifyContent:"center",
    backgroundColor:"#00AFAA",
    borderColor:"#dce1e5",
    borderWidth:2,
    borderRadius:10,
    overflow:"hidden",
    paddingTop:24,
    paddingBottom:24,
    alignItems:"center",
    marginLeft:4
  },
  button_google_log_in:{
    flex:1,
    flexDirection:"row",
    alignItems:"center",
    justifyContent:"center",
    backgroundColor:"#ffffff",
    borderColor:"#dce1e5",
    borderWidth:2,
    borderRadius:10,
    overflow:"hidden",
    paddingTop:16,
    paddingBottom:16,
    alignItems:"center",
    marginLeft:4
  },
  button_log_in_text:{
    fontWeight:"bold",
    color:"#ffffff"
  },
  button_sign_up_text:{
    fontWeight:"bold",
    color:"#00AFAA"
  }
});

const mapStateToProps = (state) => {
  return state
}

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: (action) => { dispatch(action) }
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(Panier);
