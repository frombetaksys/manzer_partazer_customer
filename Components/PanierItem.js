import React from 'react'
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native'
import { mileSeparator } from '../Helper/StandardHelper';
import stylesDefault from '../Styles/styleDefault'


class PanierItem extends React.Component {

  render(){
    const { panierItem,editable } = this.props;

    const produit = panierItem.produit;
    return(
      <TouchableOpacity style={styles.main_container} onPress={ ()=> console.log("omeo detail") }>

        <View style={{ justifyContent:"center",paddingLeft:8 }}>
            <Image style={{ width:110,height:110,resizeMode:Image.resizeMode.cover,borderRadius:100 }}
              source={(produit.photo=="-1")?require('../Images/other/no_img_placeholder.jpg'):{uri:produit.photo}}
            />
        </View>

        <View style={{ flex:1, paddingLeft:8, margin:5 }}>

          <View style={{ flexDirection:'row' }}>
            <Text style={ [styles.title_text,stylesDefault.textThemePrimary] }>{ (produit.nom_produit)?produit.nom_produit:produit.nom }</Text>
          </View>

          <View style={{ flex:1,flexDirection:"row" }}>

            <View style={{ flex:1 }}>

              {produit.adresseRamassage?
                <View style={{ flexDirection:"row" }}>
                  <Text style={{ color:'#969696',fontFamily:"Barlow_Regular" }}>Ramassage:</Text>
                  <Text style={{ flex:1,color:'#969696',fontFamily:"Barlow_Regular" }}> { produit.adresseRamassage.label }, { produit.adresseRamassage.ville }</Text>
                </View>
                :null}

              <View style={{ flexDirection:"row" }}>
                <Text style={{ color:'#969696',fontFamily:"Barlow_Regular" }}>Prix unitaire:</Text>
                <Text style={{ flex:1,color:'#969696',fontFamily:"Barlow_Regular" }}> {mileSeparator(produit.prix)} Ar</Text>
              </View>

              <View style={{ flexDirection:"row" }}>
                <Text style={{ color:'#969696',fontFamily:"Barlow_Regular" }}>Réduction:</Text>
                <Text style={{ flex:1,color:'#969696',fontFamily:"Barlow_Regular" }}> -{ (produit.pourcentage_reduction)?produit.pourcentage_reduction:produit.reduction }%</Text>
              </View>

              <View style={{ flexDirection:"row" }}>
                <Text style={{ color:'#969696',fontFamily:"Barlow_Regular" }}>Date limite:</Text>
                <Text style={{ flex:1,color:'#969696',fontFamily:"Barlow_Regular" }}> { (produit.dateLimite)?produit.dateLimite:produit.datePeremption }</Text>
              </View>

              <View style={{ flexDirection:"row" }}>
                <Text style={{ color:'#969696',fontFamily:"Barlow_Regular" }}>Qté:</Text>
                <Text style={{ flex:1,color:'#969696',fontFamily:"Barlow_Regular" }}> {panierItem.qte}</Text>
              </View>

              <View style={{ flexDirection:"row" }}>
                <Text style={{ color:'#969696',fontFamily:"Barlow_Regular" }}>Option de livraison:</Text>
                <Text style={{ flex:1,color:'#969696',fontFamily:"Barlow_Regular" }}> Non</Text>
              </View>

            </View>

          </View>

          <View style={{ flexDirection:"row",marginTop:4 }}>
            <View style={{ flex:1,flexDirection:"row" }}>

              {editable?
                <View style={{ flex:1,flexDirection:"row" }}>
                  <TouchableOpacity style={{ paddingRight:9,paddingTop:6,paddingBottom:6,alignItems:"center",justifyContent:"center" }} onPress={ ()=>{ this.props.removeFromPanier(produit.id) } }>
                    <Image style={{ width:25,height:25,resizeMode:Image.resizeMode.contain,opacity:0.9 }} source={require('../Images/icon/cross.png')} />
                  </TouchableOpacity>

                  <TouchableOpacity style={{ paddingLeft:9,paddingRight:6,paddingTop:6,paddingBottom:6,alignItems:"center",justifyContent:"center" }} onPress={ ()=>{ this.props.editPanier(panierItem) } }>
                    <Image style={{ width:25,height:25,resizeMode:Image.resizeMode.contain,opacity:0.9 }} source={require('../Images/icon/edit.png')} />
                  </TouchableOpacity>
                </View>
                :null}


            </View>
            <View style={{ alignItems:"center",justifyContent:"center" }}>
              <Text style={{ backgroundColor:"#00AFAA",paddingTop:4,paddingBottom:4,paddingLeft:10,paddingRight:10,color:"#ffffff",fontSize:15,borderRadius:100,fontFamily:"Barlow_BlackItalic" }}>{ mileSeparator(produit.prix*panierItem.qte) } Ar</Text>
            </View>
          </View>

        </View>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  main_container: {
    flexDirection:"row",
    backgroundColor: '#FFFFFF',
    borderRadius:8,
    marginLeft:8,
    marginRight:8,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 1,
    shadowRadius: 1,
    elevation: 4,
    marginTop:4,
    marginBottom:6
  },
  title_text: {
    flex:1,
    fontSize:20,
    flexWrap : 'wrap',
    color:"#4f4f4f",
    fontFamily:"Barlow_Bold"
  },
  favorite_image: {
      width: 40,
      height: 40

  }
})

export default PanierItem;
