import React from 'react'
import BaseComponent from './BaseComponent';
import { StyleSheet, View, Image, Text, BackHandler, TextInput, TouchableOpacity,KeyboardAvoidingView, ScrollView,Alert,AsyncStorage,TouchableHighlight } from 'react-native'
import { logIn,createAccount } from '../API/UtilisateurAPI';
import Loader from './Loader';
import stylesDefault from "../Styles/styleDefault";
import { Dimensions } from 'react-native';
import { connect } from 'react-redux';


const win = Dimensions.get('window');
const iW = 2309;
const iH = 874;
const ratio = win.width/iW;


class Authentification extends BaseComponent {
  _didFocusSubscription;
  _willBlurSubscription;


  constructor(props) {
    super(props);

    this.state={
      id:"",
      nom:"",
      prenom:"",
      email:"",
      password:"",
      loading:false,
      nextSrc:require('../Images/icon/next.png'),
      dialog:require('../Images/quel_plaisir.png'),
      showForm:true
    };

  }

  _signIn = () => {

    const emailLogin = this.state.email;
    const passwordLogin = this.state.password;

    if(emailLogin.length>0 && passwordLogin.length>0){
      this.setState({loading:true});
      logIn(emailLogin,passwordLogin).then(data=>{
        console.log(data);

        if(data.status==-1 || data.status==-3){
          Alert.alert(
            'Information incorrect',
            'E-mail ou mot de passe incorrect.',
            [
              {text: 'OK', onPress: () => console.log('OK Pressed'),style: 'cancel'},
            ],
            { cancelable: true }
          );
        }else if(data.status==1){
          this._saveUserInfo(data.data);
          AsyncStorage.multiSet([['consommateurId',''+data.data.user.id_consommateur],['userId',''+data.data.user.utilisateur_id],['userName',''+data.data.user.nom],['userFirstName',''+data.data.user.prenom],['userEmail',''+data.data.user.mail],['userToken',''+data.data.token]],(err)=>{
            console.log(err);
            if(err){
                console.log(err);
            }else{

            }
          });
          this.setState({nom:data.data.user.nom,prenom:data.data.user.prenom,dialog:require('../Images/tu_nous_a_manque.png'),showForm:false});
        }

        this.setState({
          nomLogin:"",
          passwordLogin:"",
          loading:false
        });

      }).catch(error=>{
        console.log(error);
      })
    }else{
      Alert.alert(
        'Information incomplète',
        'Veuillez renseigner vos informations d\'identification',
        [
          {text: 'OK', onPress: () => console.log('OK Pressed'),style: 'cancel'},
        ],
        { cancelable: true }
      );
    }

  }

  _saveUserInfo(data){
    const action = { type: "SET_INFO", value: {idUser:data.user.utilisateur_id,token:data.token,userName:data.nom,userFirstName:data.prenom,email:data.mail} };
    this.props.dispatch(action);

  }

  _salutation(){
    if(this.state.showForm){
      this._signIn();
    }else{
      this._startActivity("ListeProduit");
    }
  }

  _renderMainView(){
    if(this.state.showForm){
      return(
        <View style={{ marginTop:100 }}>

          <View style={styles.form_input}>
            <View style={styles.text_input}>
              <TextInput underlineColorAndroid="#ffffff" style={[styles.input,stylesDefault.barlowRegular]} placeholderTextColor={"#fff"} placeholder={"Mail"} onChangeText={(text)=>this.setState({email:text})} />
            </View>
          </View>

          <View style={styles.form_input}>

            <View style={styles.text_input}>
              <TextInput underlineColorAndroid="#ffffff" style={[styles.input,stylesDefault.barlowRegular]} placeholderTextColor={"#fff"} placeholder={"Mots de passe"} onChangeText={(text)=> this.setState({password:text})} secureTextEntry={true}/>
            </View>

          </View>

        </View>
      )
    }else{
      return(
        <View style={{ flexDirection:"row",marginTop:-4 }}>
          <Text style={[{ flex:1,color:"#fff",fontSize:22,textAlign:"right",paddingRight:64 },stylesDefault.barlowRegular]} >{ this.state.nom } !</Text>
        </View>
      )
    }
  }

  render() {
      return (
        <KeyboardAvoidingView style={{flex:1}} >
          <Loader loading={this.state.loading} />
          <ScrollView style={[styles.main_container,stylesDefault.themePrimary]} keyboardShouldPersistTaps="handled">

            <View style={{ marginTop:60 }}>
              <Image style={{ width:128,height:32,resizeMode:Image.resizeMode.contain }} source={require('../Images/3_points.png')} />
            </View>

            <View>
              <Image style={{ width:win.width-32,height:iH*ratio,resizeMode:Image.resizeMode.contain }} source={this.state.dialog} />
            </View>

            { this._renderMainView() }

          </ScrollView>
          <TouchableOpacity style={{position:"absolute",bottom:16,right:16}} onPress={()=>{ this._salutation() }} >
            <Image fadeDuration={10} style={{ width:40,height:40,resizeMode:Image.resizeMode.contain }} source={this.state.nextSrc} />
          </TouchableOpacity>
        </KeyboardAvoidingView>
      )
  }
}

const styles = StyleSheet.create({
  main_container:{
    flex:1,
    padding:16
  },
  logo_container:{
    flex:1,
    alignItems:"center",
    justifyContent:"flex-end",
    paddingBottom:50,
    paddingTop:50
  },
  logo:{
    width:100,
    height:100
  },
  form_input:{
    flexDirection:"row",
    overflow:"hidden",
    marginBottom:16
  },
  icon:{
    width:20,
    height:20,
    resizeMode:Image.resizeMode.contain
  },
  text_input:{
    flex:1,
    borderBottomColor:"#fff"
  },
  input:{
    color:"#fff",
    paddingBottom:12,
    paddingTop:12,
    paddingLeft:4,
    paddingRight:8,
    fontSize:18
  },
  icon_input:{
    paddingLeft:20,
    paddingRight:20,
    justifyContent:"center",
    backgroundColor:"#4bae32"
  },
  button_container:{
    flexDirection:"row",
    marginTop:16,
    marginBottom:20
  },
  button_sign_up:{
    flex:1,
    alignItems:"center",
    justifyContent:"center",
    borderColor:"#dce1e5",
    borderWidth:2,
    borderRadius:10,
    overflow:"hidden",
    paddingTop:16,
    paddingBottom:16,
    marginRight:4,
    backgroundColor:"#ffffff"
  },
  button_log_in:{
    flex:1,
    backgroundColor:"#4bae32",
    borderColor:"#dce1e5",
    borderWidth:2,
    borderRadius:10,
    overflow:"hidden",
    paddingTop:16,
    paddingBottom:16,
    alignItems:"center",
    marginLeft:4
  },
  button_fb_log_in:{
    flex:1,
    flexDirection:"row",
    alignItems:"center",
    justifyContent:"center",
    backgroundColor:"#4267B2",
    borderColor:"#dce1e5",
    borderWidth:2,
    borderRadius:10,
    overflow:"hidden",
    paddingTop:16,
    paddingBottom:16,
    alignItems:"center",
    marginLeft:4
  },
  button_google_log_in:{
    flex:1,
    flexDirection:"row",
    alignItems:"center",
    justifyContent:"center",
    backgroundColor:"#ffffff",
    borderColor:"#dce1e5",
    borderWidth:2,
    borderRadius:10,
    overflow:"hidden",
    paddingTop:16,
    paddingBottom:16,
    alignItems:"center",
    marginLeft:4
  },
  button_log_in_text:{
    fontWeight:"bold",
    color:"#ffffff"
  },
  button_sign_up_text:{
    fontWeight:"bold",
    color:"#4bae32"
  }
});

const mapStateToProps = (state) => {
  return state
}

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: (action) => { dispatch(action) }
  }
}

// export default connect(mapStateToProps,mapDispatchToProps)(Authentification);
export default connect(mapStateToProps,mapDispatchToProps)(Authentification);
