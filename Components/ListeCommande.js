import React from 'react'
import BaseComponent from './BaseComponent';
import { StyleSheet, View, Image, Text, BackHandler, TextInput, TouchableOpacity,ScrollView,Alert,FlatList,Icon,Modal  } from 'react-native'
import stylesDefault from '../Styles/styleDefault'
import Loader from './Loader';
import ListeCommandeItem from './ListeCommandeItem';
import { connect } from 'react-redux';
import { getListeCommande } from '../API/ProduitAPI';

class ListeCommande extends BaseComponent {
  _didFocusSubscription;
  _willBlurSubscription;

  constructor(props) {
    super(props);

    this._didFocusSubscription = props.navigation.addListener('didFocus', payload =>
      BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
    );

    this.dataIsReady = false;

    this.state = {
      prixMin:0,
      prixMax:30000,
      modalPrixVisible:false,
      modalInfoNutriVisible:false,
      infoNutriSelected:[],
      filtreApplique : [],
      date:"22/10/2018",
      filtre:{value:0,label:"Aucun"},
      commandes:[],
      loading:false,
      user:{}
    }

  }

  _back(){
    this._startActivity(this.props.navigation.state.params.src);
  }

  componentDidMount() {
    this._willBlurSubscription = this.props.navigation.addListener('willBlur', payload =>
      BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
    );
  }

  onBackButtonPressAndroid = () => {
    this._back();
    return true;
  };

  componentWillUnmount() {
    this._didFocusSubscription && this._didFocusSubscription.remove();
    this._willBlurSubscription && this._willBlurSubscription.remove();
  }


  _checkData(){
    console.log("ListeCommande","dataIsReady : "+this.dataIsReady);
    if(!this.dataIsReady && this._inListeCommande()){
      this.dataIsReady = true;
      this._getUserInfo();
    }
  }

  _getUserInfo(){
    this.setState({loading:true});

    this._getUserDataInStorage((err, result) => {
      if(result!=null){

        let user ={
          userId :  result[0][1],
          userName : result[1][1],
          userFirstName : result[2][1],
          userEmail : result[3][1],
          userToken : result[4][1],
        };

        this._getListeCommande(user);
      }else{
        this.dataIsReady = false;
        this.setState({commandes:[],loading:false});
        this._startActivity("Login");
      }
    });
  }

  _getListeCommande(user){
    let self = this;

    getListeCommande({token:user.userToken,id_utilisateur:user.userId}).then(data=>{

        console.log(data);

        let status = data.status;

        if(status==1){

            this.setState({
              loading:false,
              commandes:data.data,
              user:user
            });

        }else if(status==0){
          Alert.alert(
            'Session expirée',
            'Votre session a expiré.Veuillez vous reconnecter!',
            [
              {text: 'Ok', onPress: () => {
                this.setState({loading:false});
                self._logout();
              }, style:"cancelable"}
            ],
            { cancelable: false }
          );
        }else{
          this.setState({loading:false});
          Alert.alert(
            'Connexion interrompue',
            'Une erreur est survenue lors de la récupération de la liste de vos commandes',
            [
              {text: 'Réessayer', onPress: () => {
                self.dataIsReady = false;
                self._checkData();
              }, style:"cancelable"},
            ],
            { cancelable: false }
          );
        }

    }).catch(error=>{
      console.log(error);
      this.setState({loading:false});
      Alert.alert(
        'Connexion interrompue',
        'Une erreur est survenue lors de la récupération de la liste de vos commandes',
        [
          {text: 'Réessayer', onPress: () => {
            self.dataIsReady = false;
            self._checkData();
          }, style:"cancelable"},
        ],
        { cancelable: false }
      );
    });
  }

  _back(){
    this.dataIsReady = false;

    const destActivity = this.props.navigation.state.params.src;

    this._startActivity(destActivity);
  }

  _showCommande = (commande)=>{
    this.dataIsReady = false;
    console.log("ListeCommande","commande : "+commande);
    this._startActivity("DetailCommande",{token:this.state.user.userToken,userId:this.state.user.userId,commande_id:commande.commande_id});
  }

  _renderListeZone(){
    console.log("state.commandes",this.state.commandes);
    return (
      <View style={{ flex:1 }}>

        { this._renderListe() }

        { this._renderEmptyResult() }

      </View>
    )
  }

  _renderListe(){
    if(this.state.commandes.length!=0){
      return(
        <FlatList
          style={{ flex:1 }}
          data={this.state.commandes}
          keyExtractor={(item) => item.commande_id.toString()}
          renderItem={({item}) => (
            <ListeCommandeItem
              commande={item}
              showCommande = { this._showCommande }
            />
          )}
          onEndReachedThreshold={0.5}
          onEndReached={() => {
            console.log("reachedEnd");
          }}
        />
      )
    }
  }

  _renderEmptyResult(){
    if(this.state.commandes.length==0 && !this.state.loading){
      return(
        <View style={{ flex:1,alignItems:"center",justifyContent:"center" }}>

          <View style={{ alignItems:"center",justifyContent:"center" }}>
            <Image source={ require('../Images/other/empty_box.png') } style={{ width:100,height:100,opacity:0.25 }} />
            <Text style={{ fontSize:16,color:"#4f4f4f",opacity:0.75,marginTop:16,fontFamily:"Barlow_Regular" }}>{ "Vous n'avez effectué aucune commande." }</Text>
          </View>

        </View>
      )
    }
  }

  render() {
      this._checkData();

      return (
        <View style={{ flex:1,backgroundColor:"#ffffff" }} >

        <View ref="actionBar" style={[{
          borderBottomWidth:1,borderBottomColor:"#00000012",flexDirection:"row",paddingTop:8,paddingBottom:8,alignItems:"center",justifyContent:"center" },stylesDefault.themePrimary]}>

          <TouchableOpacity style={{ padding:10 }} onPress={()=>{this._back()}}>
            <Image source={require("../Images/other/left_arrow.png")} style={{ height:20, width:20, resizeMode:Image.resizeMode.contain,tintColor:"#fff" }} />
          </TouchableOpacity>

          <Text style={{ flex:1,fontSize:20,color:"#fff",fontFamily:"Barlow_Bold" }}>
            Mes commandes
          </Text>

        </View>

          <Loader loading={this.state.loading} />

          { this._renderListeZone() }

        </View>
      )
  }
}


const styles = StyleSheet.create({

});

const mapStateToProps = (state) => {
  return state
}

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: (action) => { dispatch(action) }
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(ListeCommande);
