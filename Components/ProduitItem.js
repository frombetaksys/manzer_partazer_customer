import React from 'react'
import { StyleSheet, View, Text, Image, TouchableOpacity,ImageBackground } from 'react-native'
import { mileSeparator } from '../Helper/StandardHelper';
import stylesDefault from '../Styles/styleDefault'

class ProduitItem extends React.Component {

  _renderQtePanier(produit){
    if(this.props.produitInPanier(produit)){
      return(
        <View>
          <Text style={{ color:"#969696" }}> ({ this.props.qteInPanier(produit) })</Text>
        </View>
      )
    }
  }

  _renderFavorisTogller(produit){
    if(!this.props.favorisList){
      return(
        <TouchableOpacity style={{ paddingRight:6,paddingTop:6,paddingBottom:6,alignItems:"center",justifyContent:"center" }} onPress={()=> this.props.toggleFavoris(produit)}>
          <Image style={{ width:25,height:25,resizeMode:Image.resizeMode.contain,tintColor:(produit.isFavoris)?"#F05423":"#99d6ea",opacity:0.9 }} source={require('../Images/other/wish.png')} />
        </TouchableOpacity>
      )
    }
  }

  render(){
    const { produit } = this.props;
    return(
      <TouchableOpacity style={styles.main_container} onPress={ ()=> this.props.showDetailProduit(produit.id) }>

          <ImageBackground source={(produit.photo=="-1")?require('../Images/other/no_img_placeholder.jpg'):{uri:produit.photo}} style={{ flex:1 }} imageStyle={{resizeMode:Image.resizeMode.cover}}>

              <View style={{flexDirection:"row",padding:8,backgroundColor:"#ffb845cc"}} >
                <Text style={[{ flex:1,color:"#fff",fontSize:16},stylesDefault.barlowBold]}>{produit.nom}</Text>
                <Text style={[{ fontSize:16 },stylesDefault.textThemeRed,stylesDefault.barlowBold]}>{produit.reduction}%</Text>
              </View>

              <View style={{ backgroundColor:"#0009",flex:1,paddingTop:16,paddingBottom:16 }}>

                <View style={{ flex:1,alignItems:"center",justifyContent:"center" }}>
                  <Text style={[{fontWeight:"bold"},stylesDefault.textThemeYellow,stylesDefault.barlowRegular]}>{produit.entreprise.nom}</Text>
                  <Text style={[{ fontSize:10,color:"#fff" },stylesDefault.barlowRegular]}>{produit.adresseRamassage.label} {produit.adresseRamassage.ville}</Text>
                  <Text style={[{ fontSize:10,color:"#fff" },stylesDefault.barlowRegular]}>{produit.datePeremption}</Text>
                  <Text style={[{ fontSize:10,color:"#fff" },stylesDefault.barlowRegular]}>{produit.qte} restants</Text>

                  <Text style={[{ color:"#fff",fontSize:20,marginTop:8 },stylesDefault.barlowBold]}>{mileSeparator(produit.prix)} Ar</Text>
                  <Text style={[{ color:"#fff",textDecorationLine:'line-through' },stylesDefault.barlowRegular]}>{mileSeparator(produit.prixNormal)} Ar</Text>
                </View>

                <View style={{position:"absolute"}}>
                  <Image style={{ marginLeft:8,marginTop:8,width:40,height:40,resizeMode:Image.resizeMode.cover,borderRadius:100 }}
                    source={(produit.entreprise.logo=="-1")?require('../Images/other/no_img_placeholder.jpg'):{uri:produit.entreprise.logo}}
                    />
                </View>

              </View>

          </ImageBackground>

          <View style={{ alignItems:"center",justifyContent:"center",backgroundColor:"#fff" }}>
            { this._renderFavorisTogller(produit) }
          </View>

      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  main_container: {
    backgroundColor: '#FFFFFF',
    marginLeft:8,
    marginRight:8,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 1,
    shadowRadius: 1,
    elevation: 4,
    marginTop:4,
    marginBottom:6
  },
  title_text: {
    flex:1,
    fontSize:20,
    fontWeight: 'bold',
    flexWrap : 'wrap',
    color:"#4f4f4f"
  },
  favorite_image: {
      width: 40,
      height: 40

  }
})

export default ProduitItem;
