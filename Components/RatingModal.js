import React from 'react';
import { View,Text,TouchableOpacity } from 'react-native';
import Modal from "react-native-modal";
import { Rating, AirbnbRating } from 'react-native-ratings';

class RatingModal extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      service:0,
      livraison:0,
      rapidite:0,
      qualiteNourriture:0
    }
  }

  _close(){
    this.setState({isVisible:false});
  }

  _saveNote(note){
    this.setState(note);
  }

  _pushReviews(){
    this.props.saveRating(this.state);
  }

  render(){
    return(
      <Modal isVisible={ this.props.showRating }>
        <View style={{ flex: 1,alignItems:"center",justifyContent:"center" }}>
          <View style={{ backgroundColor:"#ffffff",borderRadius:4,padding:16 }}>

            <View style={{ alignItems:"center",justifyContent:"center" }}>
              <Text style={{ textAlign:"center",fontSize:18,color:"#636e72" }}>Evaluez la qualité de notre travail.</Text>
            </View>

            <View style={{ backgroundColor:"#00000025", height:1, opacity:0.5,marginTop:8,marginBottom:8 }}></View>

            <View style={{ alignItems:"center",justifyContent:"center",marginBottom:16 }}>
              <Text style={{ textAlign:"center",fontSize:16,fontWeight:"bold",color:"#636e72" }}>
                Service
              </Text>

              <AirbnbRating
                count={5}
                defaultRating={0}
                size={30}
                onFinishRating={(rating)=>{ this._saveNote({ service:rating }) }}
              />

            </View>

            <View style={{ alignItems:"center",justifyContent:"center",marginBottom:16 }}>
              <Text style={{ textAlign:"center",fontSize:16,fontWeight:"bold",color:"#636e72" }}>
                Livraison
              </Text>

              <AirbnbRating
                count={5}
                defaultRating={0}
                size={30}
                onFinishRating={(rating)=>{ this._saveNote({ livraison:rating }) }}
              />

            </View>

            <View style={{ alignItems:"center",justifyContent:"center",marginBottom:16 }}>
              <Text style={{ textAlign:"center",fontSize:16,fontWeight:"bold",color:"#636e72" }}>
                Rapidité
              </Text>

              <AirbnbRating
                count={5}
                defaultRating={0}
                size={30}
                onFinishRating={(rating)=>{ this._saveNote({ rapidite:rating }) }}
              />

            </View>

            <View style={{ alignItems:"center",justifyContent:"center",marginBottom:16 }}>
              <Text style={{ textAlign:"center",fontSize:16,fontWeight:"bold",color:"#636e72" }}>
                Qualité de nourriture
              </Text>

              <AirbnbRating
                count={5}
                defaultRating={0}
                size={30}
                onFinishRating={(rating)=>{ this._saveNote({ qualiteNourriture:rating }) }}
              />

            </View>

            <View style={{ flexDirection:"row",alignItems:"center",justifyContent:"center",marginTop:16 }}>

              <TouchableOpacity style={{flexDirection:"row",alignItems:"center",justifyContent:"center",backgroundColor:"#dfe6e9",borderColor:"#dce1e5",borderWidth:2,borderRadius:10,paddingTop:10,paddingBottom:10,paddingLeft:10,paddingRight:10,marginRight:8}} onPress={()=>{ this._close() }}>
                <Text style={{fontSize:16,fontWeight:"bold",color:"#636e72"}}>ANNULER</Text>
              </TouchableOpacity>

              <TouchableOpacity style={{flexDirection:"row",alignItems:"center",justifyContent:"center",backgroundColor:"#00b894",borderColor:"#dce1e5",borderWidth:2,borderRadius:10,paddingTop:10,paddingBottom:10,paddingLeft:10,paddingRight:10,marginLeft:8}} onPress={()=>{ this._pushReviews() }}>
                <Text style={{fontSize:16,fontWeight:"bold",color:"#ffffff"}}>ENVOYER</Text>
              </TouchableOpacity>

            </View>
          </View>
        </View>
      </Modal>
    )
  }

}

export default RatingModal;
