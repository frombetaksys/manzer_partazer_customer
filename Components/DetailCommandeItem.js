import React from 'react'
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native'
import { mileSeparator } from '../Helper/StandardHelper';


class DetailCommandeItem extends React.Component {

  _renderOptionLivraison(produit){
    if(produit.optionLivraison.etat==1){
      return(
        <View style={{ flexDirection:"row" }}>
          <Text style={{ color:'#969696',textDecorationLine:"underline" }}>Coût de livraison :</Text>
          <Text style={{ flex:1,color:'#969696' }}> { mileSeparator(produit.optionLivraison.prix)  } MGA</Text>
        </View>
      )
    }
  }

  render(){
    const { detailCommandeItem } = this.props;

    const produit = detailCommandeItem.produit;

    return(
      <TouchableOpacity style={styles.main_container} onPress={ ()=> console.log("omeo detail") }>
        <Image style={{ width:110,height:110,resizeMode:Image.resizeMode.cover,borderRadius:4,backgroundColor:"#00000010" }}
          source={(produit.photo=="-1")?require('../Images/other/no_img_placeholder.jpg'):{uri:produit.photo}}
        />
        <View style={{ flex:1, paddingLeft:8, margin:5 }}>

          <View style={{ flexDirection:'row' }}>
            <Text style={ styles.title_text }>{ produit.nom_produit }</Text>
          </View>

          <View style={{ flex:1,flexDirection:"row" }}>

            <View style={{ flex:1 }}>


              <View style={{ flexDirection:"row" }}>
                <Text style={{ color:'#969696',textDecorationLine:"underline" }}>Date limite:</Text>
                <Text style={{ flex:1,color:'#969696' }}> { produit.dateLimite }</Text>
              </View>

              <View style={{ flexDirection:"row" }}>
                <Text style={{ color:'#969696',textDecorationLine:"underline" }}>Prix unité :</Text>
                <Text style={{ flex:1,color:'#969696' }}> { mileSeparator(produit.prix) } MGA</Text>
              </View>

              <View style={{ flexDirection:"row" }}>
                <Text style={{ color:'#969696',textDecorationLine:"underline" }}>Réduction :</Text>
                <Text style={{ flex:1,color:'#969696' }}> -{ produit.pourcentage_reduction }%</Text>
              </View>

              <View style={{ flexDirection:"row" }}>
                <Text style={{ color:'#969696',textDecorationLine:"underline" }}>Qté :</Text>
                <Text style={{ flex:1,color:'#969696' }}> { produit.qte_commande }</Text>
              </View>

              <View style={{ flexDirection:"row" }}>
                <Text style={{ color:'#969696',textDecorationLine:"underline" }}>Ramassage :</Text>
                <Text style={{ flex:1,color:'#969696' }}> { produit.adresse_rammassage.label }, { produit.adresse_rammassage.ville }</Text>
              </View>

            </View>

          </View>

          <View style={{ flexDirection:"row",marginTop:2 }}>
            <View style={{ flex:1,flexDirection:"row" }}>

            </View>
            <View style={{ alignItems:"center",justifyContent:"center" }}>
              <Text style={{ backgroundColor:"#00AFAA",paddingTop:4,paddingBottom:4,paddingLeft:8,paddingRight:8,color:"#ffffff",fontWeight:"bold",fontSize:15,borderRadius:2 }}>{ mileSeparator(parseInt(produit.qte_commande)*produit.prix) } MGA</Text>
            </View>
          </View>

        </View>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  main_container: {
    flexDirection: 'row',
    backgroundColor: '#FFFFFF',
    borderBottomWidth:1,
    borderColor:"#00000015",
    alignItems:"center",
    paddingTop:6,
    paddingBottom:6,
    paddingLeft:6,
    paddingRight:6,
    marginLeft:8,
    marginRight:8,
  },
  title_text: {
    flex:1,
    fontSize:20,
    fontWeight: 'bold',
    flexWrap : 'wrap',
    color:"#4f4f4f"
  },
  favorite_image: {
      width: 40,
      height: 40

  }
})

export default DetailCommandeItem;
