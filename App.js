import React from 'react';
import Navigation from './Navigation/Navigation'
import { Provider } from 'react-redux'
import { MenuProvider } from 'react-native-popup-menu';

import { PersistGate } from 'redux-persist/integration/react'
import Store from './Store/configureStore'

export default class App extends React.Component {
  render() {
    return (
      <Provider store={Store.store}>
        <PersistGate loading={null} persistor={Store.persistor}>
          <MenuProvider>
            <Navigation/>
          </MenuProvider>
        </PersistGate>
      </Provider>
    );
  }
}

//${safeExtGet('googlePlayServicesVersion', '+')}
