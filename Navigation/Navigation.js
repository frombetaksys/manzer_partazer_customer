import { createStackNavigator } from 'react-navigation'
import SplashScreen from '../Components/SplashScreen'
import Login from '../Components/Login'
import Inscription from '../Components/Inscription'
import Profil from '../Components/Profil'
import EditProfil from '../Components/EditProfil'
import NoNavBar from '../Components/NoNavBar'
import ListeProduit from '../Components/ListeProduit'
import DetailProduit from '../Components/DetailProduit'
import Panier from '../Components/Panier'
import OptionRamassage from '../Components/OptionRamassage'
import ResumeCommande from '../Components/ResumeCommande'
import ListeFavoris from '../Components/ListeFavoris'
import ListeCommande from '../Components/ListeCommande'
import DetailCommande from '../Components/DetailCommande'
import SuiviLivraison from '../Components/SuiviLivraison'
import Authentification from '../Components/Authentification'
import AuthentificationSocialNetwork from '../Components/AuthentificationSocialNetwork'
import Welcome from '../Components/Welcome'
import CarteLocalisation from '../Components/CarteLocalisation'

const AppStackNavigator = createStackNavigator({

  SplashScreen: {
    screen: SplashScreen,
    navigationOptions : {
      header:null
    }
  },
  Login: {
    screen:Login,
    navigationOptions : {
      header:null
    }
  },
  Inscription: {
    screen:Inscription,
    navigationOptions : {
      header:null
    }
  },
  Profil: {
    screen:Profil,
    navigationOptions : {
      header:null
    }
  },
  EditProfil: {
    screen:EditProfil,
    navigationOptions : {
      header:null
    }
  },
  ListeProduit: {
    screen:ListeProduit,
    navigationOptions : {
      header:null
    }
  },
  DetailProduit: {
    screen:DetailProduit,
    navigationOptions : {
      header:null
    }
  },
  Panier: {
    screen:Panier,
    navigationOptions : {
      header:null
    }
  },
  OptionRamassage: {
    screen:OptionRamassage,
    navigationOptions : {
      header:null
    }
  },
  ResumeCommande: {
    screen:ResumeCommande,
    navigationOptions : {
      header:null
    }
  },
  ListeFavoris: {
    screen:ListeFavoris,
    navigationOptions : {
      header:null
    }
  },
  ListeCommande: {
    screen:ListeCommande,
    navigationOptions : {
      header:null
    }
  },
  DetailCommande: {
    screen:DetailCommande,
    navigationOptions : {
      header:null
    }
  },
  SuiviLivraison: {
    screen:SuiviLivraison,
    navigationOptions : {
      header:null
    }
  },
  Authentification: {
    screen:Authentification,
    navigationOptions : {
      header:null
    }
  },
  AuthentificationSocialNetwork: {
    screen:AuthentificationSocialNetwork,
    navigationOptions : {
      header:null
    }
  },
  Welcome: {
    screen:Welcome,
    navigationOptions : {
      header:null
    }
  },
  CarteLocalisation: {
    screen:CarteLocalisation,
    navigationOptions : {
      header:null
    }
  }

})


export default AppStackNavigator
